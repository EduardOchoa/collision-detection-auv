// Thread that uses the frames and motion model obtained from Multicol-SLAM
// to create a depth map and detect the possible points where the AUV
// can collide.

#include <opencv2/opencv.hpp>
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <numeric>
#include <stdio.h>

#include "CollisionDetector.h"
#include "cConverter.h"

using namespace cv::xfeatures2d;

namespace MultiColSLAM
{
    CollisionDetector::CollisionDetector():
    fAcceptFrame(true), b_add_newkeyframe_(false), bnewdata_(false), b_add_newkeyframe(false)
    {}

    void CollisionDetector::Run()
    {
        int point_cloud_counter = 0; // to store multiply files
        while(true)
        {

            if(CheckDataAvailability())
            {
                //continue;
                if(b_add_newkeyframe_)
                {
					current_keyframe_ = tracked_frame_;
					
					// Checking that there is already two keyframes that have been received from the system
					if(!previous_keyframe_.empty_frame){
						// One camera implementation at the moment
						int number_cams = previous_keyframe_.camSystem.GetNrCams();
						int cam = 2; //current cam in use

                        // DEBUG ****** Windows for visualizing the feature extraction process
                        //cv::namedWindow("Epipolar lines in current frame - F OpenCV", CV_WINDOW_NORMAL);
                        //cv::resizeWindow("Epipolar lines in current frame - F OpenCV",600, 400);
                        //cv::namedWindow("Epipolar lines in current frame - F Multicol", CV_WINDOW_NORMAL);
                        //cv::resizeWindow("Epipolar lines in current frame - F Multicol",600, 400);
                        cv::namedWindow("Matches after filtering - MultiCol", CV_WINDOW_NORMAL);
                        cv::resizeWindow("Matches after filtering - MultiCol",1000, 600);
                        //cv::namedWindow("Matches after filtering - OpenCV", CV_WINDOW_NORMAL);
                        //cv::resizeWindow("Matches after filtering - OpenCV",1000, 600);

						// Image preprocessing
						cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
						clahe->setClipLimit(2);
						
						cv::Mat previous_image_cam_i, current_image_cam_i;
						clahe->apply(previous_keyframe_.images[cam], previous_image_cam_i);
						clahe->apply(current_keyframe_.images[cam], current_image_cam_i);
						
						/// FEATURE DETECTION
						// Detect keypoints and compute the descriptors
						// Implementing different types of feature descriptors from OpenCv
						std::vector<cv::KeyPoint> keypoints_previous_i, keypoints_current_i;
						cv::Mat descriptors_previous_i, descriptors_current_i;
						string detector_type = "SIFT";
						if(detector_type == "SURF"){
							int minHessian = 100; // Used for SURF Detector as a threshold (larger is more restrictive)
							cv::Ptr<SURF> detector = SURF::create(minHessian,4,3,false);
							
							detector->detectAndCompute(previous_image_cam_i, cv::Mat(), keypoints_previous_i, descriptors_previous_i);
							detector->detectAndCompute(current_image_cam_i, cv::Mat(), keypoints_current_i, descriptors_current_i);
						}
						else if (detector_type == "SIFT") {
							cv::Ptr<SIFT> detector = SIFT::create(10000,1,0.01,10,1.1);
							
							detector->detectAndCompute(previous_image_cam_i, cv::Mat(), keypoints_previous_i, descriptors_previous_i);
							detector->detectAndCompute(current_image_cam_i, cv::Mat(), keypoints_current_i, descriptors_current_i);
						}
						
						/// FEATURE MATCHING
						// Matching the descriptors using FLANN matcher
						cv::Ptr<cv::DescriptorMatcher> matcher = cv::DescriptorMatcher::create(cv::DescriptorMatcher::FLANNBASED);
						std::vector < std::vector<cv::DMatch> > knn_matches;
						matcher->knnMatch(descriptors_previous_i, descriptors_current_i, knn_matches, 2);

                        // Pre-filtering of matches using Lowe's ratio test
                        std::vector< cv::DMatch > best_matches;
                        const float ratio_thresh = 0.8f;
                        for (size_t i = 0; i < knn_matches.size(); i++)
                        {
                            if (knn_matches[i][0].distance < ratio_thresh * knn_matches[i][1].distance)
                            {
                                best_matches.push_back(knn_matches[i][0]);
                            }
                        }

                        cout << "matches " << best_matches.size() <<endl;

                        // Filtering the matches using the motion model
                        // 1. Getting the transformation matrix between the frames
                        cv::Mat extrinsics_cam_i = cv::Mat(current_keyframe_.GetPoseMc(cam)); // Intrinsics wont change in camera i
                        cv::Mat pose_current_i = cv::Mat(current_keyframe_.GetPose());
                        cv::Mat pose_previous_i = cv::Mat(previous_keyframe_.GetPose());
                        //cv::Mat intrinsics_azores = (cv::Mat_<double>(3,3) << 555, 0, 407.5, 0 , 555, 307.5, 0, 0, 1); //Azores intrinsics
                        //cv::Mat intrinsics_office = (cv::Mat_<double>(3,3) << 313.5, 0, 404, 0 , 313.5, 308, 0, 0, 1); // Hardcoded for our case. All camera images have been undistorted with the same intrinsic parameters
                        cv::Mat intrinsics_boreas = GetIntrinsicBoreas(4); // camera number 5 is the downlooking one
                        cv::Mat intrinsics;
                        intrinsics_boreas.copyTo(intrinsics);

                        cv::Mat Tcurrent_respect_previous(4,4,CV_64F), Tprevious_respect_current;
                        TransformationMatrixBetweenFrames(extrinsics_cam_i, pose_previous_i, pose_current_i, Tcurrent_respect_previous);
                        Tcurrent_respect_previous.copyTo(Tprevious_respect_current);
                        Tprevious_respect_current = Tprevious_respect_current.inv(); // Transformation of frame 1 respect to frame 2

                        // 2. Transforming the Rotation and translation parameters to obtain the Fundamental Matrix
                        cv::Mat F_matrix_multicol(3,3,CV_64F);
                        cv::Mat lines_on_previous_i, lines_on_current_i,lines_on_current2_i; // New images with the plotted epipolar lines
                        ComputeFundamentalMatrix(Tprevious_respect_current, intrinsics, F_matrix_multicol);

                        /// EPIPOLAR MATCHING
                        std::vector<cv::DMatch> matches_with_epipolar_constraint = MatchingWithEpipolarConstraint(F_matrix_multicol, previous_image_cam_i, descriptors_previous_i, keypoints_previous_i,
                                                                                                                 current_image_cam_i, descriptors_current_i, keypoints_current_i);
                        cv::Mat img_matches_multicol;
                        cout << "Multicol matches with epipolar "<< matches_with_epipolar_constraint.size() << endl;

                        std::vector <cv::Point2f> points_previous_i, points_current_i;
                        FromMatchesToVectorOfPoints(keypoints_previous_i, keypoints_current_i, best_matches,points_previous_i,points_current_i);

                        // *****OPENCV DEBUG*****
                        // Computing the fundamental matrix using OpenCV to compare the methods
                        cv::Mat outliers;
                        cv::Mat F_matrix_opencv = cv::findFundamentalMat(points_previous_i, points_current_i, cv::FM_RANSAC, 2, 0.99, outliers);

                        // Drawing epipolar lines into the images for debugging
                        std::vector <cv::Vec3f> lines_previous_i, lines_current_i, lines_current2_i;
                        cv::computeCorrespondEpilines(points_previous_i, 1, F_matrix_multicol, lines_current_i);
                        DrawEpipolarlines(current_image_cam_i, previous_image_cam_i, lines_on_current_i, lines_current_i);
                        cv::computeCorrespondEpilines(points_previous_i, 1, F_matrix_opencv, lines_current2_i); // Using F calculation from OpenCv
                        DrawEpipolarlines(current_image_cam_i, previous_image_cam_i, lines_on_current2_i, lines_current2_i);

                        // Distance from point to Epiline calculation
                        cv::Mat distances_to_epilines_OpencV, distances_to_epilines_Multicol;
                        for (int l=0; l < points_previous_i.size(); l++) {
                            double distance_opencvF = DistancePointToLine(points_current_i[l],lines_current2_i[l]);
                            double distance_multicol = DistancePointToLine(points_current_i[l],lines_current_i[l]);
                            distances_to_epilines_OpencV.push_back(distance_opencvF);
                            distances_to_epilines_Multicol.push_back(distance_multicol);
                        }

                        // Filtering the matches by using the distance from point to line
                        int pixels_distance_threshold = 4;
                        std::vector< cv::DMatch > filtered_matches_multicol, filtered_matches_opencv;
                        for (int idx=0; idx < points_previous_i.size(); idx++) {
                            if(distances_to_epilines_Multicol.at<double>(idx) <= pixels_distance_threshold){
                                filtered_matches_multicol.push_back(best_matches[idx]);
                            }
                            if(outliers.at<uchar>(idx)){
                                filtered_matches_opencv.push_back(best_matches[idx]);
                            }
                        }

                        cout << "multicol matches with FLANN" << filtered_matches_multicol.size() <<endl;
                        cout << "opencv matches " << filtered_matches_opencv.size() <<endl;

                        //cv::imshow("Epipolar lines in current frame - F OpenCV", lines_on_current2_i);
                        //cv::imshow("Epipolar lines in current frame - F Multicol", lines_on_current_i);

                        // Drawing and showing detected matches
                        /*cv::Mat img_matches_opencv;
                        cv::drawMatches( previous_image_cam0, keypoints_previous, current_image_cam0,
                                         keypoints_current, filtered_matches_multicol, img_matches_multicol,  cv::Scalar::all(-1),
                                        cv::Scalar::all(-1), std::vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
                        cv::drawMatches( previous_image_cam_i, keypoints_previous_i, current_image_cam_i,
                                         keypoints_current_i, filtered_matches_opencv, img_matches_opencv,  cv::Scalar::all(-1),
                                         cv::Scalar::all(-1), std::vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
                        cv::imshow( "Matches after filtering - OpenCV", img_matches_opencv );

                        // Drawing keypoints
                        cv::Mat previous_img_keypoints, current_img_keypoints;
                        cv::drawKeypoints(previous_image_cam_i, keypoints_previous_i, previous_img_keypoints, cv::Scalar::all(-1), cv::DrawMatchesFlags::DEFAULT);
                        cv::drawKeypoints(current_image_cam_i, keypoints_current_i, current_img_keypoints, cv::Scalar::all(-1), cv::DrawMatchesFlags::DEFAULT);
                        cv::imshow("Previous frame - camera 0", previous_img_keypoints);
                        cv::imshow("Current frame - camera 0", current_img_keypoints);*/
                        double  average_best_matches = AverageCosineValueOfMatches(best_matches, descriptors_previous_i, descriptors_current_i);
                        cout << "average best_matches " << average_best_matches << endl;
                        double  average_F_multicol_matches = AverageCosineValueOfMatches(filtered_matches_multicol, descriptors_previous_i, descriptors_current_i);
                        cout << "average F multicol " << average_F_multicol_matches << endl;
                        double  average_epipolar_matches = AverageCosineValueOfMatches(matches_with_epipolar_constraint, descriptors_previous_i, descriptors_current_i);
                        cout << "average epipolar " << average_epipolar_matches << endl;
                        cv::drawMatches( previous_image_cam_i, keypoints_previous_i, current_image_cam_i,
                                         keypoints_current_i, matches_with_epipolar_constraint, img_matches_multicol,  cv::Scalar::all(-1),
                                         cv::Scalar::all(-1), std::vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
                        cv::imshow( "Matches after filtering - MultiCol", img_matches_multicol );

                        /// TRIANGULATION STEP
                        // Beginning the triangulation process
                        // Find the projective matrices

                        // From Multicol-SLAM
                        cv::Mat projective_mat1_multicol, projective_mat2_multicol;
                        ComputeProjectionMatrices(intrinsics, Tprevious_respect_current, projective_mat1_multicol, projective_mat2_multicol); // Transformation used is from frame 1 to frame 2
                                                                                                                                            // so the origin is assumed at frame 1
                        // From OpenCV
                        // Debugging of F matrix from OpenCV
                        cv::Mat E_mat;
                        cv::sfm::essentialFromFundamental(F_matrix_opencv, intrinsics, intrinsics, E_mat);
                        std::vector<cv::Mat> R_matrices, t_matrices;

                        cv::sfm::motionFromEssential(E_mat, R_matrices, t_matrices);

                        int idx;
                        float distance = 100000;
                        // choosing the best match (the one with the less distance)
                        for (int p=0; p < filtered_matches_opencv.size(); p++) {
                            if(filtered_matches_opencv[p].distance < distance){
                                distance = filtered_matches_opencv[p].distance;
                                idx = p;
                            }
                        }
                        cv::Mat best_matched_point_prev = (cv::Mat_<double>(2,1) << keypoints_previous_i[filtered_matches_opencv[idx].queryIdx].pt.x,
                                keypoints_previous_i[filtered_matches_opencv[idx].queryIdx].pt.y);
                        cv::Mat best_matched_point_current = (cv::Mat_<double>(2,1) << keypoints_current_i[filtered_matches_opencv[idx].trainIdx].pt.x,
                                keypoints_current_i[filtered_matches_opencv[idx].trainIdx].pt.y);
                        int idx_matrices = cv::sfm::motionFromEssentialChooseSolution(R_matrices, t_matrices, intrinsics, best_matched_point_prev, intrinsics, best_matched_point_current);

                        // Getting back the Fundamental matrix from the best R and ts
                        cv::Mat projective_mat1_opencv = cv::Mat::eye(3,4,CV_64F);
                        projective_mat1_opencv = intrinsics * projective_mat1_opencv;
                        cv::Mat projective_mat2_opencv;
                        cv::sfm::projectionFromKRt(intrinsics,R_matrices[idx_matrices],t_matrices[idx_matrices],projective_mat2_opencv);

                        // Triangulation
                        std::vector< cv::Mat > matched_points_2d_opencv, matched_points_2d_multicol;
                        std::vector <cv::Point2f> filtered_points_previous_opencv, filtered_points_current_opencv;
                        std::vector <cv::Point2f> filtered_points_previous_multicol, filtered_points_current_multicol;
                        FromMatchesToVectorOfPoints(keypoints_previous_i, keypoints_current_i, filtered_matches_opencv, filtered_points_previous_opencv, filtered_points_current_opencv);
                        FromMatchesToVectorOfPoints(keypoints_previous_i, keypoints_current_i, matches_with_epipolar_constraint, filtered_points_previous_multicol, filtered_points_current_multicol);

                        // Getting the filtered points from opencv
                        GetArrayOfPoints(filtered_points_previous_opencv, filtered_points_current_opencv, matched_points_2d_opencv);
                        // Getting the filtered points from multicol-slam
                        GetArrayOfPoints(filtered_points_previous_multicol, filtered_points_current_multicol, matched_points_2d_multicol);

                        // Projection matrices
                        std::vector < cv::Mat > projection_matrices_opencv, projection_matrices_multicol;
                        projection_matrices_opencv.push_back(projective_mat1_opencv);
                        projection_matrices_opencv.push_back(projective_mat2_opencv);
                        projection_matrices_multicol.push_back(projective_mat1_multicol);
                        projection_matrices_multicol.push_back(projective_mat2_multicol);

                        // Getting 3D points
                        cv::Mat triangulated_points_opencv, triangulated_points_multicol;
                        cv::sfm::triangulatePoints(matched_points_2d_opencv,projection_matrices_opencv,triangulated_points_opencv);
                        cv::sfm::triangulatePoints(matched_points_2d_multicol, projection_matrices_multicol, triangulated_points_multicol);

                        /// SAVING THE POINTCLOUDS
                        char buffer [100];
                        std::sprintf(buffer, "/home/eduochoa/PointClouds/Boreas/PointCloud_Multicol%d.ply",point_cloud_counter);
                        ofstream outfile(buffer);
                        outfile << "ply\n" << "format ascii 1.0\n";
                        outfile << "element vertex " << triangulated_points_multicol.size().width << "\n";
                        outfile << "property float x\n" << "property float y\n" << "property float z\n";
                        outfile << "end_header\n";
                        for (int i = 0; i < triangulated_points_multicol.size().width; i++)
                        {
                            outfile << triangulated_points_multicol.at<double>(0,i) << " ";
                            outfile << triangulated_points_multicol.at<double>(1,i) << " ";
                            outfile << triangulated_points_multicol.at<double>(2,i) << " ";
                            outfile << "\n";
                        }
                        outfile.close();
                        point_cloud_counter++;

//                        ofstream outfile2("PointCloud_OpenCV.ply");
//                        outfile2 << "ply\n" << "format ascii 1.0\n";
//                        outfile2 << "element vertex " << triangulated_points_opencv.size().width << "\n";
//                        outfile2 << "property float x\n" << "property float y\n" << "property float z\n";
//                        outfile2 << "end_header\n";
//                        for (int i = 0; i < triangulated_points_opencv.size().width; i++)
//                        {
//                            outfile2 << triangulated_points_opencv.at<double>(0,i) << " ";
//                            outfile2 << triangulated_points_opencv.at<double>(1,i) << " ";
//                            outfile2 << triangulated_points_opencv.at<double>(2,i) << " ";
//                            outfile2 << "\n";
//                        }
//                        outfile2.close();
                        cv::waitKey(10);
						
					}
					
					FramesUpdate(current_keyframe_);
                    b_add_newkeyframe_ = false;
				}
				else
				{
					/*
                    cv::Vec3d t = GetTranslation(tracked_frame_pose_);
                    vector<double> q = GetRotation(tracked_frame_pose_);
                    cout <<"********COLLISION DETECTION THREAD COPIED POSE************"<<endl;
                    cout <<"********Current pose: "<< setprecision(7) << " " << t(0) << " " << t(1) << " " << t(2)
                            << " " << q[0] << " " << q[1] << " " << q[2] << " " << q[3] << endl<<endl;*/
                    //cout << tracked_frame_pose_ << endl;
				}
                //std::this_thread::sleep_for(std::chrono::milliseconds(2000));
            } 
        }

     }

    // Makes the transfer of data from Tracking Thread to Collision Detection Thread
    void CollisionDetector::TransferFrameData(cMultiFrame F, bool newdata, bool add_newkeyframe)
    {
        {

        std::unique_lock<std::mutex> lock(mMutexReceiveData);
        bnewdata_ = newdata;
        b_add_newkeyframe = add_newkeyframe;
        if(b_add_newkeyframe){
            tracked_frame = F;
            tracked_frame_pose = F.GetPose();
        }
        else {
            tracked_frame_pose = F.GetPose();
        }

        }
    }

    // Checks if new data is available for processing and copies it into the object variables
    bool CollisionDetector::CheckDataAvailability()
    {
        {

        std::unique_lock<std::mutex> lock(mMutexReceiveData);
        bool temp = bnewdata_;
        if(bnewdata_){
            tracked_frame_ = tracked_frame;
            tracked_frame_pose_ = tracked_frame_pose;
            bnewdata_ = false;
            b_add_newkeyframe_ = b_add_newkeyframe;
        }

        return temp;

        }
    }

    void CollisionDetector::AcceptMultiFrames(bool flag)
    {
        std::unique_lock<std::mutex> lock(mMutexAccept);
        fAcceptFrame = flag;
    }

    // Gets the Translation parameters of pose T
    cv::Vec3d CollisionDetector::GetTranslation(cv::Matx<double, 4, 4> &T)
    {
        return cConverter::Hom2T(T);
    }

    // Gets the rotation parameters of pose T
    vector<double> CollisionDetector::GetRotation(cv::Matx<double, 4, 4> &T)
    {
        cv::Matx33d R = cConverter::Hom2R(T);
        return cConverter::toQuaternion(R);
    }
    
    // Update the Keyframes of a moment in time
    void CollisionDetector::FramesUpdate(cMultiFrame current_frame)
    {
		previous_keyframe_ = current_frame;
	}

    // Gets the transformation matrix between contiguous frames
    void CollisionDetector::TransformationMatrixBetweenFrames(cv::Mat &camera_pose_respect_to_body, cv::Mat &previous_base_pose_respect_to_world,
                                           cv::Mat &current_base_pose_respect_to_world, cv::Mat &transformation){
        cv::Mat transformation_cam_respect_to_world_previous, transformation_cam_respect_to_world_current;

        transformation_cam_respect_to_world_previous = previous_base_pose_respect_to_world * camera_pose_respect_to_body;
        transformation_cam_respect_to_world_current = current_base_pose_respect_to_world * camera_pose_respect_to_body;

        // Getting the transformation
        transformation = transformation_cam_respect_to_world_previous.inv() * transformation_cam_respect_to_world_current;
    }

    // Computes the fundamental matrix between two frames given the transformation matrix
    void CollisionDetector::ComputeFundamentalMatrix(cv::Mat &transformation_between_frames, cv::Mat &Intrinsic_parameters, cv::Mat &fundamental_matrix){
        cv::Mat rotation = transformation_between_frames(cv::Range(0, 3), cv::Range(0, 3) );
        cv::Mat translation = (cv::Mat_<double>(3,1) << transformation_between_frames.at<double>(0,3),
                transformation_between_frames.at<double>(1,3),
                transformation_between_frames.at<double>(2,3));

        cv::Mat A = Intrinsic_parameters * rotation.t() * translation;
        cv::Mat cross_product_mat = (cv::Mat_<double>(3,3) << 0, -A.at<double>(2,0), A.at<double>(1,0),
                A.at<double>(2,0), 0, -A.at<double>(0,0),
                -A.at<double>(1,0), A.at<double>(0,0), 0);
        fundamental_matrix = (Intrinsic_parameters.inv()).t() * rotation * Intrinsic_parameters.t() * cross_product_mat;
        fundamental_matrix = fundamental_matrix/fundamental_matrix.at<double>(2,2);
    }

    // Matches keypoints of two images using epipolar constraint
    std::vector<cv::DMatch> CollisionDetector::MatchingWithEpipolarConstraint(const cv::Mat &F, const cv::Mat &image1, const cv::Mat &descriptors1, const std::vector<cv::KeyPoint> &keypoints1,
                                                           const cv::Mat &image2, const cv::Mat &descriptors2, const std::vector<cv::KeyPoint> &keypoints2){
        std::vector<cv::DMatch> matches_from12;
        std::vector<cv::Point2f> keypoints1_vector_of_pt, keypoints2_vector_of_pt;

        // Converting the keypoints into a vector of points2d
        for (int i = 0; i < keypoints1.size(); ++i) {
            keypoints1_vector_of_pt.push_back(keypoints1[i].pt);
        }
        for (int i = 0; i < keypoints2.size(); ++i) {
            keypoints2_vector_of_pt.push_back(keypoints2[i].pt);
        }

        // Computing the epipolar lines based on the F matrix
        std::vector<cv::Vec3f> epipolar_lines_on_image1, epipolar_lines_on_image2;
        cv::computeCorrespondEpilines(keypoints1_vector_of_pt,1,F,epipolar_lines_on_image2);
        cv::computeCorrespondEpilines(keypoints2_vector_of_pt,2,F,epipolar_lines_on_image1);

        assert(keypoints1_vector_of_pt.size() == epipolar_lines_on_image2.size() and
               keypoints2_vector_of_pt.size() == epipolar_lines_on_image1.size());

        // Epipolar feature matching
        const float inlier_distance = 4.f;
        const double inlier_ratio = 0.8f;
        const double inlier_cosine_threshold = 0.7f;

        std::vector<cv::DMatch> match_1to2;
        for (int i = 0; i < keypoints1.size(); ++i) {
            double best_distance_score = 100000000000;
            double second_best_distance_score = 100000000000;
            cv::DMatch match;
            for (int j = 0; j < keypoints2.size(); ++j) {
                if(DistancePointToLine(keypoints2[j].pt, epipolar_lines_on_image2[i]) < inlier_distance){
                    double cos = descriptors1.row(i).dot(descriptors2.row(j)) /
                                 (cv::norm(descriptors1.row(i)) * cv::norm(descriptors2.row(j)));
                    if(cos > inlier_cosine_threshold) {
                        double dist = cv::norm(descriptors1.row(i), descriptors2.row(j), cv::NORM_L2, cv::noArray());
                        if (dist < best_distance_score) {
                            second_best_distance_score = best_distance_score;
                            best_distance_score = dist;
                            match = cv::DMatch(i, j, (float) dist);
                        } else if (dist < second_best_distance_score) {
                            second_best_distance_score = dist;
                        }
                    }
                }
            }
            if(best_distance_score / second_best_distance_score < inlier_ratio){
                match_1to2.push_back(match);
            }
            else {
                match_1to2.push_back(cv::DMatch(-1, -1, -114514.f));
            }
        }

        std::vector<cv::DMatch> match_2to1;
        for (int i = 0; i < keypoints2.size(); ++i) {
            double best_distance_score = 100000000000;
            double second_best_distance_score = 100000000000;
            cv::DMatch match;
            for (int j = 0; j < keypoints1.size(); ++j) {
                if(DistancePointToLine(keypoints1[j].pt, epipolar_lines_on_image1[i]) < inlier_distance){
                    double cos = descriptors1.row(j).dot(descriptors2.row(i)) /
                                 (cv::norm(descriptors1.row(j)) * cv::norm(descriptors2.row(i)));
                    if(cos > inlier_cosine_threshold) {
                        double dist = cv::norm(descriptors2.row(i), descriptors1.row(j), cv::NORM_L2, cv::noArray());
                        if (dist < best_distance_score) {
                            second_best_distance_score = best_distance_score;
                            best_distance_score = dist;
                            match = cv::DMatch(i, j, (float) dist);
                        } else if (dist < second_best_distance_score) {
                            second_best_distance_score = dist;
                        }
                    }
                }
            }
            if(best_distance_score / second_best_distance_score < inlier_ratio){
                match_2to1.push_back(match);
            }
            else {
                match_2to1.push_back(cv::DMatch(-1, -1, -114514.f));
            }
        }

        // Cross check
        for (int n = 0; n < match_1to2.size(); ++n) {
            if(match_1to2[n].queryIdx != -1 and
               match_1to2[n].trainIdx != -1 and
               match_1to2[n].queryIdx == match_2to1[match_1to2[n].trainIdx].trainIdx){
                matches_from12.push_back(match_1to2[n]);
            }
        }

        return matches_from12;
    }

    // Computation of the orthogonal distance from a point to a line
    double CollisionDetector::DistancePointToLine( cv::Point2f point, cv::Vec3f epiline){
        return abs(epiline[0]*point.x + epiline[1]*point.y + epiline[2])/sqrt(pow(epiline[0],2) + pow(epiline[1],2));
    }

    // Transform a vector of DMatch into a one of Point2d
    void CollisionDetector::FromMatchesToVectorOfPoints(std::vector<cv::KeyPoint> &keypoints_frame1, std::vector<cv::KeyPoint> &keypoints_frame2,
                                     std::vector<cv::DMatch> &matches, std::vector <cv::Point2f> &points_frame1,std::vector <cv::Point2f> &points_frame2){
        for (int p = 0; p < (int)matches.size(); p++) {
            points_frame1.push_back(keypoints_frame1[matches[p].queryIdx].pt);
            points_frame2.push_back(keypoints_frame2[matches[p].trainIdx].pt);
        }
    }

    // Draws the epipolar lines from points of one image into the other one
    void CollisionDetector::DrawEpipolarlines(cv::Mat &first_image, cv::Mat &second_image, cv::Mat &lines_image,
                           std::vector <cv::Vec3f> lines){
        /// first_image will be the image on which we drwa the eiplines from the points of second_image
        /// lines are the corresponding epiline

        first_image.copyTo(lines_image);

        for (auto it = lines.begin(); it != lines.end(); ++it) {
            cv::line(lines_image, cv::Point(0,-(*it)[2]/(*it)[1]),
                     cv::Point(lines_image.cols, -((*it)[2]+(*it)[0]*lines_image.cols)/(*it)[1]),
                     cv::Scalar(255,255,255));
        }
    }

    // Creates the projection matrices out of the Rotation and translation parameters between two frames
    void CollisionDetector::ComputeProjectionMatrices(cv::Mat &camera_intrinsics, cv::Mat &transformation_mat, cv::Mat &projective1, cv::Mat &projective2){
        cv::Mat reference = cv::Mat::eye(3,4,CV_64F); // Located at frame 1 at the moment
        cv::Mat rotation = transformation_mat(cv::Range(0, 3), cv::Range(0, 3) );
        cv::Mat translation = (cv::Mat_<double>(3,1) << transformation_mat.at<double>(0,3),
                transformation_mat.at<double>(1,3),
                transformation_mat.at<double>(2,3));

        // Projective matrix comes from multiplying the intrinsics by the rotation & translation
        // if intrisics are the same (same camera) then is not needed.
        projective1 =  camera_intrinsics * reference;
        cv::sfm::projectionFromKRt(camera_intrinsics,rotation,translation,projective2);
        //projective2 =  camera_intrinsics * transformation_mat(cv::Range(0, 3), cv::Range(0, 4) );
    }

    // Transform points into a InputArrayOfArrays
    void CollisionDetector::GetArrayOfPoints(std::vector<cv::Point2f> &points_frame1, std::vector<cv::Point2f> &points_frame2, std::vector<cv::Mat> &Array_of_points){
        cv::Mat points1Mat = (cv::Mat_<double>(2,1) << points_frame1[0].x, points_frame1[0].y);
        cv::Mat points2Mat = (cv::Mat_<double>(2,1) << points_frame2[0].x, points_frame2[0].y);

        for(int i=1; i < points_frame1.size(); i++){
            cv::Mat point1 = (cv::Mat_<double>(2,1) << points_frame1[i].x, points_frame1[i].y);
            cv::Mat point2 = (cv::Mat_<double>(2,1) << points_frame2[i].x, points_frame2[i].y);
            cv::hconcat(points1Mat,point1,points1Mat);
            cv::hconcat(points2Mat,point2,points2Mat);
        }

        Array_of_points.push_back(points1Mat);
        Array_of_points.push_back(points2Mat);
    }

    double CollisionDetector::AverageCosineValueOfMatches(const std::vector<cv::DMatch> &matches, const cv::Mat &descriptors1, const cv::Mat &descriptors2){
        std::vector<double> cosine_average;
        double min_cosine = 10000000;
        double max_cosine = 0;
        for (int i = 0; i < matches.size() ; ++i) {
            int idx_desc1 = matches[i].queryIdx;
            int idx_desc2 = matches[i].trainIdx;

            double cos = descriptors1.row(idx_desc1).dot(descriptors2.row(idx_desc2)) / (cv::norm(descriptors1.row(idx_desc1)) * cv::norm(descriptors2.row(idx_desc2)));
            cosine_average.push_back(cos);

            if(cos < min_cosine){
                min_cosine = cos;
            }
            if(cos > max_cosine){
                max_cosine = cos;
            }
        }

        cout << "max cos " << max_cosine << endl;
        cout << "min cos " << min_cosine << endl;
        return accumulate( cosine_average.begin(), cosine_average.end(), 0.0)/ cosine_average.size();
    }

    cv::Mat CollisionDetector::GetIntrinsicBoreas(int cam){
        if(cam ==0){
            cv::Mat intrinsics_of_cam = (cv::Mat_<double>(3,3) << 599.33, 0, 694.545, 0 , 599.33, 513.71, 0, 0, 1);
            return intrinsics_of_cam;
        }
        else if(cam == 1){
            cv::Mat intrinsics_of_cam = (cv::Mat_<double>(3,3) << 603.01, 0, 649.05, 0 , 603.01, 529.07, 0, 0, 1);
            return intrinsics_of_cam;
        }
        else if(cam == 2){
            cv::Mat intrinsics_of_cam = (cv::Mat_<double>(3,3) << 598.725, 0, 674.795, 0 , 598.725, 517.895, 0, 0, 1);
            return intrinsics_of_cam;
        }
        else if(cam == 3){
            cv::Mat intrinsics_of_cam = (cv::Mat_<double>(3,3) << 592.015, 0, 664.99, 0 , 592.015, 526.025, 0, 0, 1);
            return intrinsics_of_cam;
        }
        else if(cam == 4){
            cv::Mat intrinsics_of_cam = (cv::Mat_<double>(3,3) << 610.08, 0, 677.49, 0 , 610.08, 501.44, 0, 0, 1);
            return intrinsics_of_cam;
        }
        else{
            cv::Mat intrinsics_of_cam;
            return intrinsics_of_cam;
        }
    }
/*
 *     bool CollisionDetector::CheckNewKeyFrame()
    {
        {
        std::unique_lock<std::mutex> lock(mMutexReceiveData);
        bool temp = bNewKF;
        bNewKF = false;
        return temp;
        }
    }
        cv::Matx<double, 4, 4> CollisionDetector::GetTransformation()
        {
            std::unique_lock<std::mutex> lock(mMutexReceiveP);
            cv::Matx<double, 4, 4> T = FramePoses.front();
            FramePoses.pop_front();
            return T;
        }

    void CollisionDetector::NewKeyFrameData(cMultiFrame pkF)
    {
        std::unique_lock<std::mutex> lock(mMutexReceiveF);
        MultiKeyFrames.push_back(pkF);
        AcceptMultiFrames(false);
    }

    void CollisionDetector::CreatingKeyFrame()
    {
        std::unique_lock<std::mutex> lock(NewMKF);
        NewKFflag = true;
    }
*/

}
