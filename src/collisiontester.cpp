// Source code to test the acquisition of tracked images and its processing in the Collision
// detection thread. The main goal is to acquire the image info and its tracked posed to
// perform triangulation and create a local depth map.

#include <iostream>
#include <cmath>
#include <cstdlib>

// OpenCV
#include <opencv2/opencv.hpp>

// Header
#include "collisiontester.h"

using namespace cv::xfeatures2d;

int main (int argc, char**  argv){

    if(argc !=2){
        cerr << "The input file was not given." << endl;
        return 1;
    }

    // Loading the contents of the given file, which should include the path to the images and the
    // poses that were tracked by the tracking thread
    string images_and_poses_file = string(argv[1]);
    ifstream information_file; // Operates on files

    information_file.open(images_and_poses_file.c_str()); //= cv::Mat::zeros(4,4, CV_64F);
    string line;

    vector<string> current_image_names(3); // Number of camera images.
    vector<string> previous_image_names(3);
    cv::Mat previous_tracked_pose;

    // Camera matrix which defines the transformation from body to camera frame
    cv::Mat Mc_camera0 = (cv::Mat_<double>(4,4) << 0.003011860141974995, -0.003147042978819237, 0.9999905123644797, 0.04120206087827682,
                          0.002174760587811114, 0.9999927038122716, 0.003140499738587044, -0.001059569884091616,
                          -0.9999930995336272, 0.002165281208486779, 0.003018682231916471, -0.06195668503642082,
                          0, 0, 0, 1);
    // Intrinsic parameters of the camera 0 for the resized images
    cv::Mat K_camera0 = (cv::Mat_<double>(3,3) << 313.5, 0, 404, 0 , 313.5, 308, 0, 0, 1);


    // Reading the file
    //while(std::getline(information_file,line)){
    for (int n = 0 ; n < 700 ; n++) {
        std::getline(information_file,line);
        cv::Mat current_tracked_pose;
        bool emptyFlag = false;
        double number;
        char read_comma,read_braces;

        std::istringstream iss(line);

       // Stores the path to the images
       if(!(iss >> current_image_names[0] >> current_image_names[1] >> current_image_names[2])){
           emptyFlag = true;
       }

       // Extracting the numeric information of the txt file into a Mat representation.
       // Catching the square initial square bracket
       iss >> read_braces;
       // Storing the matrix info
       int get_matrix_data = 0;
       while (get_matrix_data < 4) {
           for (int get_column_info = 0; get_column_info < 4; get_column_info++) {
               iss >> number >> read_comma;
               current_tracked_pose.push_back((double)number);
           }

           if(get_matrix_data < 3){
               std::getline(information_file,line); // Reads the next line of numbers as the saving format has
                                                    // an \n character that separates the matrix rows.
           }
           iss.clear();
           iss.str(line); // reading the line
           get_matrix_data++;
       }

       if(n==300 || n==320){
           current_tracked_pose = current_tracked_pose.reshape(1,4);

           // Checking that there is already two frames that have been received from the system
           // if(baseline < threshold){ Could be a condition, by evaluating the translation of the base and take the Euclidean distance as the baseline
           if(!previous_tracked_pose.empty()){
               // Windows for visualizing the feature extraction process
               cv::namedWindow("Epipolar lines in current frame - F OpenCV", CV_WINDOW_NORMAL);
               cv::resizeWindow("Epipolar lines in current frame - F OpenCV",600, 400);
               cv::namedWindow("Epipolar lines in current frame - F Multicol", CV_WINDOW_NORMAL);
               cv::resizeWindow("Epipolar lines in current frame - F Multicol",600, 400);
               cv::namedWindow("Matches after filtering - MultiCol", CV_WINDOW_NORMAL);
               cv::resizeWindow("Matches after filtering - MultiCol",1000, 600);
               cv::namedWindow("Matches after filtering - OpenCV", CV_WINDOW_NORMAL);
               cv::resizeWindow("Matches after filtering - OpenCV",1000, 600);

               // Loading the images of the camera 0
               cv::Mat previous_image_cam0_original = cv::imread(previous_image_names[0],CV_LOAD_IMAGE_GRAYSCALE);
               cv::Mat current_image_cam0_original = cv::imread(current_image_names[0],CV_LOAD_IMAGE_GRAYSCALE);

               // Pre-processing images
               cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
               clahe->setClipLimit(2);

               cv::Mat previous_image_cam0, current_image_cam0;
               clahe->apply(previous_image_cam0_original, previous_image_cam0);
               clahe->apply(current_image_cam0_original, current_image_cam0);

               /// FEATURE DETECTION
               // Detect keypoints and compute the descriptors
               // Implementing different types of feature descriptors from OpenCv
               std::vector<cv::KeyPoint> keypoints_previous, keypoints_current;
               cv::Mat descriptors_previous, descriptors_current;
               string detector_type = "SIFT";
               if(detector_type == "SURF"){
                   int minHessian = 100; // Used for SURF Detector as a threshold (larger is more restrictive)
                   cv::Ptr<SURF> detector = SURF::create(minHessian,4,3,false);

                   detector->detectAndCompute(previous_image_cam0, cv::Mat(), keypoints_previous, descriptors_previous);
                   detector->detectAndCompute(current_image_cam0, cv::Mat(), keypoints_current, descriptors_current);
               }
               else if (detector_type == "SIFT") {
                   cv::Ptr<SIFT> detector = SIFT::create(10000,1,0.01,10,1.1);

                   detector->detectAndCompute(previous_image_cam0, cv::Mat(), keypoints_previous, descriptors_previous);
                   detector->detectAndCompute(current_image_cam0, cv::Mat(), keypoints_current, descriptors_current);
               }

               cout << "Features " << keypoints_previous.size() << " " << keypoints_current.size() << endl;

               /// FEATURE MATCHING
               // Matching the descriptors using FLANN matcher
               cv::Ptr<cv::DescriptorMatcher> matcher = cv::DescriptorMatcher::create(cv::DescriptorMatcher::FLANNBASED);
               std::vector < std::vector<cv::DMatch> > knn_matches;
               matcher->knnMatch(descriptors_previous, descriptors_current, knn_matches, 2);

               // Pre-filtering of matches using Lowe's ratio test
               std::vector< cv::DMatch > best_matches;
               const float ratio_thresh = 1.f;
               for (size_t i = 0; i < knn_matches.size(); i++)
               {
                   if (knn_matches[i][0].distance < ratio_thresh * knn_matches[i][1].distance)
                   {
                       best_matches.push_back(knn_matches[i][0]);
                   }
               }

               cout << "matches " << best_matches.size() <<endl;

               // Filtering the matches using the motion model
               // Getting the transformation matrix between the frames
               cv::Mat T2_respect_1(4,4,CV_64F), T1_respect_2;
               TransformationMatrixBetweenFrames(Mc_camera0, previous_tracked_pose, current_tracked_pose, T2_respect_1);
               T2_respect_1.copyTo(T1_respect_2);
               T1_respect_2 = T1_respect_2.inv(); // Transformation of frame 1 respect to frame 2

               // Transforming the Rotation and translation parameters to obtain the Fundamental Matrix
               cv::Mat F_matrix_multicol(3,3,CV_64F);
               cv::Mat lines_on_previous, lines_on_current,lines_on_current2; // New images with the plotted epipolar lines
               ComputeFundamentalMatrix(T1_respect_2, K_camera0, F_matrix_multicol);

               /// EPIPOLAR MATCHING
               // DEBUG: Matching using epipolar constraint
               std::vector<cv::DMatch> matches_with_epipolar_constraint = MatchingWithEpipolarConstraint(F_matrix_multicol, previous_image_cam0,
                                                                                                         descriptors_previous, keypoints_previous, current_image_cam0, descriptors_current, keypoints_current);
               cv::Mat img_matches_multicol;
               cv::drawMatches( previous_image_cam0, keypoints_previous, current_image_cam0,
                                keypoints_current, matches_with_epipolar_constraint, img_matches_multicol,  cv::Scalar::all(-1),
                               cv::Scalar::all(-1), std::vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
               cv::imshow( "Matches after filtering - MultiCol", img_matches_multicol );
               cout << "Multicol matches with epipolar "<< matches_with_epipolar_constraint.size() << endl;

               std::vector <cv::Point2f> points_previous, points_current;
               FromMatchesToVectorOfPoints(keypoints_previous, keypoints_current, best_matches,points_previous,points_current);

               // Computing the fundamental matrix using OpenCV to compare the methods
               cv::Mat outliers;
               cv::Mat F_matrix_opencv = cv::findFundamentalMat(points_previous, points_current, cv::FM_RANSAC, 2, 0.99, outliers);

               // Drawing epipolar lines into the images for debugging
               std::vector <cv::Vec3f> lines_previous, lines_current, lines_current2;
               cv::computeCorrespondEpilines(points_previous, 1, F_matrix_multicol, lines_current);
               DrawEpipolarlines(current_image_cam0, previous_image_cam0, lines_on_current, lines_current);
               cv::computeCorrespondEpilines(points_previous, 1, F_matrix_opencv, lines_current2); // Using F calculation from OpenCv
               DrawEpipolarlines(current_image_cam0, previous_image_cam0, lines_on_current2, lines_current2);

               // Distance from point to Epiline calculation
               cv::Mat distances_to_epilines_OpencV, distances_to_epilines_Multicol;
               for (int l=0; l < points_previous.size(); l++) {
                   double distance_opencvF = DistancePointToLine(points_current[l],lines_current2[l]);
                   double distance_multicol = DistancePointToLine(points_current[l],lines_current[l]);
                   distances_to_epilines_OpencV.push_back(distance_opencvF);
                   distances_to_epilines_Multicol.push_back(distance_multicol);
               }
               // Saving the files to read them in Matlab as csv
               //writeCSV("Distances_to_epipolar_Multicol1", distances_to_epilines_Multicol);
               //writeCSV("Distances_to_epipolar_linesOpenCV", distances_to_epilines_OpencV);

               // Filtering the matches by using the distance from point to line
               int pixels_distance_threshold = 4;
               std::vector< cv::DMatch > filtered_matches_multicol, filtered_matches_opencv;
               for (int idx=0; idx < points_previous.size(); idx++) {
                   //debug variables
                   /*cv::Mat new_image;
                   cv::Mat img_matches_test;
                   std::vector <cv::Vec3f> lines;
                   std::vector< cv::DMatch > filtered_matches;*/

                   if(distances_to_epilines_Multicol.at<double>(idx) <= pixels_distance_threshold){
                       filtered_matches_multicol.push_back(best_matches[idx]);

                       // debug
                       /*filtered_matches.push_back(best_matches[idx]);
                       lines.push_back(lines_current[idx]);
                       DrawEpipolarlines(current_image_cam0, previous_image_cam0, new_image, lines);
                       cv::drawMatches( previous_image_cam0, keypoints_previous, new_image,
                                        keypoints_current, filtered_matches, img_matches_test,  cv::Scalar::all(-1),
                                        cv::Scalar::all(-1), std::vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
                       cv::imshow( "Matches after filtering - MultiCol", img_matches_test );
                       cv::waitKey(30);
                       cin.ignore();*/

                   }
                   if(outliers.at<uchar>(idx)){
                       filtered_matches_opencv.push_back(best_matches[idx]);

                   }
               }

               cout << "multicol matches with FLANN" << filtered_matches_multicol.size() <<endl;
               cout << "opencv matches " << filtered_matches_opencv.size() <<endl;

               // Check by drawing the points
               //DrawKeyPointsInImage(points_current, lines_on_current);
               //DrawKeyPointsInImage(points_current, lines_on_current2);

               cv::imshow("Epipolar lines in current frame - F OpenCV", lines_on_current2);
               cv::imshow("Epipolar lines in current frame - F Multicol", lines_on_current);

               // Drawing and showing detected matches
               //cv::Mat img_matches_multicol, img_matche_opencv;
               cv::Mat img_matche_opencv;
               /*cv::drawMatches( previous_image_cam0, keypoints_previous, current_image_cam0,
                                keypoints_current, filtered_matches_multicol, img_matches_multicol,  cv::Scalar::all(-1),
                               cv::Scalar::all(-1), std::vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);*/
               cv::drawMatches( previous_image_cam0, keypoints_previous, current_image_cam0,
                                keypoints_current, filtered_matches_opencv, img_matche_opencv,  cv::Scalar::all(-1),
                                cv::Scalar::all(-1), std::vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

               /*for (int j = 0; j < (int)best_matches.size(); j++) {
                 printf("Matching [%d] previous keypoint: %d -- current keypoint: %d \n",j, best_matches[j].queryIdx,
                 best_matches[j].trainIdx);
                 }*/

               //cv::imshow( "Matches after filtering - MultiCol", img_matches_multicol );
               cv::imshow( "Matches after filtering - OpenCV", img_matche_opencv );

               // Drawing keypoints
               cv::Mat previous_img_keypoints, current_img_keypoints;
               cv::drawKeypoints(previous_image_cam0, keypoints_previous, previous_img_keypoints, cv::Scalar::all(-1), cv::DrawMatchesFlags::DEFAULT);
               cv::drawKeypoints(current_image_cam0, keypoints_current, current_img_keypoints, cv::Scalar::all(-1), cv::DrawMatchesFlags::DEFAULT);

               cv::imshow("Previous frame - camera 0", previous_img_keypoints);
               cv::imshow("Current frame - camera 0", current_img_keypoints);

               /// TRIANGULATION STEP
               // Beginning the triangulation process
               // Find the projective matrices
               // From OpenCV
               cv::Mat projective_mat1_multicol, projective_mat2_multicol;
               //cv::Mat projective_mat1_opencv, projective_mat2_opencv;
               //cv::sfm::projectionsFromFundamental(F_matrix_opencv, projective_mat1_opencv, projective_mat2_opencv);

               // From Multicol-SLAM
               ComputeProjectionMatrices(K_camera0, T1_respect_2, projective_mat1_multicol, projective_mat2_multicol); // Transformation used is from frame 1 to frame 2
                                                                                                                           // so the origin is assumed at frame 1
               // Debugging of F matrix from OpenCV
               cv::Mat E_mat;
               cv::sfm::essentialFromFundamental(F_matrix_opencv, K_camera0, K_camera0, E_mat);
               std::vector<cv::Mat> R_matrices, t_matrices;

               cv::sfm::motionFromEssential(E_mat, R_matrices, t_matrices);

               int idx;
               float distance = 100;
               // choosing the best match (the one with the less distance)
               for (int p=0; p < filtered_matches_opencv.size(); p++) {
                   if(filtered_matches_opencv[p].distance < distance){
                       distance = filtered_matches_opencv[p].distance;
                       idx = p;
                   }
               }
               cv::Mat best_matched_point_prev = (cv::Mat_<double>(2,1) << keypoints_previous[filtered_matches_opencv[idx].queryIdx].pt.x,
                       keypoints_previous[filtered_matches_opencv[idx].queryIdx].pt.y);
               cv::Mat best_matched_point_current = (cv::Mat_<double>(2,1) << keypoints_current[filtered_matches_opencv[idx].trainIdx].pt.x,
                       keypoints_current[filtered_matches_opencv[idx].trainIdx].pt.y);
               int idx_matrices = cv::sfm::motionFromEssentialChooseSolution(R_matrices, t_matrices, K_camera0, best_matched_point_prev, K_camera0, best_matched_point_current);

               // Getting back the Fundamental matrix from the best R and ts
               cv::Mat projective_mat1_opencv = cv::Mat::eye(3,4,CV_64F);
               projective_mat1_opencv = K_camera0 * projective_mat1_opencv;
               cv::Mat projective_mat2_opencv;
               cv::sfm::projectionFromKRt(K_camera0,R_matrices[idx_matrices],t_matrices[idx_matrices],projective_mat2_opencv);


               // Triangulation
               std::vector< cv::Mat > matched_points_2d_opencv, matched_points_2d_multicol;
               std::vector <cv::Point2f> filtered_points_previous_opencv, filtered_points_current_opencv;
               std::vector <cv::Point2f> filtered_points_previous_multicol, filtered_points_current_multicol;
               FromMatchesToVectorOfPoints(keypoints_previous, keypoints_current, filtered_matches_opencv, filtered_points_previous_opencv, filtered_points_current_opencv);
               FromMatchesToVectorOfPoints(keypoints_previous, keypoints_current, matches_with_epipolar_constraint, filtered_points_previous_multicol, filtered_points_current_multicol);

               // Getting the filtered points from opencv
               GetArrayOfPoints(filtered_points_previous_opencv, filtered_points_current_opencv, matched_points_2d_opencv);
               // Getting the filtered points from multicol-slam
               GetArrayOfPoints(filtered_points_previous_multicol, filtered_points_current_multicol, matched_points_2d_multicol);

               // Projection matrices
               std::vector < cv::Mat > projection_matrices_opencv, projection_matrices_multicol;
               projection_matrices_opencv.push_back(projective_mat1_opencv);
               projection_matrices_opencv.push_back(projective_mat2_opencv);
               projection_matrices_multicol.push_back(projective_mat1_multicol);
               projection_matrices_multicol.push_back(projective_mat2_multicol);

               // Getting 3D points
               cv::Mat triangulated_points_opencv, triangulated_points_multicol;
               cv::sfm::triangulatePoints(matched_points_2d_opencv,projection_matrices_opencv,triangulated_points_opencv);
               cv::sfm::triangulatePoints(matched_points_2d_multicol, projection_matrices_multicol, triangulated_points_multicol);

               /// SAVING THE POINTCLOUDS
               ofstream outfile("PointCloud_Multicol.ply");
               /*outfile << "ply\n" << "format ascii 1.0\n" << "comment VTK generated PLY File\n";
               outfile << "obj_info vtkPolyData points and polygons : vtk4.0\n" << "element vertex " << pointCloud.size() << "\n";*/
               outfile << "ply\n" << "format ascii 1.0\n";
               //outfile << "element vertex " << points3d.size() << "\n";
               outfile << "element vertex " << triangulated_points_multicol.size().width << "\n";
               outfile << "property float x\n" << "property float y\n" << "property float z\n";
               outfile << "end_header\n";
               for (int i = 0; i < triangulated_points_multicol.size().width; i++)
               //for (int i = 0; i < points3d.size(); i++)
               {
                   outfile << triangulated_points_multicol.at<double>(0,i) << " ";
                   outfile << triangulated_points_multicol.at<double>(1,i) << " ";
                   outfile << triangulated_points_multicol.at<double>(2,i) << " ";

                   //outfile << points3d[i].x << " ";
                   //outfile << points3d[i].y << " ";
                   //outfile << points3d[i].z << " ";
                   outfile << "\n";
               }
               outfile.close();

               ofstream outfile2("PointCloud_OpenCV.ply");
               outfile2 << "ply\n" << "format ascii 1.0\n";
               outfile2 << "element vertex " << triangulated_points_opencv.size().width << "\n";
               outfile2 << "property float x\n" << "property float y\n" << "property float z\n";
               outfile2 << "end_header\n";
               for (int i = 0; i < triangulated_points_opencv.size().width; i++)
               {
                   outfile2 << triangulated_points_opencv.at<double>(0,i) << " ";
                   outfile2 << triangulated_points_opencv.at<double>(1,i) << " ";
                   outfile2 << triangulated_points_opencv.at<double>(2,i) << " ";
                   outfile2 << "\n";
               }
               outfile2.close();

               cv::waitKey(0);
               cin.ignore();
           }

           FramesUpdate(current_image_names, previous_image_names, current_tracked_pose, previous_tracked_pose);
       }

    }

    return 0;
}


void FramesUpdate(vector<string> current_frame, vector<string> &previous_frame,
                  cv::Mat current_pose, cv::Mat &previous_pose){
    previous_frame = current_frame;
    previous_pose = current_pose;
}

// Gets the transformation matrix between contiguous frames
void TransformationMatrixBetweenFrames(cv::Mat &camera_pose_respect_to_body, cv::Mat &previous_base_pose_respect_to_world,
                                       cv::Mat &current_base_pose_respect_to_world, cv::Mat &transformation){
    cv::Mat transformation_cam_respect_to_world_previous, transformation_cam_respect_to_world_current;

    transformation_cam_respect_to_world_previous = previous_base_pose_respect_to_world * camera_pose_respect_to_body;
    transformation_cam_respect_to_world_current = current_base_pose_respect_to_world * camera_pose_respect_to_body;

    // Getting the transformation
    transformation = transformation_cam_respect_to_world_previous.inv() * transformation_cam_respect_to_world_current;
}

// Computes the fundamental matrix between two frames given the transformation matrix
void ComputeFundamentalMatrix(cv::Mat &transformation_between_frames, cv::Mat &Intrinsic_parameters, cv::Mat &fundamental_matrix){
    cv::Mat rotation = transformation_between_frames(cv::Range(0, 3), cv::Range(0, 3) );
    cv::Mat translation = (cv::Mat_<double>(3,1) << transformation_between_frames.at<double>(0,3),
                           transformation_between_frames.at<double>(1,3),
                           transformation_between_frames.at<double>(2,3));

    cv::Mat A = Intrinsic_parameters * rotation.t() * translation;
    cv::Mat cross_product_mat = (cv::Mat_<double>(3,3) << 0, -A.at<double>(2,0), A.at<double>(1,0),
                                 A.at<double>(2,0), 0, -A.at<double>(0,0),
                                 -A.at<double>(1,0), A.at<double>(0,0), 0);
    fundamental_matrix = (Intrinsic_parameters.inv()).t() * rotation * Intrinsic_parameters.t() * cross_product_mat;
    fundamental_matrix = fundamental_matrix/fundamental_matrix.at<double>(2,2);
}

// Draws the epipolar lines from points of one image into the other one
void DrawEpipolarlines(cv::Mat &first_image, cv::Mat &second_image, cv::Mat &lines_image,
                       std::vector <cv::Vec3f> lines){
    /// first_image will be the image on which we drwa the eiplines from the points of second_image
    /// lines are the corresponding epiline

    first_image.copyTo(lines_image);

    for (auto it = lines.begin(); it != lines.end(); ++it) {
        cv::line(lines_image, cv::Point(0,-(*it)[2]/(*it)[1]),
                cv::Point(lines_image.cols, -((*it)[2]+(*it)[0]*lines_image.cols)/(*it)[1]),
                cv::Scalar(255,255,255));
    }
}

// Computation of the orthogonal distance from a point to a line
double DistancePointToLine( cv::Point2f point, cv::Vec3f epiline){
    return abs(epiline[0]*point.x + epiline[1]*point.y + epiline[2])/sqrt(pow(epiline[0],2) + pow(epiline[1],2));
}

// Draws the points given into the image
void DrawKeyPointsInImage(std::vector <cv::Point2f> &point_image2, cv::Mat& image2){
    auto it = point_image2.begin();
    cv::Scalar color(0,0,255);
    cv::cvtColor(image2, image2, CV_GRAY2BGR);
    while (it != point_image2.end()) {
        cv::circle(image2,*it,3, color,2);
        it++;
    }
}

// Creates the projection matrices out of the Rotation and translation parameters between two frames
void ComputeProjectionMatrices(cv::Mat &camera_intrinsics, cv::Mat &transformation_mat, cv::Mat &projective1, cv::Mat &projective2){
    cv::Mat reference = cv::Mat::eye(3,4,CV_64F); // Located at frame 1 at the moment
    cv::Mat rotation = transformation_mat(cv::Range(0, 3), cv::Range(0, 3) );
    cv::Mat translation = (cv::Mat_<double>(3,1) << transformation_mat.at<double>(0,3),
                           transformation_mat.at<double>(1,3),
                           transformation_mat.at<double>(2,3));

    // Projective matrix comes from multiplying the intrinsics by the rotation & translation
    // if intrisics are the same (same camera) then is not needed.
    projective1 =  camera_intrinsics * reference;
    cv::sfm::projectionFromKRt(camera_intrinsics,rotation,translation,projective2);
    //projective2 =  camera_intrinsics * transformation_mat(cv::Range(0, 3), cv::Range(0, 4) );
}

// Transform a vector of DMatch into a one of Point2d
void FromMatchesToVectorOfPoints(std::vector<cv::KeyPoint> &keypoints_frame1, std::vector<cv::KeyPoint> &keypoints_frame2,
                                 std::vector<cv::DMatch> &matches, std::vector <cv::Point2f> &points_frame1,std::vector <cv::Point2f> &points_frame2){
    for (int p = 0; p < (int)matches.size(); p++) {
        points_frame1.push_back(keypoints_frame1[matches[p].queryIdx].pt);
        points_frame2.push_back(keypoints_frame2[matches[p].trainIdx].pt);
    }
}

// Transform points into a InputArrayOfArrays
void GetArrayOfPoints(std::vector<cv::Point2f> &points_frame1, std::vector<cv::Point2f> &points_frame2, std::vector<cv::Mat> &Array_of_points){
    cv::Mat points1Mat = (cv::Mat_<double>(2,1) << points_frame1[0].x, points_frame1[0].y);
    cv::Mat points2Mat = (cv::Mat_<double>(2,1) << points_frame2[0].x, points_frame2[0].y);

    for(int i=1; i < points_frame1.size(); i++){
        cv::Mat point1 = (cv::Mat_<double>(2,1) << points_frame1[i].x, points_frame1[i].y);
        cv::Mat point2 = (cv::Mat_<double>(2,1) << points_frame2[i].x, points_frame2[i].y);
        cv::hconcat(points1Mat,point1,points1Mat);
        cv::hconcat(points2Mat,point2,points2Mat);
    }

    Array_of_points.push_back(points1Mat);
    Array_of_points.push_back(points2Mat);

    //cout << "Array " << Array_of_points[0] << endl;
}

// Matches keypoints of two images using epipolar constraint
std::vector<cv::DMatch> MatchingWithEpipolarConstraint(const cv::Mat &F, const cv::Mat &image1, const cv::Mat &descriptors1, const std::vector<cv::KeyPoint> &keypoints1,
                                                       const cv::Mat &image2, const cv::Mat &descriptors2, const std::vector<cv::KeyPoint> &keypoints2){
    std::vector<cv::DMatch> matches_from12;
    std::vector<cv::Point2f> keypoints1_vector_of_pt, keypoints2_vector_of_pt;

    // Converting the keypoints into a vector of points2d
    for (int i = 0; i < keypoints1.size(); ++i) {
        keypoints1_vector_of_pt.push_back(keypoints1[i].pt);
    }
    for (int i = 0; i < keypoints2.size(); ++i) {
        keypoints2_vector_of_pt.push_back(keypoints2[i].pt);
    }

    // Computing the epipolar lines based on the F matrix
    std::vector<cv::Vec3f> epipolar_lines_on_image1, epipolar_lines_on_image2;
    cv::computeCorrespondEpilines(keypoints1_vector_of_pt,1,F,epipolar_lines_on_image2);
    cv::computeCorrespondEpilines(keypoints2_vector_of_pt,2,F,epipolar_lines_on_image1);

    assert(keypoints1_vector_of_pt.size() == epipolar_lines_on_image2.size() and
           keypoints2_vector_of_pt.size() == epipolar_lines_on_image1.size());

    // Epipolar feature matching
    const float inlier_distance = 4.f;
    const double inlier_ratio = 0.8f;

    std::vector<cv::DMatch> match_1to2;
    for (int i = 0; i < keypoints1.size(); ++i) {
        double best_distance_score = 100000000000;
        double second_best_distance_score = 100000000000;
        cv::DMatch match;
        for (int j = 0; j < keypoints2.size(); ++j) {
            if(DistancePointToLine(keypoints2[j].pt, epipolar_lines_on_image2[i]) < inlier_distance){
                double dist = cv::norm(descriptors1.row(i), descriptors2.row(j), cv::NORM_L2, cv::noArray());
                if(dist < best_distance_score){
                    best_distance_score = dist;
                    match = cv::DMatch(i,j, (float)dist);
                }
                else if(dist < second_best_distance_score){
                    second_best_distance_score = dist;
                }
            }
        }
        if(best_distance_score / second_best_distance_score < inlier_ratio){
            match_1to2.push_back(match);
        }
        else {
            match_1to2.push_back(cv::DMatch(-1, -1, -114514.f));
        }
    }

    std::vector<cv::DMatch> match_2to1;
    for (int i = 0; i < keypoints2.size(); ++i) {
        double best_distance_score = 100000000000;
        double second_best_distance_score = 100000000000;
        cv::DMatch match;
        for (int j = 0; j < keypoints1.size(); ++j) {
            if(DistancePointToLine(keypoints1[j].pt, epipolar_lines_on_image1[i]) < inlier_distance){
                double dist = cv::norm(descriptors2.row(i), descriptors1.row(j), cv::NORM_L2, cv::noArray());
                if(dist < best_distance_score){
                    best_distance_score = dist;
                    match = cv::DMatch(i,j, (float)dist);
                }
                else if(dist < second_best_distance_score){
                    second_best_distance_score = dist;
                }
            }
        }
        if(best_distance_score / second_best_distance_score < inlier_ratio){
            match_2to1.push_back(match);
        }
        else {
            match_2to1.push_back(cv::DMatch(-1, -1, -114514.f));
        }
    }

    // Cross check
    for (int n = 0; n < match_1to2.size(); ++n) {
        if(match_1to2[n].queryIdx != -1 and
                match_1to2[n].trainIdx != -1 and
                match_1to2[n].queryIdx == match_2to1[match_1to2[n].trainIdx].trainIdx){
            matches_from12.push_back(match_1to2[n]);
        }
    }

    return matches_from12;
}
