#include <opencv2/opencv.hpp>
#include <iostream>
#include <chrono>
#include "LadybugDriver.h"


// Camera parameters.

#define VIDEO_MODE DC1394_VIDEO_MODE_FORMAT7_7
#define OPERATION_MODE DC1394_OPERATION_MODE_1394B
#define ISO_SPEED DC1394_ISO_SPEED_800
#define BPP 8160
#define DMA_RING_BUFFER 1 // Buffer of images
#define WIDTH 808
#define HEIGHT                                                                 \
  5000 // MAX 14784   No more fps smaller than 5000. Increase, decrease related
       // to fps and jpeg quality.

/*
DriverJpeg::DriverJpeg(int argc, char **argv)
    : saveImagesThread_(boost::bind(&DriverJpeg::save_images, this)) {
  directory_ = argv[1]; // Relative path to the directory
  time_ = atof(argv[2]);
  buffer_full_ = 0;
}
*/

namespace MultiColSLAM {

    DriverJpeg::DriverJpeg(int num_cams)
        : b_new_data_(false)
    {
        //: saveImagesThread_(boost::bind(&DriverJpeg::save_images, this)) {
        buffer_full_ = 0;
        number_of_cams_ = num_cams;
        images_.resize(num_cams);
    }

    DriverJpeg::~DriverJpeg() {}

    void DriverJpeg::Stop(){
        stop_execution = true;
    }

    bool DriverJpeg::StopRequested(){
        return stop_execution;
    }

    int DriverJpeg::Run() {

        // DEBUG to visualize the current frame of the camera
        //cv::namedWindow("Preview Recon", CV_WINDOW_NORMAL);
        //cv::resizeWindow("Preview Recon",600, 400);

        //cv::namedWindow("Preview Undist", CV_WINDOW_NORMAL);
        //cv::resizeWindow("Preview Undist",600, 400);

        dc1394error_t err;
        dc1394camera_t *camera;
        dc1394_t *d;
        dc1394camera_list_t *list;

        d = dc1394_new();
        if (!d) return 1;
        err = dc1394_camera_enumerate(d, &list);
        DC1394_ERR_RTN(err, "Failed to enumerate cameras");

        if (list->num == 0) {
            dc1394_log_error("No cameras found");
            return 1;
        }

        camera = dc1394_camera_new(d, list->ids[0].guid);
        if (!camera) {
            dc1394_log_error("Failed to initialize camera with guid %llx",
                         list->ids[0].guid);
            return 1;
        }
        dc1394_camera_free_list(list);
        printf("Using camera \"%s %s\"\n", camera->vendor, camera->model);

        // setup video mode, etc...
        err = dc1394_video_set_operation_mode(camera, OPERATION_MODE);
        DC1394_ERR_RTN(err, "Could not set B mode");
        err = dc1394_video_set_iso_speed(camera, ISO_SPEED);
        DC1394_ERR_RTN(err, "Could not set 800Mbps speed");
        err = dc1394_video_set_mode(camera, VIDEO_MODE);
        DC1394_ERR_RTN(err, "Could not set video mode");

        err = dc1394_format7_set_roi(camera, VIDEO_MODE, DC1394_COLOR_CODING_MONO8,
                                   BPP, 0, 0, WIDTH, HEIGHT);
        DC1394_ERR_RTN(err, "Could not set ROI");

        // setup capture
        err = dc1394_capture_setup(camera, DMA_RING_BUFFER,
                                 DC1394_CAPTURE_FLAGS_DEFAULT);
        DC1394_ERR_RTN(err, "Could not setup capture");
        err = dc1394_video_set_transmission(camera, DC1394_ON);
        DC1394_ERR_RTN(err, "Could not start transmission");

        double total_time = 0;

        // Loading camera calibration parameters used to undistort images
        LoadCameraConvertionParameters();

        // DEBUG
        bool first_image = true;
        double prev_ladybug,start_ladybug;
        total_diff_ = 0;       // Difference of time

         //while (total_time < time_ & !finished) {
        while (!StopRequested()) {
            dc1394video_frame_t *frame;
            // capture frame
            dc1394_capture_dequeue(camera, DC1394_CAPTURE_POLICY_WAIT, &frame);
            DC1394_ERR_RTN(err, "Could not dequeue a frame")

            // Save first timestamp as laduybug does not give absoulte time.
            if (first_image) {
                time_start_ = (double)frame->timestamp / 1000000;
                //first_image = false;
            }

            mbuffer_access.lock();
/*	    
            // Get Ladybug timestamp
	    unsigned int timestamp_address;
            timestamp_address = 0x0018;
            unsigned int seconds = (((unsigned int)*(frame->image + timestamp_address)) << 24) +
                                   (((unsigned int)*(frame->image + timestamp_address + 1)) << 16) +
                                   (((unsigned int)*(frame->image + timestamp_address + 2)) << 8) +
                                   (((unsigned int)*(frame->image + timestamp_address + 3)));
	    std::cout << "sec "<< seconds << std::endl;

            timestamp_address = 0x001C;
            unsigned int microseconds = (((unsigned int)*(frame->image + timestamp_address)) << 24) +
                                        (((unsigned int)*(frame->image + timestamp_address + 1)) << 16) +
                                        (((unsigned int)*(frame->image + timestamp_address + 2)) << 8) +
                                        (((unsigned int)*(frame->image + timestamp_address + 3)));
	    std::cout << "micsec "<< microseconds << std::endl;
            double timestamp_ladybug = seconds + microseconds / 1000000.0;
	    std::cout << "timestamp "<< timestamp_ladybug << std::endl;

            if (first_image) {
              start_ladybug = timestamp_ladybug; // We keep a copy of first (non
                                                 // absoulte ladybug timestamp)
              timestamp_ladybug = time_start_;
              first_image = false;
            } else {
              timestamp_ladybug += time_start_ - start_ladybug;
            }
	    
	    std::cout << "timestamp modified "<< timestamp_ladybug << std::endl;
	    std::cout << "start "<< time_start_ << std::endl;
	    std::cout << "start lady  "<< start_ladybug << std::endl;
            // Saving the current timestamp
            current_frame_timestamp_ = timestamp_ladybug;
            total_diff_ = timestamp_ladybug - time_start_;
            prev_ladybug = timestamp_ladybug;*/


	    // Getting the RGGB images from all of the selected cameras
            unsigned int jpgadr, jpgsize, channel_address;
            bool cameras_data_ok = true;

	    cv::Mat image_reconstructed[number_of_cams_];
            for (int cam = 0; cam < number_of_cams_; cam++) {
              cv::Mat image_channels[4];
              bool channels_data_ok = true;
              // Getting the 4 channels (RGGB) of each image in the current frame
              for (int k = 0; k < 4; k++) {
                    channel_address = 0x340 + cam * 32 + k * 8;
                    jpgadr = (((unsigned int)*(frame->image + channel_address)) << 24) +
                            (((unsigned int)*(frame->image + channel_address + 1)) << 16) +
                            (((unsigned int)*(frame->image + channel_address + 2)) << 8) +
                            (((unsigned int)*(frame->image + channel_address + 3)));
                    channel_address += 4;
                    jpgsize = (((unsigned int)*(frame->image + channel_address)) << 24) +
                            (((unsigned int)*(frame->image + channel_address + 1)) << 16) +
                            (((unsigned int)*(frame->image + channel_address + 2)) << 8) +
                            (((unsigned int)*(frame->image + channel_address + 3)));

                    if (jpgsize != 0) {
                        std::vector<unsigned char> buffer_temp(jpgsize);
                        memcpy(&(buffer_temp[0]), (unsigned char *)frame->image + jpgadr,jpgsize);
                        image_channels[k] = cv::imdecode(buffer_temp, CV_LOAD_IMAGE_GRAYSCALE);
                    }
                    else{
                        channels_data_ok = false;
                        break;
                    }
              }

              if(channels_data_ok){
                  image_reconstructed[cam] = ReconstructCameraFrame(image_channels);
              }
              else{
                  cameras_data_ok = false;
                  break;
              }

              /*if(cam==0){
                  // cv::imshow("Preview Recon", image_reconstructed);
                  // cv::imshow("Preview Undist", images_[cam]);
                  //cv::waitKey(30);}*/
            }

            if(cameras_data_ok){
		
		// Undistortion of the images done until all camera images have been correctly extracted
		std::chrono::steady_clock::time_point t3 = std::chrono::steady_clock::now();
                
		for(int idx = 0; idx < number_of_cams_; idx++){
                        images_[idx] = UndistortImage(image_reconstructed[idx],idx);
		}
		std::chrono::steady_clock::time_point t4 = std::chrono::steady_clock::now();
		//std::cout << "Undistort time: "<<std::chrono::duration_cast<std::chrono::milliseconds>(t4 - t3).count()<<std::endl;
		b_new_data_ = true; 
	    }

            total_time = total_diff_;
            mbuffer_access.unlock();

            // release frame
            err = dc1394_capture_enqueue(camera, frame);
            DC1394_ERR_RTN(err, "Could not enqueue a frame");
        }
        // stop capture
        err = dc1394_video_set_transmission(camera, DC1394_OFF);
        DC1394_ERR_RTN(err, "Could not stop transmission");
        err = dc1394_capture_stop(camera);
        DC1394_ERR_RTN(err, "Could not stop capture");
        dc1394_camera_free(camera);
        dc1394_free(d);
        printf("Waiting until all images are saved\n");
        //saveImagesThread_.join();

        // First frame doesn't count for computations of fps.
        std::cout << "Stopping the camera images acquisition thread"<< std::endl;
        return 0;
    }

    //Reconstruct the original image from the four channels! RGGB
    cv::Mat DriverJpeg::ReconstructCameraFrame(cv::Mat image_channels[]){

        cv::Mat Reconstructed = cv::Mat::zeros(1232, 1616, CV_8U);
        int x,y;
        for(y = 0; y < 1232; y++){
                uint8_t* row = Reconstructed.ptr<uint8_t>(y);
                if(y % 2 == 0){
                        uint8_t* i0 = image_channels[0].ptr<uint8_t>(y / 2);
                        uint8_t* i1 = image_channels[1].ptr<uint8_t>(y / 2);

                        for(x = 0; x < 1616; ){
                                //R
                                row[x] = i0[x / 2];
                                x++;

                                //G1
                                row[x] = i1[x / 2];
                                x++;
                        }
                }
                else {
                        uint8_t* i2 = image_channels[2].ptr<uint8_t>(y / 2);
                        uint8_t* i3 = image_channels[3].ptr<uint8_t>(y / 2);

                        for(x = 0; x < 1616; ){
                                //G2
                                row[x] = i2[x / 2];
                                x++;

                                //B
                                row[x] = i3[x / 2];
                                x++;
                        }
                }
        }

        //Debayer
        cv::Mat ReconstructedColor;
        cv::cvtColor(Reconstructed, ReconstructedColor,  cv::COLOR_BayerBG2GRAY ); // Change cv::COLOR_BayerBG2BGR parameter to obtain just grayscale images
        return ReconstructedColor;
    }

    cv::Mat DriverJpeg::UndistortImage(cv::Mat &image, int camera_idx){

        cv::Mat camera_mappings1, camera_mappings2;

        cv::fisheye::initUndistortRectifyMap(camera_extrinsics_parameters_[camera_idx], camera_distortion_parameters_[camera_idx],
            cv::Mat::eye(3, 3, CV_64F), new_camera_matrix_, image.size(), CV_16SC2, camera_mappings1, camera_mappings2);

        cv::Mat imageRect0;
        cv::remap(image, imageRect0, camera_mappings1, camera_mappings2, cv::INTER_LINEAR, cv::BORDER_CONSTANT, 0);
        return imageRect0;
    }

    // Loads the calibration and distortion parameters that are used to re-map the distorted images
    void DriverJpeg::LoadCameraConvertionParameters(){
        camera_extrinsics_parameters_.resize(6);
        camera_distortion_parameters_.resize(6);

        camera_extrinsics_parameters_[0] = (cv::Mat_<double>(3,3) << 758.1595458984375, 0.0, 765.58740234375, 0.0, 758.1595458984375, 625.93994140625, 0.0, 0.0, 1.0);
        camera_extrinsics_parameters_[1] = (cv::Mat_<double>(3,3) << 678.20263671875, 0.0, 813.1387939453125, 0.0, 678.20263671875, 610.7614135742188, 0.0, 0.0, 1.0);
        camera_extrinsics_parameters_[2] = (cv::Mat_<double>(3,3) << 753.1053466796875, 0.0, 786.4234008789062, 0.0, 753.1053466796875, 589.8832397460938, 0.0, 0.0, 1.0);
        camera_extrinsics_parameters_[3] = (cv::Mat_<double>(3,3) << 751.7859497070312, 0.0, 824.0542602539062, 0.0, 751.7859497070312, 583.7281494140625, 0.0, 0.0, 1.0);
        camera_extrinsics_parameters_[4] = (cv::Mat_<double>(3,3) << 673.9381103515625, 0.0, 800.3792114257812, 0.0, 673.9381103515625, 620.39794921875, 0.0, 0.0, 1.0);
        camera_extrinsics_parameters_[5] = (cv::Mat_<double>(3,3) << 676.1578369140625, 0.0, 798.8628540039062, 0.0, 676.1578369140625, 617.714111328125, 0.0, 0.0, 1.0);

        camera_distortion_parameters_[0] = (cv::Mat_<double>(1,4) << 0.060670867562294006, 0.016504257917404175, 0.0038004927337169647, 0.0035121100954711437);
        camera_distortion_parameters_[1] = (cv::Mat_<double>(1,4) << -0.006889168173074722, 2.6165353119722567e-05, -0.0005449394811876118, -0.00045275824959389865);
        camera_distortion_parameters_[2] = (cv::Mat_<double>(1,4) << 0.062091100960969925, 0.01209763903170824, 0.008517001755535603, 0.0004688111657742411);
        camera_distortion_parameters_[3] = (cv::Mat_<double>(1,4) << 0.061837468296289444, -0.0024798305239528418, 0.042187582701444626, -0.014464247040450573);
        camera_distortion_parameters_[4] = (cv::Mat_<double>(1,4) << -0.007778202183544636, -0.00037914581480436027, 0.0010194646893069148, -0.0012224405072629452);
        camera_distortion_parameters_[5] =  (cv::Mat_<double>(1,4) <<-0.005055880639702082, -0.0029979427345097065, 0.00276470510289073, -0.0013700607232749462);

        new_camera_matrix_ = (cv::Mat_<double>(3,3) << 627, 0.0, 807.5, 0.0, 627, 615.5, 0.0, 0.0, 1.0);
    }

    // Transfer the current frame acquired images to the main execution file
    bool DriverJpeg::CurrentFrameTransfer(std::vector<cv::Mat> & images, double &current_timestamp){
        {
        mbuffer_access.lock();
        bool temp_new_data = b_new_data_;
        if(b_new_data_){
            images = images_;
            b_new_data_ = false;
            current_timestamp = current_frame_timestamp_;
        }
        mbuffer_access.unlock();
        return temp_new_data;
        }
    }
/*
    void DriverJpeg::save_images() {

      double start_ladybug;
      double prev_ladybug;
      total_diff_ = 0;       // Difference of time
      max_diff_ladybug_ = 0; // Difference of time
      int prev_ladybug_id;
      int lost_frames = 0;
      total_lost_frames_ = 0;
      FILE *fd;

      // dc1394video_frame_t *frame;
      // unsigned char * image;
      char filename[256];
      nframes_ = 0; // Captured frames.

      // Variables to retrieve directory path.
      long size;
      char *buf;
      char *basename;

      size = pathconf(".", _PC_PATH_MAX);
      if ((buf = (char *)malloc((size_t)size)) != NULL)
        basename = getcwd(buf, (size_t)size);
    /*
      // char directory;
      char directoryPath[200];
      // directory="test";

      sprintf(directoryPath, "%s/%s", basename, directory_);

      int status;
      status = mkdir(directoryPath, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
      if (status == -1) {
        printf("Cannot create directory or directory exists. Please choose another "
               "one or delete it.\n");
        exit(1);
      }

      bool first_image = true;
      int cam, k = 0;
      unsigned int jpgadr, jpgsize, adr;

      while (true) {
        unsigned char *image = NULL;
        mbuffer_access.lock();
        int frames = buffer_.size();
        if (frames > 0) {
          image = buffer_.front();
          buffer_.erase(buffer_.begin(), buffer_.begin() + 1);
        }
        mbuffer_access.unlock();
        if (image != NULL) {
          // Get Ladybug timestamp
          adr = 0x0018;
          unsigned int seconds = (((unsigned int)*(image + adr)) << 24) +
                                 (((unsigned int)*(image + adr + 1)) << 16) +
                                 (((unsigned int)*(image + adr + 2)) << 8) +
                                 (((unsigned int)*(image + adr + 3)));

          adr = 0x001C;
          unsigned int microseconds = (((unsigned int)*(image + adr)) << 24) +
                                      (((unsigned int)*(image + adr + 1)) << 16) +
                                      (((unsigned int)*(image + adr + 2)) << 8) +
                                      (((unsigned int)*(image + adr + 3)));
          double timestamp_ladybug = seconds + microseconds / 1000000.0;

          adr = 0x0020; // Frame id
          int id_ladybug = (int)((((unsigned int)*(image + adr)) << 24) +
                                 (((unsigned int)*(image + adr + 1)) << 16) +
                                 (((unsigned int)*(image + adr + 2)) << 8) +
                                 (((unsigned int)*(image + adr + 3))));

          // Save the image in separated files
          for (cam = 0; cam < 6; cam++) {
            for (k = 0; k < 4; k++) {
              adr = 0x340 + cam * 32 + k * 8;
              jpgadr = (((unsigned int)*(image + adr)) << 24) +
                       (((unsigned int)*(image + adr + 1)) << 16) +
                       (((unsigned int)*(image + adr + 2)) << 8) +
                       (((unsigned int)*(image + adr + 3)));
              adr += 4;
              jpgsize = (((unsigned int)*(image + adr)) << 24) +
                        (((unsigned int)*(image + adr + 1)) << 16) +
                        (((unsigned int)*(image + adr + 2)) << 8) +
                        (((unsigned int)*(image + adr + 3)));

              if (jpgsize != 0) {

                sprintf(filename, "%s/%s%d_%s%05d_%d.jpg", directoryPath, "camera",
                        cam, "frame", nframes_, k);
                // printf("%s\n",filename);
                fd = fopen(filename, "w");
                fwrite((unsigned char *)(jpgadr + image), jpgsize, 1, fd);
                fclose(fd);
              }
            }
          }
          // std::cout <<id_ladybug << std::endl;
          if (first_image) {
            start_ladybug = timestamp_ladybug; // We keep a copy of first (non
                                               // absoulte ladybug timestamp)
            timestamp_ladybug = time_start_;
            first_image = false;
          } else {
            timestamp_ladybug += time_start_ - start_ladybug;
            lost_frames = id_ladybug - prev_ladybug_id - 1;
            if (lost_frames != 0) {
              total_lost_frames_ += lost_frames;
              std::cout << "==============WARNING: " << lost_frames
                        << " FRAMES LOST==============" << std::endl;
            }
            // compute time delay
            double diff_ladybug = timestamp_ladybug - prev_ladybug;
            if (max_diff_ladybug_ < diff_ladybug) {
              max_diff_ladybug_ = diff_ladybug;
            }
            // printf("Time elapsed between frames: %f sec\n",diff);
            // printf("Time elapsed between frames ladybug: %f sec\n",diff_ladybug);
          }
          prev_ladybug_id = id_ladybug;

          // Save timestamp
          sprintf(filename, "%s/%s%05d.txt", directoryPath, "frame", nframes_);
          fd = fopen(filename, "w");
          // fprintf(fd,"%lf",time_sec);
          fprintf(fd, "%lf", timestamp_ladybug);
          fclose(fd);

          total_diff_ = timestamp_ladybug - time_start_;
          prev_ladybug = timestamp_ladybug;

          std::cout << std::setw(5) << std::setfill('0') << nframes_
                    << " Buffer: " << std::setw(5) << std::setfill('0') << frames
                    << " (100) \r" << std::flush;
          nframes_++;
          free(image);
        } else if (total_diff_ >= time_ || finished) {
          return;
        } else {
          usleep(60000); // Sleep 60ms
        }
      }
    }*/

}


/*
int main(int argc, char **argv) {
  if (argc != 3) {
    std::cerr << "Usage :" << argv[0] << " <Directory> <Time>" << std::endl;
    std::cerr
        << "This will save all the frames (24 images per frame) in the "
           "specified directory ( must be new and relative path) during Time(s)"
        << std::endl;
    return 1;
  }

  // Handle ctrl-c
  struct sigaction sigIntHandler;

  sigIntHandler.sa_handler = my_handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;

  sigaction(SIGINT, &sigIntHandler, NULL);

  // Create a new NodeExample object.
  DriverJpeg *object = new DriverJpeg(argc, argv);
  object->run();

  return 0;
} // end main()
*/
