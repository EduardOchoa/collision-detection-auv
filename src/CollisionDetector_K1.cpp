// Thread that uses the frames and motion model obtained from Multicol-SLAM
// to create a depth map and detect the possible points where the AUV
// can collide.

#include <opencv2/opencv.hpp>
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <numeric>
#include <stdio.h>

#include "CollisionDetector.h"
#include "cConverter.h"

using namespace cv::xfeatures2d;

namespace MultiColSLAM
{
    CollisionDetector::CollisionDetector():
    fAcceptFrame(true), b_add_newkeyframe_(false), bnewdata_(false), b_add_newkeyframe(false)
    {}

    void CollisionDetector::Run()
    {
        bool b_do_clahe = false;
        bool b_matching_all_all = true;
        bool b_do_opencv = true;


        int epipolar_distance_threshold = 4;

        int point_cloud_counter = 0; // to store multiply files
        while(true)
        {
            if(CheckDataAvailability())
            {
                //continue;
                if(b_add_newkeyframe_)
                {
					current_keyframe_ = tracked_frame_;
					
					// Checking that there is already two keyframes that have been received from the system
					if(!previous_keyframe_.empty_frame) {

//                        /// OPENCV WINDOW MANAGEMENT
//                        cv::namedWindow("Matches after filtering - MultiCol", CV_WINDOW_NORMAL);
//                        cv::resizeWindow("Matches after filtering - MultiCol", 1000, 600);
//                        cv::namedWindow("Matches after filtering - OpenCV", CV_WINDOW_NORMAL);
//                        cv::resizeWindow("Matches after filtering - OpenCV", 1000, 600);

                        // One camera implementation at the moment
                        int number_cams = previous_keyframe_.camSystem.GetNrCams();

                        /// Select camera
                        int cam_id = 1; //current cam in use

                        /// Get intrinsics
                        //cv::Mat intrinsics_azores = (cv::Mat_<double>(3,3) << 555, 0, 407.5, 0 , 555, 307.5, 0, 0, 1); //Azores intrinsics
                        //cv::Mat intrinsics_wall = (cv::Mat_<double>(3,3) << 250, 0, 403.5, 0 , 250, 307.5, 0, 0, 1);
                        //cv::Mat intrinsics_office = (cv::Mat_<double>(3,3) << 313.5, 0, 404, 0 , 313.5, 308, 0, 0, 1); // Hardcoded for our case. All camera images have been undistorted with the same intrinsic parameters
                        cv::Mat intrinsics_boreas = GetIntrinsicBoreas(3); // camera number 5 is the downlooking one

                        cv::Mat intrinsics;
                        intrinsics_boreas.copyTo(intrinsics);

                        /// GET CAMERA POSE(Multicol SLAM)
                        // 1. Getting the transformation matrix between the frames
                        cv::Mat base_T_cam = cv::Mat(
                                current_keyframe_.GetPoseMc(cam_id)); // Intrinsics wont change in camera i
                        cv::Mat world_Tcurrent_base = cv::Mat(current_keyframe_.GetPose());
                        cv::Mat world_Tprevious_base = cv::Mat(previous_keyframe_.GetPose());

                        // 2. Get relative transformations
                        cv::Mat cam_previous_T_cam_current(4, 4, CV_64F);
                        TransformationMatrixBetweenFrames(base_T_cam, world_Tprevious_base, world_Tcurrent_base,
                                                          cam_previous_T_cam_current);

                        cv::Mat cam_current_T_cam_previous(4, 4, CV_64F);
                        cam_previous_T_cam_current.copyTo(cam_current_T_cam_previous);
                        cam_current_T_cam_previous = cam_current_T_cam_previous.inv();

                        ///PROCESS
                        cv::Mat previous_image_cam_i, current_image_cam_i;
                        /// Image preprocessing
                        if (b_do_clahe) {
                            cv::Ptr <cv::CLAHE> clahe = cv::createCLAHE();
                            clahe->setClipLimit(2);

                            clahe->apply(previous_keyframe_.images[cam_id], previous_image_cam_i);
                            clahe->apply(current_keyframe_.images[cam_id], current_image_cam_i);
                        } else {
                            previous_image_cam_i = previous_keyframe_.images[cam_id];
                            current_image_cam_i = current_keyframe_.images[cam_id];
                        }

                        /// FEATURE DETECTION
                        // Detect keypoints and compute the descriptors
                        // Implementing different types of feature descriptors from OpenCv
                        std::vector <cv::KeyPoint> keypoints_previous_i, keypoints_current_i;
                        cv::Mat descriptors_previous_i, descriptors_current_i;
                        string detector_type = "SIFT";
                        if (detector_type == "SURF") {
                            int minHessian = 100; // Used for SURF Detector as a threshold (larger is more restrictive)
                            cv::Ptr <SURF> detector = SURF::create(minHessian, 4, 3, false);

                            detector->detectAndCompute(previous_image_cam_i, cv::Mat(), keypoints_previous_i,
                                                       descriptors_previous_i);
                            detector->detectAndCompute(current_image_cam_i, cv::Mat(), keypoints_current_i,
                                                       descriptors_current_i);
                        } else if (detector_type == "SIFT") {
                            int n_features = 5000;
                            cv::Ptr <SIFT> detector = SIFT::create(n_features, 1, 0.01, 10, 1.1);

                            detector->detectAndCompute(previous_image_cam_i, cv::Mat(), keypoints_previous_i,
                                                       descriptors_previous_i);
                            detector->detectAndCompute(current_image_cam_i, cv::Mat(), keypoints_current_i,
                                                       descriptors_current_i);
                        }

                        /// FEATURE MATCHING - ALL against ALL
                        std::vector< cv::DMatch > filtered_matches_multicol;

                        // 2. Transforming the Rotation and translation parameters to obtain the Fundamental Matrix
                        cv::Mat current_Fmulticol_previous_(3, 3, CV_64F);
                        ComputeFundamentalMatrix(cam_current_T_cam_previous, intrinsics, current_Fmulticol_previous_);

                        if(b_matching_all_all)
                        {
                            // Matching the descriptors using FLANN matcher
                            std::vector <cv::DMatch> best_matches;
                            cv::Ptr <cv::DescriptorMatcher> matcher = cv::DescriptorMatcher::create(
                                    cv::DescriptorMatcher::FLANNBASED);
                            std::vector <std::vector<cv::DMatch>> knn_matches;
                            matcher->knnMatch(descriptors_previous_i, descriptors_current_i, knn_matches, 2);

                            // Pre-filtering of matches using Lowe's ratio test
                            const float ratio_thresh = 0.8f;
                            for (size_t i = 0; i < knn_matches.size(); i++) {
                                if (knn_matches[i][0].distance < ratio_thresh * knn_matches[i][1].distance) {
                                    best_matches.push_back(knn_matches[i][0]);
                                }
                            }

                            // Compute epipolar lines on current image
                            std::vector <cv::Point2f> points_previous_i, points_current_i;
                            FromMatchesToVectorOfPoints(keypoints_previous_i, keypoints_current_i, best_matches,
                                                        points_previous_i, points_current_i);


                            std::vector <cv::Vec3f> lines_current_i;
                            cv::computeCorrespondEpilines(points_previous_i, 1, current_Fmulticol_previous_, lines_current_i);

                            for (int l=0; l < points_previous_i.size(); l++) {
                                double epipole_d_multicol = DistancePointToLine(points_current_i[l],
                                                                                lines_current_i[l]);

                                if (epipole_d_multicol < epipolar_distance_threshold) {
                                    filtered_matches_multicol.push_back(best_matches[l]);
                                }
                            }
                        } else{
                            /// FEATURE MATCHING - Epipolar matching
                            filtered_matches_multicol = MatchingWithEpipolarConstraint(
                                    current_Fmulticol_previous_, previous_image_cam_i, descriptors_previous_i,
                                    keypoints_previous_i,
                                    current_image_cam_i, descriptors_current_i, keypoints_current_i);
                        }

                        /// TRIANGULATION STEP
                        if (filtered_matches_multicol.size() > 0) {
                            // Find the projective matrices
                            cv::Mat P_previous_multicol, P_current_multicol;
                            ComputeProjectionMatrices(intrinsics, cam_current_T_cam_previous, P_previous_multicol,
                                                      P_current_multicol); // Transformation used is from frame 1 to frame 2

                            // Projection matrices
                            std::vector <cv::Mat> projection_matrices_multicol;
                            projection_matrices_multicol.push_back(P_previous_multicol);
                            projection_matrices_multicol.push_back(P_current_multicol);

                            // Triangulation
                            std::vector <cv::Mat> vec_filtered_points_2d_multicol;
                            std::vector <cv::Point2f> vec_filtered_matches_multicol_previous, vec_filtered_matches_multicol_current;

                            // Getting the filtered points from multicol-slam
                            FromMatchesToVectorOfPoints(keypoints_previous_i, keypoints_current_i,
                                                        filtered_matches_multicol,
                                                        vec_filtered_matches_multicol_previous,
                                                        vec_filtered_matches_multicol_current);
                            GetArrayOfPoints(vec_filtered_matches_multicol_previous, vec_filtered_matches_multicol_current,
                                             vec_filtered_points_2d_multicol);

                            // Getting 3D points
                            cv::Mat triangulated_points_multicol;
                            cv::sfm::triangulatePoints(vec_filtered_points_2d_multicol, projection_matrices_multicol,
                                                       triangulated_points_multicol);

                            /// Check 3D POints
                            std::vector <cv::Mat> final_points;

                            for (int pt_i = 0; pt_i < triangulated_points_multicol.size().width; pt_i++) {

                                cv::Mat pt_hom;
                                cv::sfm::euclideanToHomogeneous(triangulated_points_multicol.col(pt_i), pt_hom);
                                cv::Mat pt_cam_previous, pt_cam_current;

                                cv::sfm::euclideanToHomogeneous(projection_matrices_multicol[0] * pt_hom,
                                                                pt_cam_previous);
                                cv::sfm::euclideanToHomogeneous(projection_matrices_multicol[1] * pt_hom,
                                                                pt_cam_current);

                                if (pt_cam_previous.at<double>(2, 0) < 0.0 || pt_cam_current.at<double>(2, 0) < 0.0) {
                                    continue;
                                }

                                // Image 1 id
                                cv::Point2f feat_img_1 = keypoints_previous_i[filtered_matches_multicol[pt_i].queryIdx].pt;
                                cv::Point2f feat_img_2 = keypoints_current_i[filtered_matches_multicol[pt_i].trainIdx].pt;

                                pt_cam_previous = pt_cam_previous / pt_cam_previous.at<double>(2, 0);
                                pt_cam_current = pt_cam_current / pt_cam_current.at<double>(2, 0);

                                double errXp =
                                        pt_cam_previous.at<double>(0, 0) - cv::saturate_cast<double>(feat_img_1.x);
                                double errYp =
                                        pt_cam_previous.at<double>(1, 0) - cv::saturate_cast<double>(feat_img_1.y);
                                double errXc =
                                        pt_cam_current.at<double>(0, 0) - cv::saturate_cast<double>(feat_img_2.x);
                                double errYc =
                                        pt_cam_current.at<double>(1, 0) - cv::saturate_cast<double>(feat_img_2.y);

                                if (cv::sqrt(errXp * errXp + errYp * errYp) > 4.0)
                                    continue;


                                if (cv::sqrt(errXc * errXc + errYc * errYc) > 4.0)
                                    continue;


                                final_points.push_back(triangulated_points_multicol.col(pt_i));

                            }

                            std::cout << "N final points: " << final_points.size() << endl;


                            /// SAVING THE POINTCLOUDS
                            {
                                char buffer[100];
                                std::sprintf(buffer, "/home/eduochoa/PointClouds/Boreas/PointCloud_Multicol%d.ply",
                                             point_cloud_counter);
                                ofstream outfile(buffer);
                                outfile << "ply\n" << "format ascii 1.0\n";
                                outfile << "element vertex " << final_points.size() << "\n";
                                outfile << "property float x\n" << "property float y\n" << "property float z\n";
                                outfile << "end_header\n";
                                for (int i = 0; i < final_points.size(); i++) {
                                    outfile << final_points[i].at<double>(0, 0) << " ";
                                    outfile << final_points[i].at<double>(1, 0) << " ";
                                    outfile << final_points[i].at<double>(2, 0) << " ";
                                    outfile << "\n";
                                }
                                outfile.close();

                                /*
                                /// DRAW MATCHES
                                {
                                    cv::Mat img_matches_multicol;
                                    cv::drawMatches(previous_image_cam_i, keypoints_previous_i, current_image_cam_i,
                                                    keypoints_current_i, filtered_matches_multicol,
                                                    img_matches_multicol, cv::Scalar::all(-1),
                                                    cv::Scalar::all(-1), std::vector<char>(),
                                                    cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
                                    cv::imshow("Matches after filtering - MultiCol", img_matches_multicol);
                                }
                                cv::waitKey(10);
                                 */
                            }

                            //if (b_do_opencv)
                            {
                                /// OPENCV
                                std::vector <cv::Point2f> points_previous_i, points_current_i;
                                FromMatchesToVectorOfPoints(keypoints_previous_i, keypoints_current_i, filtered_matches_multicol,
                                                            points_previous_i, points_current_i);
                                // Computing the fundamental matrix using OpenCV to compare the methods
                                cv::Mat outliers;
                                cv::Mat current_Fopencv_previous_ = cv::findFundamentalMat(points_previous_i,
                                                                                           points_current_i, cv::FM_RANSAC,
                                                                                           2, 0.99, outliers);
                                std::vector <cv::DMatch> filtered_matches_opencv;
                                for (int idx = 0; idx < points_previous_i.size(); idx++) {
                                    if (outliers.at<uchar>(idx)) {
                                        filtered_matches_opencv.push_back(filtered_matches_multicol[idx]);
                                    }
                                }

                                cv::Mat E_mat;
                                cv::sfm::essentialFromFundamental(current_Fopencv_previous_, intrinsics, intrinsics, E_mat);
                                std::vector <cv::Mat> R_matrices, t_matrices;
                                cv::sfm::motionFromEssential(E_mat, R_matrices, t_matrices);


                                // choosing the best match (the one with the less distance)
                                int best_point_idx;
                                float best_point_distance = 100000;
                                for (int p = 0; p < filtered_matches_opencv.size(); p++) {
                                    if (filtered_matches_opencv[p].distance < best_point_distance) {
                                        best_point_distance = filtered_matches_opencv[p].distance;
                                        best_point_idx = p;
                                    }
                                }
                                cv::Mat best_matched_point_prev = (cv::Mat_<double>(2, 1)
                                        << keypoints_previous_i[filtered_matches_opencv[best_point_idx].queryIdx].pt.x,
                                        keypoints_previous_i[filtered_matches_opencv[best_point_idx].queryIdx].pt.y);
                                cv::Mat best_matched_point_current = (cv::Mat_<double>(2, 1)
                                        << keypoints_current_i[filtered_matches_opencv[best_point_idx].trainIdx].pt.x,
                                        keypoints_current_i[filtered_matches_opencv[best_point_idx].trainIdx].pt.y);

                                //                        int idx_matrices = cv::sfm::motionFromEssentialChooseSolution(R_matrices, t_matrices, intrinsics, best_matched_point_prev, intrinsics, best_matched_point_current);
                                int idx_matrices = cv::sfm::motionFromEssentialChooseSolution(R_matrices, t_matrices,
                                                                                              intrinsics,
                                                                                              best_matched_point_prev,
                                                                                              intrinsics,
                                                                                              best_matched_point_current);

                                // Getting back the Fundamental matrix from the best R and ts
                                cv::Mat projective_mat_previous_opencv = cv::Mat::eye(3, 4, CV_64F);
                                projective_mat_previous_opencv = intrinsics * projective_mat_previous_opencv;
                                cv::Mat projective_mat_current_opencv;
                                //                        cv::sfm::projectionFromKRt(intrinsics,R_matrices[idx_matrices].t(),-R_matrices[idx_matrices].t()*t_matrices[idx_matrices],projective_mat2_opencv);
                                cv::sfm::projectionFromKRt(intrinsics, R_matrices[idx_matrices], t_matrices[idx_matrices],
                                                           projective_mat_current_opencv);


                                // Triangulation
                                std::vector <cv::Mat> vec_filtered_points_2d_opencv;
                                std::vector <cv::Point2f> vec_filtered_matches_opencv_previous, vec_filtered_matches_opencv_current;

                                // Getting the filtered points from multicol-slam
                                FromMatchesToVectorOfPoints(keypoints_previous_i, keypoints_current_i,
                                                            filtered_matches_opencv, vec_filtered_matches_opencv_previous,
                                                            vec_filtered_matches_opencv_current);
                                GetArrayOfPoints(vec_filtered_matches_opencv_current, vec_filtered_matches_opencv_current,
                                                 vec_filtered_points_2d_opencv);

                                // Projection matrices
                                std::vector <cv::Mat> projection_matrices_opencv;
                                projection_matrices_opencv.push_back(projective_mat_previous_opencv);
                                projection_matrices_opencv.push_back(projective_mat_current_opencv);

                                // Getting 3D points
                                cv::Mat triangulated_points_opencv;
                                cv::sfm::triangulatePoints(vec_filtered_points_2d_opencv, projection_matrices_opencv,
                                                           triangulated_points_opencv);


                                /// Check 3D POints
                                std::vector <cv::Mat> final_points_opencv;
                                for (int pt_i = 0; pt_i < triangulated_points_opencv.size().width; pt_i++) {
                                    final_points_opencv.push_back(triangulated_points_opencv.col(pt_i));

                                }

                                /// SAVING THE POINTCLOUDS
                                char buffer[100];
                                std::sprintf(buffer, "/home/eduochoa/PointClouds/Boreas/PointCloud_OpenCV%d.ply",
                                             point_cloud_counter);
                                ofstream outfile(buffer);
                                outfile << "ply\n" << "format ascii 1.0\n";
                                outfile << "element vertex " << final_points_opencv.size() << "\n";
                                outfile << "property float x\n" << "property float y\n" << "property float z\n";
                                outfile << "end_header\n";
                                for (int i = 0; i < final_points_opencv.size(); i++) {
                                    outfile << final_points_opencv[i].at<double>(0, 0) << " ";
                                    outfile << final_points_opencv[i].at<double>(1, 0) << " ";
                                    outfile << final_points_opencv[i].at<double>(2, 0) << " ";
                                    outfile << "\n";
                                }
                                outfile.close();

                                /*
                                /// DRAW MATCHES
                                {
                                    cv::Mat img_matches_multicol;
                                    cv::drawMatches(previous_image_cam_i, keypoints_previous_i, current_image_cam_i,
                                                    keypoints_current_i, filtered_matches_opencv, img_matches_multicol,
                                                    cv::Scalar::all(-1),
                                                    cv::Scalar::all(-1), std::vector<char>(),
                                                    cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
                                    cv::imshow("Matches after filtering - OpenCV", img_matches_multicol);
                                    cv::waitKey(10);
                                }
                                */
                            }
                            point_cloud_counter++;
                        }
                    }
					FramesUpdate(current_keyframe_);
                    b_add_newkeyframe_ = false;
				}
				else
				{
                    // DO SOMETHING
				}
                //std::this_thread::sleep_for(std::chrono::milliseconds(2000));
            } 
        }

     }

    // Makes the transfer of data from Tracking Thread to Collision Detection Thread
    void CollisionDetector::TransferFrameData(cMultiFrame F, bool newdata, bool add_newkeyframe)
    {
        {

        std::unique_lock<std::mutex> lock(mMutexReceiveData);
        bnewdata_ = newdata;
        b_add_newkeyframe = add_newkeyframe;
        if(b_add_newkeyframe){
            tracked_frame = F;
            tracked_frame_pose = F.GetPose();
        }
        else {
            tracked_frame_pose = F.GetPose();
        }

        }
    }

    // Checks if new data is available for processing and copies it into the object variables
    bool CollisionDetector::CheckDataAvailability()
    {
        {

        std::unique_lock<std::mutex> lock(mMutexReceiveData);
        bool temp = bnewdata_;
        if(bnewdata_){
            tracked_frame_ = tracked_frame;
            tracked_frame_pose_ = tracked_frame_pose;
            bnewdata_ = false;
            b_add_newkeyframe_ = b_add_newkeyframe;
        }

        return temp;

        }
    }

    void CollisionDetector::AcceptMultiFrames(bool flag)
    {
        std::unique_lock<std::mutex> lock(mMutexAccept);
        fAcceptFrame = flag;
    }

    // Gets the Translation parameters of pose T
    cv::Vec3d CollisionDetector::GetTranslation(cv::Matx<double, 4, 4> &T)
    {
        return cConverter::Hom2T(T);
    }

    // Gets the rotation parameters of pose T
    vector<double> CollisionDetector::GetRotation(cv::Matx<double, 4, 4> &T)
    {
        cv::Matx33d R = cConverter::Hom2R(T);
        return cConverter::toQuaternion(R);
    }
    
    // Update the Keyframes of a moment in time
    void CollisionDetector::FramesUpdate(cMultiFrame current_frame)
    {
		previous_keyframe_ = current_frame;
	}

    // Gets the transformation matrix between contiguous frames
    void CollisionDetector::TransformationMatrixBetweenFrames(cv::Mat &base_T_cam, cv::Mat &world_Tprevious_base,
                                                              cv::Mat &world_Tcurrent_base, cv::Mat &cam_previous_T_cam_current){

        cv::Mat world_Tprevious_cam = world_Tprevious_base * base_T_cam;
        cv::Mat world_Tcurrent_cam = world_Tcurrent_base * base_T_cam;

        // Getting the transformation
        cam_previous_T_cam_current = world_Tprevious_cam.inv() * world_Tcurrent_cam;
    }

    // Computes the fundamental matrix between two frames given the transformation matrix
    void CollisionDetector::ComputeFundamentalMatrix(cv::Mat & current_T_previous, cv::Mat & cam_K, cv::Mat &current_F_previous){
        // https://sourishghosh.com/2016/fundamental-matrix-from-camera-matrices/
        cv::Mat rotation = current_T_previous(cv::Range(0, 3), cv::Range(0, 3) );
        cv::Mat translation = (cv::Mat_<double>(3,1) << current_T_previous.at<double>(0,3),
                current_T_previous.at<double>(1,3),
                current_T_previous.at<double>(2,3));

        cv::Mat A = cam_K * rotation.t() * translation;
        cv::Mat cross_product_mat = (cv::Mat_<double>(3,3) << 0, -A.at<double>(2,0), A.at<double>(1,0),
                A.at<double>(2,0), 0, -A.at<double>(0,0),
                -A.at<double>(1,0), A.at<double>(0,0), 0);
        current_F_previous = (cam_K.inv()).t() * rotation * cam_K.t() * cross_product_mat;
        current_F_previous = current_F_previous/current_F_previous.at<double>(2,2);
    }


    // Matches keypoints of two images using epipolar constraint
    std::vector<cv::DMatch> CollisionDetector::MatchingWithEpipolarConstraint(const cv::Mat &F_21, const cv::Mat &image1, const cv::Mat &descriptors1, const std::vector<cv::KeyPoint> &keypoints1,
                                                           const cv::Mat &image2, const cv::Mat &descriptors2, const std::vector<cv::KeyPoint> &keypoints2){
        std::vector<cv::Point2f> vec_keypoints_1, vec_keypoints_2;
        std::vector<cv::DMatch> matches_from12;

        // Converting the keypoints into a vector of points2d
        for (int i = 0; i < keypoints1.size(); ++i) {
            vec_keypoints_1.push_back(keypoints1[i].pt);
        }
        for (int i = 0; i < keypoints2.size(); ++i) {
            vec_keypoints_2.push_back(keypoints2[i].pt);
        }

        // Computing the epipolar lines based on the F matrix
        std::vector<cv::Vec3f> epipolar_lines_1, epipolar_lines_2;
        cv::computeCorrespondEpilines(vec_keypoints_1,1,F_21,epipolar_lines_2);
        cv::computeCorrespondEpilines(vec_keypoints_2,2,F_21,epipolar_lines_1);

        // Epipolar feature matching
        const float inlier_distance = 2.f;
        const double inlier_ratio = 1.0/0.8f;
        const double inlier_cosine_threshold = 0.90f;
        const double inlier_desc_distance_threshold = 200.0f;

        std::vector<cv::DMatch> vec_matches_1to2;
        for (int i = 0; i < keypoints1.size(); ++i) {
            double best_distance_score = -1.0f;
            double second_best_distance_score = -1.0f;
            cv::DMatch match;
            for (int j = 0; j < keypoints2.size(); ++j) {
                // Distance to epipolar line
                float epi_distance = DistancePointToLine(keypoints2[j].pt, epipolar_lines_2[i]);
                if(epi_distance > inlier_distance) {
                    continue;
                }


                // Difference of descriptiors
                double cos = descriptors1.row(i).dot(descriptors2.row(j)) /(cv::norm(descriptors1.row(i)) * cv::norm(descriptors2.row(j)));
                if(cos < inlier_cosine_threshold) {
                    continue;
                }

                if (cos > best_distance_score) {
                    second_best_distance_score = best_distance_score;
                    best_distance_score = cos;
                    match = cv::DMatch(i, j, (float) cos);
                }
                else if(cos > second_best_distance_score)
                {
                    second_best_distance_score = cos;
                }
            }
            if(best_distance_score > 0 && best_distance_score / second_best_distance_score < inlier_ratio){
                vec_matches_1to2.push_back(match);
            }
            else {
                vec_matches_1to2.push_back(cv::DMatch(-1, -1, -114514.f));
            }
        }

        std::vector<cv::DMatch> vec_matches_2to1;
        for (int i = 0; i < keypoints2.size(); ++i) {
            double best_distance_score = -1.0f;
            double second_best_distance_score = -1.0f;
            cv::DMatch match;
            for (int j = 0; j < keypoints1.size(); ++j) {
                // Distance to epipolar line
                float epi_distance = DistancePointToLine(keypoints1[j].pt, epipolar_lines_1[i]);

                if(epi_distance > inlier_distance) {
                    continue;
                }

                // Difference of descriptiors

                double cos = descriptors2.row(i).dot(descriptors1.row(j)) /(cv::norm(descriptors2.row(i)) * cv::norm(descriptors1.row(j)));
                if(cos < inlier_cosine_threshold) {
                    continue;
                }

                if (cos > best_distance_score) {
                    second_best_distance_score = best_distance_score;
                    best_distance_score = cos;
                    match = cv::DMatch(i, j, (float) cos);
                }
                else if(cos > second_best_distance_score)
                {
                    second_best_distance_score = cos;
                }
            }
            if(best_distance_score > 0 && best_distance_score / second_best_distance_score < inlier_ratio){
                vec_matches_2to1.push_back(match);
            }
            else {
                vec_matches_2to1.push_back(cv::DMatch(-1, -1, -114514.f));
            }
        }

        // Cross check
        for (int n = 0; n < vec_matches_1to2.size(); ++n) {
            if(vec_matches_1to2[n].queryIdx != -1 and vec_matches_1to2[n].trainIdx != -1)
            {
                if(vec_matches_1to2[n].queryIdx == vec_matches_2to1[vec_matches_1to2[n].trainIdx].trainIdx) {
                    matches_from12.push_back(vec_matches_1to2[n]);
                }
            }
        }

        return matches_from12;
    }

    // Computation of the orthogonal distance from a point to a line
    double CollisionDetector::DistancePointToLine( cv::Point2f point, cv::Vec3f epiline){
        return abs(epiline[0]*point.x + epiline[1]*point.y + epiline[2])/sqrtf(pow(epiline[0],2) + pow(epiline[1],2));
    }

    // Transform a vector of DMatch into a one of Point2d
    void CollisionDetector::FromMatchesToVectorOfPoints(std::vector<cv::KeyPoint> &keypoints_frame1, std::vector<cv::KeyPoint> &keypoints_frame2,
                                     std::vector<cv::DMatch> &matches, std::vector <cv::Point2f> &points_frame1,std::vector <cv::Point2f> &points_frame2){
        for (int p = 0; p < (int)matches.size(); p++) {
            points_frame1.push_back(keypoints_frame1[matches[p].queryIdx].pt);
            points_frame2.push_back(keypoints_frame2[matches[p].trainIdx].pt);
        }
    }

    // Draws the epipolar lines from points of one image into the other one
    void CollisionDetector::DrawEpipolarlines(cv::Mat &first_image, cv::Mat &second_image, cv::Mat &lines_image,
                           std::vector <cv::Vec3f> lines){
        /// first_image will be the image on which we drwa the eiplines from the points of second_image
        /// lines are the corresponding epiline

        first_image.copyTo(lines_image);

        for (auto it = lines.begin(); it != lines.end(); ++it) {
            cv::line(lines_image, cv::Point(0,-(*it)[2]/(*it)[1]),
                     cv::Point(lines_image.cols, -((*it)[2]+(*it)[0]*lines_image.cols)/(*it)[1]),
                     cv::Scalar(255,255,255));
        }
    }

    // Creates the projection matrices out of the Rotation and translation parameters between two frames
    void CollisionDetector::ComputeProjectionMatrices(cv::Mat &cam_K, cv::Mat &current_T_previous, cv::Mat &P_previous, cv::Mat &P_current){
        cv::Mat reference = cv::Mat::eye(3,4,CV_64F); // Located at frame 1 at the moment
        cv::Mat rotation = current_T_previous(cv::Range(0, 3), cv::Range(0, 3) );
        cv::Mat translation = (cv::Mat_<double>(3,1) << current_T_previous.at<double>(0,3),
                current_T_previous.at<double>(1,3),
                current_T_previous.at<double>(2,3));

        // Projective matrix comes from multiplying the intrinsics by the rotation & translation
        P_previous =  cam_K * reference;
        cv::sfm::projectionFromKRt(cam_K,rotation,translation,P_current);
    }

    // Transform points into a InputArrayOfArrays
    void CollisionDetector::GetArrayOfPoints(std::vector<cv::Point2f> &points_frame1, std::vector<cv::Point2f> &points_frame2, std::vector<cv::Mat> &array_of_points){
        cv::Mat points1Mat = (cv::Mat_<double>(2,1) << points_frame1[0].x, points_frame1[0].y);
        cv::Mat points2Mat = (cv::Mat_<double>(2,1) << points_frame2[0].x, points_frame2[0].y);

        for(int i=1; i < points_frame1.size(); i++){
            cv::Mat point1 = (cv::Mat_<double>(2,1) << points_frame1[i].x, points_frame1[i].y);
            cv::Mat point2 = (cv::Mat_<double>(2,1) << points_frame2[i].x, points_frame2[i].y);
            cv::hconcat(points1Mat,point1,points1Mat);
            cv::hconcat(points2Mat,point2,points2Mat);
        }

        array_of_points.push_back(points1Mat);
        array_of_points.push_back(points2Mat);
    }

    double CollisionDetector::AverageCosineValueOfMatches(const std::vector<cv::DMatch> &matches, const cv::Mat &descriptors1, const cv::Mat &descriptors2){
        std::vector<double> cosine_average;
        double min_cosine = 10000000;
        double max_cosine = 0;
        for (int i = 0; i < matches.size() ; ++i) {
            int idx_desc1 = matches[i].queryIdx;
            int idx_desc2 = matches[i].trainIdx;

            double cos = descriptors1.row(idx_desc1).dot(descriptors2.row(idx_desc2)) / (cv::norm(descriptors1.row(idx_desc1)) * cv::norm(descriptors2.row(idx_desc2)));
            cosine_average.push_back(cos);

            if(cos < min_cosine){
                min_cosine = cos;
            }
            if(cos > max_cosine){
                max_cosine = cos;
            }
        }

        cout << "max cos " << max_cosine << endl;
        cout << "min cos " << min_cosine << endl;
        return accumulate( cosine_average.begin(), cosine_average.end(), 0.0)/ cosine_average.size();
    }

    cv::Mat CollisionDetector::GetIntrinsicBoreas(int cam){
        if(cam ==0){
            cv::Mat intrinsics_of_cam = (cv::Mat_<double>(3,3) << 599.33, 0, 694.545, 0 , 599.33, 513.71, 0, 0, 1);
            return intrinsics_of_cam;
        }
        else if(cam == 1){
            cv::Mat intrinsics_of_cam = (cv::Mat_<double>(3,3) << 603.01, 0, 649.05, 0 , 603.01, 529.07, 0, 0, 1);
            return intrinsics_of_cam;
        }
        else if(cam == 2){
            cv::Mat intrinsics_of_cam = (cv::Mat_<double>(3,3) << 598.725, 0, 674.795, 0 , 598.725, 517.895, 0, 0, 1);
            return intrinsics_of_cam;
        }
        else if(cam == 3){
            cv::Mat intrinsics_of_cam = (cv::Mat_<double>(3,3) << 592.015, 0, 664.99, 0 , 592.015, 526.025, 0, 0, 1);
            return intrinsics_of_cam;
        }
        else if(cam == 4){
            cv::Mat intrinsics_of_cam = (cv::Mat_<double>(3,3) << 610.08, 0, 677.49, 0 , 610.08, 501.44, 0, 0, 1);
            return intrinsics_of_cam;
        }
        else{
            cv::Mat intrinsics_of_cam;
            return intrinsics_of_cam;
        }
    }

}
