#ifndef DRIVER_JPEG_DISK_H
#define DRIVER_JPEG_DISK_H

#include <boost/thread.hpp>
#include <iostream>
#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>

// Includes libdc1394
#include <dc1394/dc1394.h>

// Includes opencv
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

// Include Multicol SLAM needed files

namespace MultiColSLAM {

    using std::string;

    class DriverJpeg {
    public:
      //! Constructor.
      DriverJpeg(int num_cams);

      //! Destructor.
      ~DriverJpeg();

      int Run();
      void Stop();

      bool StopRequested();

      void LoadCameraConvertionParameters();

      // Functions to process the images
      cv::Mat ReconstructCameraFrame(cv::Mat image_channels[]);
      cv::Mat UndistortImage(cv::Mat &image, int camera_idx);

      // Communication between Driver and Main file
      bool CurrentFrameTransfer(std::vector<cv::Mat> & images,double &current_timestamp);

    protected:
      int number_of_cams_;
      bool b_new_data_;
      double current_frame_timestamp_;

      // flag used to stop the frame acquisition process
      bool stop_execution = false;

      // Variable to store images obtained by the driver
      std::vector<cv::Mat> images_;

      // Camera extrinsic parameters variable and re-mapping variables
      std::vector<cv::Mat> camera_extrinsics_parameters_; // Ladybug camera has 6 cameras in total
      std::vector<cv::Mat> camera_distortion_parameters_;
      cv::Mat new_camera_matrix_;

      // Protects the access to the buffer
      boost::mutex mbuffer_access;

    private:
      //void save_images();

      char *directory_;

      std::vector<unsigned char *> buffer_;

      //boost::thread saveImagesThread_;
      double time_;
      int nframes_;
      double total_diff_;
      double max_diff_ladybug_;
      int total_lost_frames_;
      int buffer_full_;
      double time_start_;
    };

}
#endif // DRIVER_JPEG_DISK_H

