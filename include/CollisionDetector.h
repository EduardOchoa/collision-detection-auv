#ifndef COLLISIONDETECTOR_H
#define COLLISIONDETECTOR_H

#include<mutex>
#include <math.h>
#include <sys/stat.h>

// OpenCV
#include "opencv2/core.hpp"
#include <opencv2/sfm.hpp>
#include "opencv2/highgui.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"
#include <opencv2/viz.hpp>
#include <opencv2/calib3d.hpp>

#include "cam_system_omni.h"
#include "cMultiFrame.h"
#include "cSystem.h"
#include "cTracking.h"
#include "cMultiKeyFrame.h"
#include "cMap.h"
#include "cMultiKeyFrameDatabase.h"

namespace MultiColSLAM
{
	class cTracking;
	class cLocalMapping;
	
	class CollisionDetector
	{
        public:
            CollisionDetector();
		
	    int pc_ID;

            // Variables used as shared memory between tracking thread and collision detection thread
            bool b_add_newkeyframe;
            cMultiFrame tracked_frame;
            cv::Matx<double, 4, 4> tracked_frame_pose;

            // Running function of the collision detection thread
            void Run();

            // Function that manage thread communication with tracking
            bool CheckDataAvailability();
            void AcceptMultiFrames(bool flag);
            void TransferFrameData(cMultiFrame &F, bool newdata, bool add_newkeyframe);
            void SystemReset();
            bool CheckReset();
            void ResetFinished();
            void ResetThreadData();

            cv::Vec3d GetTranslation(cv::Matx<double, 4, 4> &T);
            vector<double> GetRotation(cv::Matx<double, 4, 4> &T);
            
            // Functions for the depth map creation schedule
            void FramesUpdate(cMultiFrame &current_frame);
            void TransformationMatrixBetweenFrames(cv::Mat &camera_pose_respect_to_body, cv::Mat &previous_base_pose_respect_to_world,
                                               cv::Mat &current_base_pose_respect_to_world, cv::Mat &transformation);
            void ComputeFundamentalMatrix(cv::Mat &transformation_between_frames, cv::Mat &Intrinsic_parameters, cv::Mat &fundamental_matrix);
            std::vector<cv::DMatch> MatchingWithEpipolarConstraint(const cv::Mat &F, const cv::Mat &image1, const cv::Mat &descriptors1, const std::vector<cv::KeyPoint> &keypoints1,
                                                                const cv::Mat &image2, const cv::Mat &descriptors2, const std::vector<cv::KeyPoint> &keypoints2);
            double DistancePointToLine( cv::Point2f point, cv::Vec3f epiline);
            void FromMatchesToVectorOfPoints(std::vector<cv::KeyPoint> &keypoints_frame1, std::vector<cv::KeyPoint> &keypoints_frame2,
                                            std::vector<cv::DMatch> &matches, std::vector <cv::Point2f> &points_frame1,std::vector <cv::Point2f> &points_frame2);
            void DrawEpipolarlines(cv::Mat &first_image, cv::Mat &second_image, cv::Mat &lines_image, std::vector <cv::Vec3f> lines);
            void GetArrayOfPoints(std::vector<cv::Point2f> &points_frame1, std::vector<cv::Point2f> &points_frame2, std::vector<cv::Mat> &Array_of_points);
            void ComputeProjectionMatrices(cv::Mat &camera_intrinsics, cv::Mat &transformation_mat, cv::Mat &projective1, cv::Mat &projective2);
            double AverageCosineValueOfMatches(const std::vector<cv::DMatch> &matches, const cv::Mat &descriptors1, const cv::Mat &descriptors2);

            // Functions for collision detection with 3d point
            void ComputeRiskFactor(std::vector<cv::Mat> &points_3d, cv::Mat &world_Tprevious_base, cv::Mat &world_Tcurrent_base, double prev_timestamp, double current_timestamp, cv::Mat &pts_risk_factor,
                                   cv::Mat &d_min,cv::Mat &t_min);
            double GetAngleWeight(const double &value, const double &angle_threshold);
            void GetRepulsionVectors(std::vector<cv::Mat> &points_3d, cv::Mat &vec_forces);
            void GetRepulsiveVector(cv::Mat &weight_factors, cv::Mat &vec_forces, cv::Mat &weighted_forces, cv::Mat &repulsive_force);


            //cv::Matx<double, 4, 4> GetTransformation();

        protected:
            bool fAcceptFrame;
            bool b_add_newkeyframe_;
            bool bnewdata_;
            bool b_reset_thread_;
            std::mutex mMutexAccept;
            std::mutex mMutexReceiveData;
            std::mutex m_reset_collision_;

			// Variables that stores the data received from Tracking
            cv::Matx<double, 4, 4> tracked_frame_pose_;
            cMultiFrame tracked_frame_;
            
            // Frames used for triangulation
            cMultiFrame previous_keyframe_;
            cMultiFrame current_keyframe_;
	};
	
}

#endif
