#ifndef COLLISIONTESTER_H
#define COLLISIONTESTER_H

// OpenCV
#include "opencv2/core.hpp"
#include <opencv2/sfm.hpp>
#include "opencv2/highgui.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"
#include <opencv2/viz.hpp>
#include <opencv2/calib3d.hpp>

#include <fstream>

#include "cam_system_omni.h"
#include "cMultiFrame.h"
#include "cSystem.h"
#include "cTracking.h"
#include "cMultiKeyFrame.h"
#include "cMap.h"
#include "cMultiKeyFrameDatabase.h"


void FramesUpdate(vector<string> current_frame, vector<string> &previous_frame,
                  cv::Mat current_pose, cv::Mat &previous_pose);

void TransformationMatrixBetweenFrames(cv::Mat &camera_pose_respect_to_body, cv::Mat &previous_base_pose_respect_to_world,
                                       cv::Mat &current_base_pose_respect_to_world, cv::Mat &transformation);

void ComputeFundamentalMatrix(cv::Mat &transformation_between_frames, cv::Mat &Intrinsic_parameters, cv::Mat &fundamental_matrix);

void DrawEpipolarlines(cv::Mat &first_image, cv::Mat &second_image, cv::Mat &lines_image,
                       std::vector <cv::Vec3f> lines);

double DistancePointToLine( cv::Point2f point, cv::Vec3f epiline);

void writeCSV(string filename, cv::Mat m){
    ofstream myfile;
    myfile.open(filename.c_str());
    myfile<< cv::format(m, cv::Formatter::FMT_CSV) << std::endl;
    myfile.close();
};

void DrawKeyPointsInImage(std::vector <cv::Point2f> &point_image2, cv::Mat& image2);

void ComputeProjectionMatrices(cv::Mat &camera_intrinsics, cv::Mat &transformation_mat, cv::Mat &projective1, cv::Mat &projective2);
void FromMatchesToVectorOfPoints(std::vector<cv::KeyPoint> &keypoints_frame1, std::vector<cv::KeyPoint> &keypoints_frame2,
                                 std::vector<cv::DMatch> &matches, std::vector <cv::Point2f> &points_frame1,std::vector <cv::Point2f> &points_frame2);
void GetArrayOfPoints(std::vector<cv::Point2f> &points_frame1, std::vector<cv::Point2f> &points_frame2, std::vector<cv::Mat> &Array_of_points);

std::vector<cv::DMatch> MatchingWithEpipolarConstraint(const cv::Mat &F, const cv::Mat &image1, const cv::Mat &descriptors1, const std::vector<cv::KeyPoint> &keypoints1,
                                                       const cv::Mat &image2, const cv::Mat &descriptors2, const std::vector<cv::KeyPoint> &keypoints2);

#endif // COLLISIONTESTER_H
