// The original version was released under the following license
/**
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/

// All modifications are released under the following license
/**
* This file is part of MultiCol-SLAM
*
* Copyright (C) 2015-2016 Steffen Urban <rurbste at googlemail.com>
* For more information see <https://github.com/urbste/MultiCol-SLAM>
*
* MultiCol-SLAM is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* MultiCol-SLAM is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with MultiCol-SLAM . If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <thread>
#include <mutex>
#include <signal.h>

#include <opencv2/core/core.hpp>

#include "cTracking.h"
#include "cConverter.h"
#include "cam_model_omni.h"
#include "cSystem.h"
#include "LadybugDriver.h"

using namespace std;

void LoadImagesAndTimestamps(
        const int startFrame,
        const int endFrame,
        const string path2imgs,
        vector<vector<string>> &vstrImageFilenames,
        vector<double> &vTimestamps, const string& path2MCScalibrationFiles);

// Manages the interruption signal coming from Crtl+C
volatile bool stop_request = false;

void code_execution_handler(int s) {
  printf("Caught signal in the handler %d\n", s);
  stop_request = true;
}

int main(int argc, char **argv)
{
        if (argc != 5)
        {
                cerr << endl << "Usage: ./MultiCol_Slam_Lafida vocabulary_file slam_settings_file path_to_settings path_to_img_sequence" << endl;
                return 1;
        }

        string path2voc = string(argv[1]);
        string path2settings = string(argv[2]);
        string path2calibrations = string(argv[3]);
        string path2imgs = string(argv[4]);

        cout << endl << "MultiCol-SLAM Copyright (C) 2016 Steffen Urban" << endl << endl;

        // Handle ctrl-c
        struct sigaction sigIntHandler;
        sigIntHandler.sa_handler = code_execution_handler;
        sigemptyset(&sigIntHandler.sa_mask);
        sigIntHandler.sa_flags = 0;

        sigaction(SIGINT, &sigIntHandler, NULL);

        // --------------
        // 1. Tracking settings
        // --------------
        cv::FileStorage frameSettings(path2settings, cv::FileStorage::READ);

        int traj = (int)frameSettings["traj2Eval"];
        string trajs = to_string(traj);
        const int endFrame = (int)frameSettings["traj.EndFrame"];
        const int startFrame = (int)frameSettings["traj.StartFrame"];

        // Loading the number of cameras
        string multicam_calibration = path2calibrations + "/MultiCamSys_Calibration.yaml";
        cv::FileStorage calib_data(multicam_calibration, cv::FileStorage::READ);
        const int nrCams = (int)calib_data["CameraSystem.nrCams"];
        //const int nrCams = 3;

        // Frame Grabber thread comunication pointers
        MultiColSLAM::DriverJpeg *mpFramesGrabber;
        std::thread* mptFramesGrabber;

        // --------------
        // 4. Load image paths and timestamps
        // --------------
        /*vector<vector<string>> imgFilenames;
        vector<double> timestamps;
        LoadImagesAndTimestamps(startFrame, endFrame, path2imgs, imgFilenames, timestamps, path2calibrations); //***** Added path2calibrations parameter
        */

        //********* Added section ************************
        //Initialize the camera frames acquisition thread if the online settings is active
        //if not the images are loaded through a txt file
        int online_mode = (int)frameSettings["Online"];
	    vector<vector<string>> imgFilenames;
        vector<double> timestamps;	
	    int nImages = 0;

        // Vector for tracking time statistics
        vector<float> vTimesTrack;

        if(online_mode){
            mpFramesGrabber = new MultiColSLAM::DriverJpeg(nrCams);
            mptFramesGrabber = new thread(&MultiColSLAM::DriverJpeg::Run, mpFramesGrabber);
        }
        else {
            LoadImagesAndTimestamps(startFrame, endFrame, path2imgs, imgFilenames, timestamps, path2calibrations);
            nImages = imgFilenames[0].size();

            vTimesTrack.resize(nImages);
        }

        MultiColSLAM::cSystem MultiSLAM(path2voc, path2settings, path2calibrations, true);

//        cout << endl << "-------" << endl;
//        cout << "Start processing sequence ..." << endl;
        //cout << "Images in the sequence: " << nImages << endl << endl;

        // Main loop
        //const int nrCams = static_cast<int>(imgFilenames.size());
        std::vector<cv::Mat> frame_images(nrCams);

	// For debugging visualization
	    int ni = 0;
	    double tframe=0,timestep = 0,frame_debug;
        while (!stop_request)
        {
            std::string img_name;
            std::chrono::steady_clock::time_point pini = std::chrono::steady_clock::now();
            if(online_mode){
                bool new_data_available = mpFramesGrabber->CurrentFrameTransfer(frame_images,frame_debug);

                // If there is no new data from the camera skip the tracking process, as we dont want to use the
                // previous frame again.
                if(!new_data_available){
                    continue;
                }
                else {
		    // If something else is needed
                    //cv::imshow("Record", frame_images[1]);
                    //cv::waitKey(40);
		    //cout << "timestamp"<< setprecision(11) << frame_debug << endl;
		    tframe = timestep/16;
		    timestep++;
		    //cout << "Virtual timestamp"<<tframe << endl;
                }
                /*bool no_data = false;
                 * for (int c = 0; c < nrCams; ++c){
                 * if(frame_images[c].empty()){
                 * no_data = true;
                 * }
                 * }
                 * if(no_data){
                 * std::this_thread::sleep_for(std::chrono::milliseconds(30));
                 * continue;
                 * }*/
                // Sleeps before asking for new data
                //std::this_thread::sleep_for(std::chrono::milliseconds(30));
            }
            else{
                // Offline version of the SLAM system
                if(ni > nImages-1){
                    break; // Stops the system when there is no more images to process
                }

                // Loads images from txt file
                std::chrono::steady_clock::time_point p_read_image = std::chrono::steady_clock::now();
                cout << "----------" << endl;

                for (int c = 0; c < nrCams; ++c)
                {
                    // Printing the images files to get file for a new implementation in the collision detector thread
                    frame_images[c] = cv::imread(imgFilenames[c][ni], CV_LOAD_IMAGE_GRAYSCALE); // NFM: Always taking the grayscale image, advantageous to have the RGB one(?)

                    // Image preprocessing
                    cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
                    clahe->setClipLimit(5);

                    clahe->apply(frame_images[c], frame_images[c]);

                    if (frame_images[c].empty())
                    {
                        cerr << endl << "Failed to load image at: " << imgFilenames[c][ni] << endl;
                        return 1;
                    }
                }

                tframe = timestamps[ni];
                std::chrono::steady_clock::time_point p_read_image_end = std::chrono::steady_clock::now();
                double read_image = std::chrono::duration_cast<std::chrono::milliseconds>(p_read_image_end - p_read_image).count();
//                cout << "1. Read image time " << std::setprecision(5) << read_image << " ms" << endl;

                img_name = imgFilenames[0][ni];
                // Search for the substring in string
                size_t pos = img_name.find(path2imgs);

                if (pos != std::string::npos)
                {
                    // If found then erase it from string
                    img_name.erase(pos, path2imgs.length());
                }
            }



            std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
            // Pass the image to the SLAM system
            MultiSLAM.TrackMultiColSLAM(frame_images, tframe,img_name);

            std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();

            double ttrack = std::chrono::duration_cast<std::chrono::duration<double> >(t2 - t1).count();

            if(!online_mode){
                vTimesTrack[ni] = ttrack;

                // Wait to load the next frame
                double T = 0.0f;
                if (ni < nImages - 1) {
                    T = timestamps[ni + 1] - tframe;
                }
                else if (ni > 0) {
                    T = tframe - timestamps[ni - 1];
                }

                if (ttrack < T) {
                    cout << "Missing to next frame: " << std::chrono::duration<double>((T - ttrack)).count() << endl;
                    std::this_thread::sleep_for( std::chrono::duration<double>((T - ttrack)));
                }

                // Additional sleep for giving collision time
                std::this_thread::sleep_for(std::chrono::milliseconds(850));

                ni++;
            }else{
		        vTimesTrack.push_back(ttrack);
            }

            std::chrono::steady_clock::time_point pend = std::chrono::steady_clock::now();
            double while_time = std::chrono::duration_cast<std::chrono::milliseconds>(pend - pini).count();
            double tracking_time = std::chrono::duration_cast<std::chrono::milliseconds >(t2 - t1).count();

//            cout << "2. tracking time "<< std::setprecision(5) << tracking_time << " ms" << endl;
//            cout << "3. Complete time "<< std::setprecision(5) << while_time << " ms" << endl;
//            cout << "----------" << endl;


        }

        // Stop all threads
        MultiSLAM.Shutdown();

        // Stop the frame acquisition thread
        if(online_mode){
            mpFramesGrabber->Stop();
            mptFramesGrabber->join();
        }
/*
        // Tracking time statistics
        sort(vTimesTrack.begin(), vTimesTrack.end());
        float totaltime = 0;
        for (int ni = 0; ni<nImages; ni++)
        {
                totaltime += vTimesTrack[ni];
        }
        cout << "-------" << endl << endl;
        cout << "median tracking time: " << vTimesTrack[nImages / 2] << endl;
        cout << "mean tracking time: " << totaltime / nImages << endl;

        // Save camera trajectory
        MultiSLAM.SaveMKFTrajectoryLAFIDA("MKFTrajectory.txt");
*/
        return 0;
}

void LoadImagesAndTimestamps(const int startFrame,
        const int endFrame,
        const string path2imgs,
        vector<vector<string>> &vstrImageFilenames,
        vector<double> &vTimestamps, const string& path2MCScalibrationFiles)
{
        //*************** Added section **************
        string mcs_settings = path2MCScalibrationFiles + "/MultiCamSys_Calibration.yaml";
        cv::FileStorage mcs_calib_data(mcs_settings, cv::FileStorage::READ);
        int numCams = (int)mcs_calib_data["CameraSystem.nrCams"];
        //********************************************

        vstrImageFilenames.resize(numCams); // Takes the filenames of "3" cameras
                                                                  // Modify it to add more cameras(?)
        ifstream fTimes; // Input stream to operate on files
        string images_and_timestamps_file;
        cout << "Enter the images file name:\n>";
        getline(cin, images_and_timestamps_file);
        string strPathTimeFile = path2imgs + "/" + images_and_timestamps_file + ".txt";
        cout << "String: "<< strPathTimeFile;
//        string strPathTimeFile = path2imgs + "/images_and_timestamps.txt";

        fTimes.open(strPathTimeFile.c_str());
        string line;


        int cnt = 1;
        while (std::getline(fTimes, line))
        {
                if (cnt >= startFrame && cnt < endFrame) // skip until startframe
                {
                        std::istringstream iss(line);
                        double timestamp;
                        //******** Added section*************************
                        bool emptyFlag = false;
                        vector<string> pathimg(numCams);

                        if(!(iss >> timestamp)){
                                emptyFlag = true;
                                }
                        for(int n=0; n < numCams; n++){
                                if(!(iss >> pathimg[n])) emptyFlag = true;
                                }
                        if(emptyFlag) break;

                        vTimestamps.push_back(timestamp);
                        for(int t=0; t < numCams; t++){
                                vstrImageFilenames[t].push_back(path2imgs + '/' + pathimg[t]);
                                }
                        //***********************************************

                }
                ++cnt;

        }
}
