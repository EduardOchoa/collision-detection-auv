function [polygon_coordinates] = CreatePolygon(sides,radius)
% [polygon_x_coordinates,polygon_y_coordinates] = CreatePolygon(sides,radius)
%
% Returns the x and y coordinates of a N sides polygon. It takes as input
% the number of sides that are desired and the radius of the polygon.

    polygon_coordinates = [];
    
    theta = 0:2*pi/sides:2*pi;
    polygon_x_coordinates = radius * cos(theta);
    polygon_y_coordinates = radius * sin(theta);
    
    for i = 1:size(polygon_x_coordinates,2)
        polygon_coordinates = [polygon_coordinates;polygon_x_coordinates(i),polygon_y_coordinates(i)];
    end
    plot(polygon_x_coordinates,polygon_y_coordinates);
end