clear all;
close all;
clc;

addpath(genpath('InterpolationEquiCode'));

doGroundtruth = false;
doValidmask = true;
counter = 0;
maxAcceptableInc3Dpoints = 3000;

% Variables to select datasets
dataset = 4;
forces_flag = false;

if dataset == 1
    cam_filepath = '/home/eduochoa/PointClouds/BoreasLong_PC/CSV_CamPoses';
    pts_filepath =  '/home/eduochoa/PointClouds/BoreasLong_PC/CSV_PointClouds';
    panoramic_imgs_path = '/home/eduochoa/Datasets/BoreasLongFiles/panos_1.5m';
    sim_flag = 0;
    data_size = 482;
elseif dataset == 2
    cam_filepath = '/home/eduochoa/PointClouds/Sim_all_cams_fixed/CSV_CamPoses';
    pts_filepath =  '/home/eduochoa/PointClouds/Sim_all_cams_fixed/CSV_PointClouds';
    panoramic_imgs_path = '/home/eduochoa/Datasets/ladybug_simulated_panos';
    sim_flag = 1;
    data_size = 265;
elseif dataset == 3
    cam_filepath = '/home/eduochoa/PointClouds/Boreas_Short/CSV_CamPoses';
    pts_filepath =  '/home/eduochoa/PointClouds/Boreas_Short/CSV_PointClouds';
    panoramic_imgs_path = '/home/eduochoa/panos';
    sim_flag = 0;
    data_size = 55;
elseif dataset == 4
    cam_filepath = '/home/eduochoa/PointClouds/BoreasLong2/CSV_CamPoses';
    pts_filepath =  '/home/eduochoa/PointClouds/BoreasLong2/CSV_PointClouds';
    panoramic_imgs_path = '/home/eduochoa/Datasets/all';
    sim_flag = 0;
    data_size = 1613;
    
end

% Mask used for the ground truth comparison
% panoramic_valid_mask_img_filename = [panoramic_imgs_path filesep 'pano_valid_mask.png'];
panoramic_valid_mask_img_filename = ['pano_valid_mask.png'];

panoramic_img_dir = dir(strcat(panoramic_imgs_path,'/*.jpg'));
% 
% fullDateStr = datestr(now,30);
% incVidObj = VideoWriter(['risk_animation_' fullDateStr '_inc.avi']);
% incVidObj.FrameRate = 2;
% open(incVidObj);


if doGroundtruth
    sfmVidObj = VideoWriter(['risk_animation_' fullDateStr '_sfm.avi']);
    sfmVidObj.FrameRate = 2;
    open(sfmVidObj);
end;

riskColormap = jet(64);

if doValidmask
    panoValidMask = (rgb2gray(imread(panoramic_valid_mask_img_filename)) > 128);
    % newfig(normal(double(panoValidMask)));
end;

if forces_flag
    % Point cloud of the total trajectory in world coordinates
    world_pc = [];
    % Camera positions in world coordinates
    cam_trajectory = [];
    % Repulsion forces
    repulsion_vectors = [];
end

% final_number = data_size;
% final_number = 120+20;

for pcd_i = 1000:data_size
    cam_data = readtable(sprintf('%s%s%s_%d.csv', cam_filepath,filesep,'CAM',pcd_i),'Delimiter',' ');
    pts_data = readtable(sprintf('%s%s%s_%d.csv', pts_filepath,filesep,'PointCloud',pcd_i));
    
    % Getting the current image sequence name
    current_frame_name = cell2mat(cam_data{2,19});
    idx_frameID_in_str = strfind(current_frame_name,'frame');
    frameID = current_frame_name(idx_frameID_in_str:idx_frameID_in_str+9);
    
    % Checking which is the panoramic img correspondence
    for img_n = 1:size(panoramic_img_dir,1)
        if strfind(panoramic_img_dir(img_n).name,frameID)
            panoram_img = imread(strcat(panoramic_imgs_path,'/',panoramic_img_dir(img_n).name));
        end
    end
    
    if sim_flag
        pani(:,:,1) = panoram_img(:,:,3);
        panoram_img(:,:,3) = panoram_img(:,:,1);
        panoram_img(:,:,1) = pani(:,:,1);
    end
    
    % Get timestamps
    timestamp_1 = cam_data{1,18};
    timestamp_2 = cam_data{2,18};
    
    % Compute relative pose
    w_T_b1 = reshape(cam_data{1,2:17},4,4)';
    w_T_b2 = reshape(cam_data{2,2:17},4,4)';
    b2_T_w = eye(4,4);
    b2_T_w(1:3,1:3) = w_T_b2(1:3,1:3)';
    b2_T_w(1:3,4) = -w_T_b2(1:3,1:3)' * w_T_b2(1:3,4);
    b2_T_b1 = b2_T_w*w_T_b1;
    
    % Get data
    C1 = b2_T_b1(1:3,4);
    C2 = zeros(3,1);
    %     v = C2 - C1;
    incPoints3D_raw = pts_data{:,1:3};
    delta_time = timestamp_2-timestamp_1;
    
    % For testing the speed effect
    factor_of_time = 1/2;
    delta_time = factor_of_time * delta_time;
    
    %% Nuno's block here
    
    if doValidmask
        incPoints3D = edu_verifyvalid3dpoints(incPoints3D_raw,panoValidMask);
    else
        incPoints3D = incPoints3D_raw;
    end;
    
    if size(incPoints3D,1) > maxAcceptableInc3Dpoints
        randIdx = randperm(size(incPoints3D,1),maxAcceptableInc3Dpoints);      
        incPoints3D = incPoints3D(randIdx,:);
    end;
    
    [minD_norm,t_norm,ptCloudRiskNear,inlierPoints2,I2modif,equi_risk_smoothed_inc,risks] = edu_processriskomni(C1,C2,b2_T_b1,delta_time,incPoints3D,panoram_img,riskColormap,counter);
    
    
%     frame = getframe(gcf);
%     newheight = floor(size(frame.cdata,1)/16)*16;
%     newwidth = floor(size(frame.cdata,2)/16)*16;
%     frame.cdata = frame.cdata(1:newheight,1:newwidth,:);
%     writeVideo(incVidObj,frame);
%     
    if doGroundtruth
        
        % Test for reading ground truth depth maps
        [~,current_frame_name_short] = fileparts(current_frame_name);
        frameNumberInFile = sscanf(current_frame_name_short, 'camera1_frame%d_rect');
        
        depth_image_path = ['/home/eduochoa/Matlab/depth_equi_cam5_gsfm_f_06_m_08_fix_new_intr_hard_ext_prunned_guided/camera5_frame' num2str(frameNumberInFile) '.jpg_equi.bin'];
        
        % Load image
        equi_depth_image = read_binary_image(depth_image_path);
        
        % Subsample depth image - IMPORTANT TO USE NEAREST PT to not add smoothing
        % witn 0 depth values (Induces gap in glow!)
        equi_depth_image = imresize(equi_depth_image,0.25,'nearest');
        
        % Get pixel coordinates
        [px_x,px_y] = meshgrid(1:size(equi_depth_image,2),1:size(equi_depth_image,1));
        px_z = equi_depth_image;
        
        % Remove zero points (UNKNOWN DEPTH)
        px_x = px_x(equi_depth_image~=0);
        px_y = px_y(equi_depth_image~=0);
        px_z = px_z(equi_depth_image~=0);
        
        if length(px_x) > 3000
            randIdx = randperm(length(px_x),3000);
            
            px_x = px_x(randIdx);
            px_y = px_y(randIdx);
            px_z = px_z(randIdx);
        end;
        
        % Get equirectangulat pixels in 3D
        azimuth_area_w_rad = (2*pi) / size(equi_depth_image,2);
        elevation_area_w_rad = pi/size(equi_depth_image,1);
        % [pt_x, pt_y, pt_z ] = EquiPxToXYZ( azimuth_area_w_rad, elevation_area_w_rad, px_x, px_y, px_z );
        warning('Nuno flipped left and right the azimuth for the generating the 3D points')
        [pt_x, pt_y, pt_z ] = EquiPxToXYZ( azimuth_area_w_rad, elevation_area_w_rad, (size(equi_depth_image,2) + 1 - px_x), px_y, px_z );
        
        % % %     % Create smoothed version
        % % %     equi_sample_deg = 0.5;
        % % %     n_neighbors_used = 50;
        % % %     max_neighbor_smoothing = 0.2;
        % % %     [ equi_depth_smoothed ] = generate_equi_image_interpolate( equi_sample_deg, [pt_x, pt_y, pt_z], px_z, n_neighbors_used, max_neighbor_smoothing );
        
        
        
        
        % Find the scale multipliers that alight the two 3D point clouds - THIS IS VERY APPROXIMATE - MAY FAIL
        %     incPoints3D = wPoints3D;
        sfmPoints3D = double([pt_x  pt_y  pt_z]);
%         [incMultiplier,sfmMultiplier] = edu_estimaterelativescale(incPoints3D,sfmPoints3D);
        incMultiplier = 2.4071;
        sfmMultiplier = 1.4390;
%         incMultiplier = 11.7007
%         sfmMultiplier = 0.9538
        
        sfmPoints3DscaledToInc = sfmPoints3D / sfmMultiplier * incMultiplier;
        [minD_norm,t_norm,ptCloudRiskNearSfm,inlierPoints2Sfm,I2modifSfm,equi_risk_smoothed_sfm,risksSfM] = edu_processriskomni(C1,C2,b2_T_b1,delta_time,sfmPoints3DscaledToInc,panoram_img,riskColormap,pcd_i); % Correct the pcd_i implementation
        
        frame = getframe(gcf);
        newheight = floor(size(frame.cdata,1)/16)*16;
        newwidth = floor(size(frame.cdata,2)/16)*16;
        frame.cdata = frame.cdata(1:newheight,1:newwidth,:);
        writeVideo(sfmVidObj,frame);
        
        resultStruct(pcd_i).panoram_img = panoram_img;
        
        resultStruct(pcd_i).C1 = C1;
        resultStruct(pcd_i).C2 = C2;
        resultStruct(pcd_i).b2_T_b1 = b2_T_b1;
        resultStruct(pcd_i).delta_time = delta_time;
        
        resultStruct(pcd_i).I2modif = I2modif;
        resultStruct(pcd_i).I2modifSfm = I2modifSfm;
        
        resultStruct(pcd_i).equi_risk_smoothed_inc = equi_risk_smoothed_inc;
        resultStruct(pcd_i).equi_risk_smoothed_sfm = equi_risk_smoothed_sfm;
        
        resultStruct(pcd_i).incPoints3D = incPoints3D;
        resultStruct(pcd_i).sfmPoints3DscaledToInc = sfmPoints3DscaledToInc;
        
    end;

    if forces_flag
        [total_force,world_pc,cam_trajectory,repulsion_vectors] = compute_forces(C1,C2,incPoints3D,risks.values,w_T_b1,w_T_b2,risks.colors,world_pc,cam_trajectory,repulsion_vectors);
    end
    
%     if pcd_i ~= data_size 
%         close all;
%     end
    close all;
    counter = counter +1;
end

% close(incVidObj);

if doGroundtruth
    close(sfmVidObj);
end;

save(['risk_animation_' fullDateStr '_results.mat'],'-v7.3');

% load('risk_animation_20190606T102340_results.mat');

if doGroundtruth
    
    comparisonVidObj = VideoWriter(['risk_animation_' fullDateStr '_comparison.avi']);
    comparisonVidObj.FrameRate = 2;
    open(comparisonVidObj);
    
    
    for pcd_i = 1:length(resultStruct)
        if ~isempty(resultStruct(pcd_i).panoram_img)
            
            
            figure('units','normalized','outerposition',[0 0 1 1]);
            
            % Visualize risk indicators with risk colors
            subplot(2,2,1);
            imshow(resultStruct(pcd_i).I2modif)
            title('Risk from Incremental')
            
            subplot(2,2,2);
            imshow(resultStruct(pcd_i).I2modifSfm);
            title('Risk from SfM')
            
            subplot(2,2,3);
            equi_risk_diff = resultStruct(pcd_i).equi_risk_smoothed_inc - resultStruct(pcd_i).equi_risk_smoothed_sfm;
            % equi_risk_diff = equi_risk_smoothed_inc - equi_risk_smoothed_sfm;
            imshow(normal(equi_risk_diff));
            title('Risk difference (normalized to [0 1])')
            
            
            % IriskDiff_RGB_small = ind2rgb(gray2ind(equi_risk_diff+0.5),polarColormap);
            IriskDiff_RGB_small = ind2rgb(gray2ind(equi_risk_diff+0.5),riskColormap);
            % figure; imshow(IriskDiff_RGB_small);
            
            IriskDiff_RGB = imresize(IriskDiff_RGB_small,[size(resultStruct(pcd_i).I2modif,1) size(resultStruct(pcd_i).I2modif,2)]);
            
            % Create the color coded heatmap image
            I2_hsv = rgb2hsv(resultStruct(pcd_i).panoram_img);
            IriskDiff_hsv = rgb2hsv(IriskDiff_RGB);
            
            I2modif_hsv(:,:,1) = IriskDiff_hsv(:,:,1);
            I2modif_hsv(:,:,2) = IriskDiff_hsv(:,:,2) * 0.75;
            I2modif_hsv(:,:,3) = I2_hsv(:,:,3);
            
            I2Diffmodif = hsv2rgb(I2modif_hsv);
            
            subplot(2,2,4);
            imshow(I2Diffmodif)
            title('Risk difference Color Coded')
%             
            frame = getframe(gcf);
            newheight = floor(size(frame.cdata,1)/16)*16;
            newwidth = floor(size(frame.cdata,2)/16)*16;
            frame.cdata = frame.cdata(1:newheight,1:newwidth,:);
            writeVideo(comparisonVidObj,frame);
            
            close all;
            
        end;
    end;
    
    close(comparisonVidObj);
    
    FP = [];
    FN = [];
    size_sequence = 0;
%     figure;
    for pcd_i = 1:length(resultStruct)
        if ~isempty(resultStruct(pcd_i).panoram_img)
            equi_risk_diff = resultStruct(pcd_i).equi_risk_smoothed_inc - resultStruct(pcd_i).equi_risk_smoothed_sfm;
            resultStruct(pcd_i).percent_area_equi_risk_diff_larger_plus01 = sum(equi_risk_diff(:)>0.1)/prod(size(equi_risk_diff));
            resultStruct(pcd_i).percent_area_equi_risk_diff_smaller_minus01 = sum(equi_risk_diff(:)<-0.1)/prod(size(equi_risk_diff));
            FP= [FP resultStruct(pcd_i).percent_area_equi_risk_diff_larger_plus01];
            FN = [FN resultStruct(pcd_i).percent_area_equi_risk_diff_smaller_minus01];
            size_sequence = size_sequence + 1;
        end;
    end;
    
    % Plotting the performance
    FP = FP * 100;
    FN = FN * 100;
    x_images = linspace(1,size_sequence,size_sequence);
    figure;
    plot(x_images,FP,'LineWidth',1.5)
    title('Percentage of false positives (FP) over time');
    xlabel('Multi-keyframe number');
    ylabel('Percentage (%)');
    
    figure;
    plot(x_images,FN,'LineWidth',1.5)
    title('Percentage of false negative (FN) over time');
    xlabel('Multi-keyframe number');
    ylabel('Percentage (%)');
    
end;

return;


%newfig(normal(equi_risk_diff>0.1))
