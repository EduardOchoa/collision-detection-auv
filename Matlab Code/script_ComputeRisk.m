clear all;

% Script to compute risk 
d = linspace(0.01,5,200);
% t = linspace(0.01,5,200);

t = linspace(-0.5,5,200);

[D,T] = meshgrid(d,t);

% Parameters
k = 0.4;
max_step_saturated = 2;
max_value = 1;
tnOffset = 0.5;

[ f ] = compute_risk( D, T, k, max_step_saturated, max_value, tnOffset );

figure;
h = surf(D,T,f);
shading interp
set(h,'edgecolor','none');
axis equal;
axis tight;
ax = gca; ax.YDir = 'normal';