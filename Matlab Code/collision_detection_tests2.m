clear all;
close all;
clc;

cam_filepath = '/home/eduochoa/PointClouds/Boreas_CAMCSVFiles_with_ImageNames/CSV_CamPoses';
pts_filepath =  '/home/eduochoa/PointClouds/Boreas_CAMCSVFiles_with_ImageNames/CSV_PointClouds';
forces_filepath = '/home/eduochoa/PointClouds/Boreas_CAMCSVFiles_with_ImageNames/CSV_RepulsiveForces';
panoramic_imgs_path = '/home/eduochoa/panos';

cam_filepath = '/home/eduochoa/PointClouds/Sim_all_cams_fixed/CSV_CamPoses';
pts_filepath =  '/home/eduochoa/PointClouds/Sim_all_cams_fixed/CSV_PointClouds';
panoramic_imgs_path = '/home/eduochoa/Datasets/ladybug_simulated_panos';
panoramic_img_dir = dir(strcat(panoramic_imgs_path,'/*.jpg'));

% Point cloud of the total trajectory in world coordinates
world_pc = [];
% Camera positions in world coordinates
cam_trajectory = [];
% Repulsion forces
repulsion_vectors = [];

figure(1);

for pcd_i = 3:18
   cam_data = readtable(sprintf('%s%s%s_%d.csv', cam_filepath,filesep,'CAM',pcd_i),'Delimiter',' ');
   pts_data = readtable(sprintf('%s%s%s_%d.csv', pts_filepath,filesep,'PointCloud',pcd_i));
   forces_data = readtable(sprintf('%s%s%s_%d.csv', forces_filepath,filesep,'REP',pcd_i));
   
   % Getting the current image sequence name
   current_frame_name = cell2mat(cam_data{2,19});
   idx_frameID_in_str = strfind(current_frame_name,'frame');
   frameID = current_frame_name(idx_frameID_in_str:idx_frameID_in_str+9);
   
   % Checking which is the panoramic img correspondence
   for img_n = 1:size(panoramic_img_dir,1)
       if strfind(panoramic_img_dir(img_n).name,frameID)
           panoram_img = imread(strcat(panoramic_imgs_path,'/',panoramic_img_dir(img_n).name));
       end
   end
   
   % Get timespamps
   timestamp_1 = cam_data{1,18};
   timestamp_2 = cam_data{2,18};
   
   % Compute relative pose
   w_T_b1 = reshape(cam_data{1,2:17},4,4)';
   w_T_b2 = reshape(cam_data{2,2:17},4,4)';
   b2_T_w = eye(4,4);
   b2_T_w(1:3,1:3) = w_T_b2(1:3,1:3)';
   b2_T_w(1:3,4) = -w_T_b2(1:3,1:3)' * w_T_b2(1:3,4);
   b2_T_b1 = b2_T_w*w_T_b1;
   
   % Get data
   C1 = b2_T_b1(1:3,4);
   C2 = zeros(3,1);
   v = C2 - C1;
   P = pts_data{:,1:3};
   delta_time = timestamp_2-timestamp_1;
   norm_factor = (norm(v)/delta_time);
   
   % Compute risk
   % Normalizing the vectors to have seconds as units   
   v_n = v/norm_factor;
   C1_n = C1/norm_factor;
   P_n = P/norm_factor;
   x1 = repmat(C1_n',size(P_n,1),1);
   x = x1 - P_n;
   
   % d_min and t_min computation
   v_n_mat = repmat(v_n',size(P_n,1),1);
   t_min_num = dot(x,v_n_mat,2);
   t_min = -t_min_num/(norm(v_n)^2);
   t_min = t_min-1;
   
   d_min_num = cross(v_n_mat,x,2);
   d_min = vecnorm(d_min_num,2,2)/norm(v_n);

   % Parameters
   k = 1;
   max_step_saturated = 1;
   max_value = 10;
   sigma = k.*t_min;
   
   risk = [];

   for i = 1:size(d_min,1)
      if t_min(i) <= 0
          risk(i) = 0;
          continue;
      else
          risk(i) = k/sigma(i) * (exp(-d_min(i)^2/(2*sigma(i)^2)));
      end
   end
   
   if max_value~=0
       risk = (max_step_saturated*max_value).*risk;
       risk(risk>max_value) = max_value;
   else
       risk = max_step_saturated.*risk;
   end

   % Displaying the points into the panoramic images
   figure(2)
   imshow(panoram_img)

   theta = [];
   azimuth = [];
   for i = 1:size(P,1)
       theta(i) = -atan2(P(i,2),P(i,1));
       azimuth(i) = acos(P(i,3)/norm(P(i,:)));
   end

   px = ((theta(:)+pi)./(2*pi)).*size(panoram_img,2);
   py = (azimuth(:)./pi).*size(panoram_img,1);

   % Setting a colormap for displaying the points
   ran=range(risk); %finding range of data
   min_val = min(risk);%finding maximum value of data
   max_val = max(risk);%finding minimum value of data
   color_value = floor(((risk-min_val)/ran)*63)+1; 
   rgb_mat = zeros(size(risk,2),3);
   map = colormap('jet');
   
   figure(2), hold on;
   for i = 1:size(px,1)
       m_idx = color_value(i);
       rgb_mat(i,:) = map(m_idx,:);
       plot(px(i),py(i),'Marker','+','Color',rgb_mat(i,:),'MarkerSize',10,'LineWidth',1);
%        plot(px(i),py(i),'r+','MarkerSize',10,'LineWidth',1);
   end
   hold off;
   
   % Figure;
   figure(1);
   % Points
   scatter3(P(:,1),P(:,2),P(:,3),10,'b','filled');
   hold on;
   % Cameras
   scatter3(C1(1),C1(2),C1(3),20,'k','filled');
   scatter3(C2(1),C2(2),C2(3),20,'r','filled');
   % Vector v
   plot3([C1(1), C1(1)+v(1)], [C1(2), C1(2)+v(2)], [C1(3), C1(3)+v(3)], 'b');
   axis equal;
   hold off;
   
   % Computing the weighted forces
   C2_mat = repmat(C2',size(P_n,1),1);
   repulsive_forces = C2_mat - P;
   
   weights = risk'./vecnorm(repulsive_forces,2,2).^3;
   weighted_forces = weights.* repulsive_forces;
   
   total_force = sum(weighted_forces,1);
   
   % Points in world coordinates
   P_hom = cat(2,P,ones(size(P,1),1));
   w_P = mtimes(w_T_b2,P_hom');
   w_P = w_P';
   
   % Camera pose in world coordinates
   cam_pose = w_T_b2(1:3,4);

   % Plotting points, trajectory and repulsive forces
   figure(3);
   hTrajectory = scatter3(w_P(:,1),w_P(:,2),w_P(:,3),1.5,rgb_mat(:,:),'filled');
   hold on;
   if ~isempty(world_pc)
       scatter3(world_pc(:,1),world_pc(:,2),world_pc(:,3),0.5,'k','filled');
   end
   xlabel('x'); ylabel('y'); zlabel('z');
   
   %Cameras
   if isempty(cam_trajectory)
       initial_cam_pose = w_T_b1(1:3,4);
       scatter3(initial_cam_pose(1),initial_cam_pose(2),initial_cam_pose(3),20,'g','filled');
       scatter3(cam_pose(1),cam_pose(2),cam_pose(3),20,'y','filled');
       % Vector t
       t = cam_pose - initial_cam_pose;
       plot3([initial_cam_pose(1), initial_cam_pose(1)+t(1)], [initial_cam_pose(2), initial_cam_pose(2)+t(2)],...
           [initial_cam_pose(3), initial_cam_pose(3)+t(3)], 'b');
       cam_trajectory = [cam_trajectory; initial_cam_pose'];
   else
       scatter3(cam_trajectory(1:end,1),cam_trajectory(1:end,2),cam_trajectory(1:end,3),20,'m','filled');
       scatter3(cam_pose(1),cam_pose(2),cam_pose(3),20,'y','filled');
       
       for step = 1:size(cam_trajectory,1)
           prev_pose = cam_trajectory(step,:);
           % Vector t
           if step == size(cam_trajectory,1)
               t = cam_pose' - prev_pose;
           else
               t =  cam_trajectory(step+1,:) - prev_pose;
           end
           plot3([prev_pose(1), prev_pose(1)+t(1)], [prev_pose(2), prev_pose(2)+t(2)],...
                [prev_pose(3), prev_pose(3)+t(3)], 'b');
       end
   end
%    axis equal;
  
   
   % repulsion vector
   % Changing the vector reference
   w_total_force =  w_T_b2(1:3,1:3) * total_force';
   w_total_force = w_total_force';
   repulsion_vector =  cam_pose' + w_total_force;
   if isempty(repulsion_vectors)
        mArrow3(cam_pose',repulsion_vector,'color',[0.89 0.47 0.2],'stemWidth',0.04,'tipWidth',0.06);
   else
       for vec_idx = 1:size(repulsion_vectors,1)
           mArrow3(cam_trajectory(vec_idx+1,:),repulsion_vectors(vec_idx,:),'color',[0.89 0.47 0.2],'stemWidth',0.04,'tipWidth',0.06);
       end
       mArrow3(cam_pose',repulsion_vector,'color',[0.89 0.47 0.2],'stemWidth',0.04,'tipWidth',0.06);
   end
   rotate(hTrajectory,[0 1 0],90);
   view(cam_pose);
   hold off;
   
   repulsion_vectors = [repulsion_vectors; repulsion_vector];
   cam_trajectory = [cam_trajectory; cam_pose'];
   world_pc = [world_pc; w_P(:,1:3)];
   pause(20/1000);
end