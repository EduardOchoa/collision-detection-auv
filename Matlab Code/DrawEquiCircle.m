function [circle_coordinates] = DrawEquiCircle(pt_x,pt_y,Heightpx,radius_in_deg,circle)
    pixels_per_degree = Heightpx/180;
    vertical_multiplier = radius_in_deg * pixels_per_degree;
    distortion_factor = 1/(cos(abs( (pt_y/(Heightpx/2)) -1 ) * pi/2 ) + 0.00001);
    horizontal_multiplier = vertical_multiplier * distortion_factor;
    
    xycoordinates = [horizontal_multiplier 0; 0 vertical_multiplier] * circle';
    circle_coordinates = [];
    for i = 1:size(xycoordinates,2)
        x_equirectangular_coordinate = xycoordinates(1,i)+pt_x;
        y_equirectangular_coordinate = xycoordinates(2,i)+pt_y;
        
        if x_equirectangular_coordinate > 2*Heightpx
            x_equirectangular_coordinate = 2*Heightpx;
        end
        
        if x_equirectangular_coordinate < 0
            x_equirectangular_coordinate = 0;
        end
        
        circle_coordinates = [circle_coordinates,x_equirectangular_coordinate,y_equirectangular_coordinate];
    end
    
end