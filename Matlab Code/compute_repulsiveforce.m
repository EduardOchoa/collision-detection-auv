function [total_force,new_world_pc,new_cam_trajectory,new_repulsion_vectors] = compute_repulsiveforce(C1,C2,3DPoints,risk_values,w_T_b1,w_T_b2,rgb_mat,world_pc,cam_trajectory,repulsion_vectors)

% Computing the weighted forces
P = 3DPoints;
risk = risk_values;
colormap('jet')
C2_mat = repmat(C2',size(P,1),1);
repulsive_forces = C2_mat - P;

weights = risk'./vecnorm(repulsive_forces,2,2).^2;
weighted_forces = weights.* repulsive_forces;

total_force = sum(weighted_forces,1);

% Points in world coordinates
P_hom = cat(2,P,ones(size(P,1),1));
w_P = mtimes(w_T_b2,P_hom');
w_P = w_P';

% Camera pose in world coordinates
cam_pose = w_T_b2(1:3,4);

% Plotting points, trajectory and repulsive forces
figure(3);
hTrajectory = scatter3(w_P(:,1),w_P(:,2),w_P(:,3),1.5,'Color',rgb_mat(:,:),'filled');
hold on;
if ~isempty(world_pc)
    scatter3(world_pc(:,1),world_pc(:,2),world_pc(:,3),0.5,'k','filled');
end
xlabel('x axis'); ylabel('y axis'); zlabel('z axis');

%Cameras
if isempty(cam_trajectory)
    initial_cam_pose = w_T_b1(1:3,4);
    scatter3(initial_cam_pose(1),initial_cam_pose(2),initial_cam_pose(3),20,'g','filled');
    scatter3(cam_pose(1),cam_pose(2),cam_pose(3),20,'y','filled');
    % Vector t
    t = cam_pose - initial_cam_pose;
    plot3([initial_cam_pose(1), initial_cam_pose(1)+t(1)], [initial_cam_pose(2), initial_cam_pose(2)+t(2)],...
        [initial_cam_pose(3), initial_cam_pose(3)+t(3)], 'b');
    cam_trajectory = [cam_trajectory; initial_cam_pose'];
else
    scatter3(cam_trajectory(1:end,1),cam_trajectory(1:end,2),cam_trajectory(1:end,3),20,'m','filled');
    scatter3(cam_pose(1),cam_pose(2),cam_pose(3),20,'y','filled');
    
    for step = 1:size(cam_trajectory,1)
        prev_pose = cam_trajectory(step,:);
        % Vector t
        if step == size(cam_trajectory,1)
            t = cam_pose' - prev_pose;
        else
            t =  cam_trajectory(step+1,:) - prev_pose;
        end
        plot3([prev_pose(1), prev_pose(1)+t(1)], [prev_pose(2), prev_pose(2)+t(2)],...
            [prev_pose(3), prev_pose(3)+t(3)], 'b');
    end
end
%    axis equal;


% repulsion vector
% Changing the vector reference
w_total_force =  w_T_b2(1:3,1:3) * total_force';
w_total_force = w_total_force';
repulsion_vector =  cam_pose' + w_total_force;
if isempty(repulsion_vectors)
    mArrow3(cam_pose',repulsion_vector,'color',[0.89 0.47 0.2],'stemWidth',0.04,'tipWidth',0.06);
else
    for vec_idx = 1:size(repulsion_vectors,1)
        mArrow3(cam_trajectory(vec_idx+1,:),repulsion_vectors(vec_idx,:),'color',[0.89 0.47 0.2],'stemWidth',0.04,'tipWidth',0.06);
    end
    mArrow3(cam_pose',repulsion_vector,'color',[0.89 0.47 0.2],'stemWidth',0.04,'tipWidth',0.06);
end
rotate(hTrajectory,[0 1 0],90);
view(cam_pose);
hold off;

new_repulsion_vectors = [repulsion_vectors; repulsion_vector];
new_cam_trajectory = [cam_trajectory; cam_pose'];
new_world_pc = [world_pc; w_P(:,1:3)];

end