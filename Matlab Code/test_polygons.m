    polygons_3d_points = [];
    radius = 3;
    % Centering the polygon for each 3D point
    for n = 1:size(rand_sparse_pts,1)
        polygon_x_coord = polygon_coordinates(:,1)*radius + rand_sparse_pts(n,1);
        polygon_y_coord = polygon_coordinates(:,1)*radius + rand_sparse_pts(n,2);
        z_coordinate = ones(size(polygon_coordinates,1),1) * rand_sparse_pts(n,3);
        [ px_x, px_y ] = XYZToEquiPx( polygon_x_coord, polygon_y_coord, z_coordinate, azimuth_area_w_rad, elevation_area_w_rad );
        aux = [px_x px_y] / size(equi_risk_smoothed,2);
        aux(:,1) = 1 - aux(:,1);
        inlierPoints2 = aux * size(I2_uint8,2);
        
        % Saving the polygons of each 3D point of the point cloud
        polygon_j = [];
        for j = 1:size(inlierPoints2,1)
            polygon_j = [polygon_j,inlierPoints2(j,1),inlierPoints2(j,2)];
        end
        
        polygons_3d_points = [polygons_3d_points;polygon_j];
    end
    