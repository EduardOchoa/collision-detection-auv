% This script is used to estimate the extrinsics from point matches

clear all;
close all;
clc;

addpath C:\Users\serdp\svn\onlinemosaics\hashim_work -end;

% imgLeftFilename = 'C:\Users\ngracias\deaimgdata\cirs20140507_short\sync_stereo_examples\imgleft_offset2050_00007.jpg';
% imgRightFilename = 'C:\Users\ngracias\deaimgdata\cirs20140507_short\sync_stereo_examples\imgright_offset2050_00007.jpg';

% imgLeftFilename = 'C:\Users\ngracias\deaimgdata\cirs20140519\widebaseline_flashsync_motionsync_distances_rolling\imageLeft07504.jpg';
% imgRightFilename = 'C:\Users\ngracias\deaimgdata\cirs20140519\widebaseline_flashsync_motionsync_distances_rolling\imageRight07532.jpg';

% % This is a set of radially corrected images. It works ok with Nuno's sifts
% imgLeftFilename = 'C:\Users\ngracias\deaimgdata\cirs20140528\widebaseline_flash_motionsync_3D_rolling\corrLeftResync07450.jpg';
% imgRightFilename = 'C:\Users\ngracias\deaimgdata\cirs20140528\widebaseline_flash_motionsync_3D_rolling\corrRightResync07450.jpg';

% % This is a set of original, not radially corrected images of the pool where the external calibration exists, done with Bouguets
% imgLeftFilename = 'C:\Users\ngracias\deaimgdata\cirs20140528\widebaseline_flash_motionsync_3D_rolling\imageLeftResync07450.jpg';
% imgRightFilename = 'C:\Users\ngracias\deaimgdata\cirs20140528\widebaseline_flash_motionsync_3D_rolling\imageRightResync07450.jpg';
% measuredTlength = 350;
% SIFTOPTIONS.resizeFactor = 0.5; SIFTOPTIONS.imgFilterNumber = 3; SIFTOPTIONS.ignoreSIFTfile = 0; SIFTOPTIONS.maxSifts = 2000;

% This is a set of original, not radially corrected images of the pool where the right camera was deliberatly pitched down to simulate a sudden change in the cameras arrangement
imgLeftFilename = 'C:\Users\ngracias\deaimgdata\cirs20140528\widebaseline_flash_motionsync_3D_rolling\imageLeftResync22032.jpg';
imgRightFilename = 'C:\Users\ngracias\deaimgdata\cirs20140528\widebaseline_flash_motionsync_3D_rolling\imageRightResync22032.jpg';
measuredTlength = 350;
SIFTOPTIONS.resizeFactor = 0.5; SIFTOPTIONS.imgFilterNumber = 3; SIFTOPTIONS.ignoreSIFTfile = 0; SIFTOPTIONS.maxSifts = 2000;


% % This is a set of original, not radially corrected images of the sea
% imgLeftFilename = 'C:\Users\ngracias\deaimgdata\sea20140504\imageLeftResync02486.jpg';
% imgRightFilename = 'C:\Users\ngracias\deaimgdata\sea20140504\imageRightResync02486.jpg';
% measuredTlength = 95.5;
% SIFTOPTIONS.resizeFactor = 0.5; SIFTOPTIONS.imgFilterNumber = 0; SIFTOPTIONS.ignoreSIFTfile = 0; SIFTOPTIONS.maxSifts = 4000;


I1_uint8 = imread(imgLeftFilename);
I2_uint8 = imread(imgRightFilename);

useMatlabExampleCode = 0;
if useMatlabExampleCode
    displayResults = 1;
    [inlierPoints1,inlierPoints2,fMatrix, matchedPoints1, matchedPoints2] = matchimagesfundamental_matlabexample(I1_uint8,I2_uint8,displayResults);
    
    figure; showMatchedFeatures(I1_uint8, I2_uint8, matchedPoints1, matchedPoints2);
    legend('Putatively matched points in I1', 'Putatively matched points in I2');
    
    figure; showMatchedFeatures(I1_uint8, I2_uint8, inlierPoints1, inlierPoints2);
    legend('Inlier points in I1', 'Inlier points in I2');
    
    [fMatrix, epipolarInliers, status] = estimateFundamentalMatrix(matchedPoints1, matchedPoints2);
    
    inlierPoints1 = matchedPoints1(epipolarInliers, :);
    inlierPoints2 = matchedPoints2(epipolarInliers, :);
    
    figure; showMatchedFeatures(I1_uint8, I2_uint8, inlierPoints1, inlierPoints2);
    legend('Inlier points in I1', 'Inlier points in I2');
end;

%% match using Nuno's functions

% SIFTOPTIONS.resizeFactor = 0.5;
% SIFTOPTIONS.imgFilterNumber = 3;
% SIFTOPTIONS.ignoreSIFTfile = 0;
% SIFTOPTIONS.maxSifts = 2000;


% SIFTOPTIONS.resizeFactor = sd_getparameter(SIFTOPTIONS,'resizeFactor',[]);
% SIFTOPTIONS.imgFilterNumber = sd_getparameter(SIFTOPTIONS,'imgFilterNumber',0);
% SIFTOPTIONS.ignoreSIFTfile = sd_getparameter(SIFTOPTIONS,'ignoreSIFTfile',0);
% SIFTOPTIONS.maxSifts = sd_getparameter(SIFTOPTIONS,'maxSifts',2000);

% MATCHOPTIONS.method = 'lsplanar';
MATCHOPTIONS.method = 'lssemrig';
MATCHOPTIONS.samplesize = 4;
MATCHOPTIONS.numiter1 = 500;
MATCHOPTIONS.numiter2 = 100;
MATCHOPTIONS.medtrhes = 0;
MATCHOPTIONS.disttrhes = 200;
MATCHOPTIONS.useJointMatch = 0;
MATCHOPTIONS.minIniSiftMatches = 30;
MATCHOPTIONS.maxMatchesPerSift = 2;


[im, des, lastloc, lastdesU8] = siftnuno(imgLeftFilename,SIFTOPTIONS);
[im, des, currloc, currdesU8] = siftnuno(imgRightFilename,SIFTOPTIONS);

% Attempt matching
[Mlastcurr,FINALCL1,FINALCL2,CL1uv,CL2uv] = imgmatchsift(lastloc,lastdesU8,currloc,currdesU8,MATCHOPTIONS);

figure;
subplot(1,2,1); showMatchedFeatures(I1_uint8, I2_uint8, FINALCL1,FINALCL2);
legend('Inlier points in I1', 'Inlier points in I2');
title('Accepted with planar homography');


[fMatrix, epipolarInliers, status] = estimateFundamentalMatrix(FINALCL1,FINALCL2,'NumTrials',1000);

inlierPoints1 = FINALCL1(epipolarInliers, :);
inlierPoints2 = FINALCL2(epipolarInliers, :);

subplot(1,2,2); showMatchedFeatures(I1_uint8, I2_uint8, inlierPoints1, inlierPoints2);
legend('Inlier points in I1', 'Inlier points in I2');
title('Accepted with F matrix from Nuno''s sift');


% Test of the optimization from triangulation done by Hashim
baseDir = fileparts(fileparts(fileparts(which(mfilename))));
if isempty(baseDir)
    baseDir = 'C:\Users\ngracias';
end;
load ([baseDir filesep 'deawork\code\gopro_calibration\cirs20140528_resync_calib_seq1\stereo rectification\Calib_Results_stereo.mat'], ...
    'om', 'T', 'fc_left', 'cc_left','kc_left','alpha_c_left','fc_right','cc_right','kc_right','alpha_c_right','ny','nx');

warning('Correcting for the T that was wrongly set to 100mm instead of 56mm');
om_ori = om;
T_ori = T * 56/100;
clear T;

STEREOCALIBstructure.fc_left = fc_left;
STEREOCALIBstructure.fc_right = fc_right;
STEREOCALIBstructure.cc_left = cc_left;
STEREOCALIBstructure.cc_right = cc_right;
STEREOCALIBstructure.kc_left = kc_left;
STEREOCALIBstructure.kc_right = kc_right;
STEREOCALIBstructure.alpha_c_right = alpha_c_right;
STEREOCALIBstructure.alpha_c_left = alpha_c_left;
STEREOCALIBstructure.sizey = ny;
STEREOCALIBstructure.sizex = nx;
% STEREOCALIBstructure.om = om;
% STEREOCALIBstructure.T = T;


% %   Load matched points
% load C:\users_local\serdp\test\stereo_results\20050509_kb\high1highres_lowrate\simple_homography_based\simplematchdata.mat ...
%     SIMPLESTEREOMATCHESstruct
% %   Matched points
% xl = SIMPLESTEREOMATCHESstruct(1).LEFT_FINALCL';
% xr = SIMPLESTEREOMATCHESstruct(1).RIGHT_FINALCL';
% %   Left and right images
% imgl = SIMPLESTEREOMATCHESstruct(1).leftImageName;
% imgr = SIMPLESTEREOMATCHESstruct(1).rightImageName;

[CL1,CL2,figHndl] = manmatch(I1_uint8,I2_uint8,20);
COORDLISTleft = fliplr(CL1);
COORDLISTright = fliplr(CL2);

%   Matched points
xl = [inlierPoints1; COORDLISTleft]';
xr = [inlierPoints2; COORDLISTright]';
%   Left and right images
imgl = imgLeftFilename;
imgr = imgRightFilename;


matchedpt = [xl; xr];
% extrinsic_initial = [om_ori;T_ori];
% f_handle = @(x)triangulationgapoptimize(x, matchedpt, STEREOCALIBstructure);
%
% minOptions = optimset('Display','iter','MaxFunEvals',3000,'Jacobian','Off','MaxIter',2000);
%
% extrinsic_final = lsqnonlin(f_handle, extrinsic_initial,[],[],minOptions);
%
% d1 = triangulationgapoptimize(extrinsic_initial, matchedpt, STEREOCALIBstructure);
% d2 = triangulationgapoptimize(extrinsic_final, matchedpt, STEREOCALIBstructure);
% figure;
% plot(sort(d1),'r')
% hold on; plot(sort(d2),'b')


% Second  test with the cost function using spherical coordinates for T,
% therefore using the exact number of parameters needed
% om_ini = [10*pi/180 10*pi/180 10*pi/180]'
om_ini = [20*pi/180 20*pi/180 1*pi/180]'
T_ini = [measuredTlength 0 0]'

[T_theta,T_phi] = cart2sph(T_ini(1),T_ini(2),T_ini(3));
extrinsic_initial_spherical = [om_ini; T_theta; T_phi];
f_handle = @(x)triangulationgapoptimizesphericalweighted(x, matchedpt, STEREOCALIBstructure);

minOptions = optimset('Display','iter','MaxFunEvals',3000,'Jacobian','Off','MaxIter',2000);
extrinsic_final_spherical = lsqnonlin(f_handle, extrinsic_initial_spherical,[],[],minOptions);
%%
d1 = triangulationgapoptimizesphericalweighted(extrinsic_initial_spherical, matchedpt, STEREOCALIBstructure);
d2 = triangulationgapoptimizesphericalweighted(extrinsic_final_spherical, matchedpt, STEREOCALIBstructure);
figure;
plot(sort(d1),'r');
hold on; plot(sort(d2),'b');
om_final = extrinsic_final_spherical(1:3)
[T_final_x,T_final_y,T_final_z] = sph2cart(extrinsic_final_spherical(4), extrinsic_final_spherical(5),norm(T_ini));

T_final = [T_final_x; T_final_y; T_final_z]
%% Display the Original and Rectified output
STEREOCALIBstructure_original = STEREOCALIBstructure;
STEREOCALIBstructure_original.om = om_ori;
STEREOCALIBstructure_original.T = T_ori;
[imglrect, imgrrect] = stereoimgrectify(imgl, imgr, STEREOCALIBstructure_original);
figure;
subplot(3,1,1); imgconcat(imglrect, imgrrect,gcf); title('Original Extrinsic Calibration');

STEREOCALIBstructure_initial = STEREOCALIBstructure;
STEREOCALIBstructure_initial.om = om_ini;
STEREOCALIBstructure_initial.T = T_ini;
[imglrect, imgrrect] = stereoimgrectify(imgl, imgr, STEREOCALIBstructure_initial);
subplot(3,1,2); imgconcat(imglrect, imgrrect,gcf); title('Starting Point for Extrinsic Estimation');

STEREOCALIBstructure_final = STEREOCALIBstructure;
STEREOCALIBstructure_final.om = om_final;
STEREOCALIBstructure_final.T = T_final;
[imglrect, imgrrect] = stereoimgrectify(imgl, imgr, STEREOCALIBstructure_final);
subplot(3,1,3); imgconcat(imglrect, imgrrect,gcf); title('Final Extrinsic Estimation');

printResults = 0;
if printResults
    [~,imagename,~] = fileparts(imgLeftFilename);
    print([baseDir filesep 'deawork\results\rectifications_' imagename '.eps'],'-depsc2');
    
    figure; showMatchedFeatures(I1_uint8, I2_uint8, inlierPoints1, inlierPoints2);
    legend('Inlier points in left image', 'Inlier points in right image');
    title('Inliers Used for Extrinsics Estimation');
    print([baseDir filesep 'deawork\results\inliers_' imagename '.eps'],'-depsc2');
    
    figure; subplot(1,2,1); imshow(I1_uint8); subplot(1,2,2); imshow(I2_uint8);
    print([baseDir filesep 'deawork\results\stereopair_' imagename '.eps'],'-depsc2');
    
    clear imglrect imgrrect
    save([baseDir filesep 'deawork\results\rectifications_data_' imagename '.mat']);
end;

T_final
T_ini
theta = acos((T_final' * T_ini)/(norm(T_final)*norm(T_ini)))
theta_deg = theta*180/pi


% save('dea_simplestereomatching_goodresult_pitcheddowncamera.mat');

return;


