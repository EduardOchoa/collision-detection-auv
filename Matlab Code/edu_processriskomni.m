function [minD_norm,t_norm,ptCloudRiskNear,inlierPoints2,I2modif,equi_risk_smoothed,risk_struct] = edu_processriskomni(C1,C2,b2_T_b1,delta_time,wPoints3D,panoram_img,riskColormap,counter)
% [minD_norm,t_norm] = edu_processrisk(wt1,wt2,dt,wPoints3D,riskColormap,ptCloud,inlierPoints2,I2_uint8)
%
% 
% dt is the duration of the camera motion step in seconds
% R is the rotation of camera 2 with repect to 1 as definer by matlabs function relativeCameraPose
    
    sim_flag = 0;
    saveImages = true;
    tests_flag = false;
    
    % just adapt the inputs to my naming of the variables
    wt1 = C1;
    wt2 = C2;
    v = C2 - C1;
    dt = delta_time;
%     wPoints3D = P;
    I2_uint8 = panoram_img;
%     riskColormap = jet(64);
    
    norm_factor = (norm(v)/delta_time);
    
    
    % Compute colision risk indicators
    
    % dt = 2; % Assumed time interval between the images in seconds
    [minD_norm,t_norm] = edu_computemindt(wt1,wt2,dt,wPoints3D);
    
    % % % plot colision risk indicators
    % % figure; plot(minD_norm,t_norm,'.'); axis equal;
    % % xlabel('min dist normalized (sec)');
    % % ylabel('time to min dist normalized (sec)');
    % % xlim([0 30]);
    % % ylim([-20 50]);
    
    % Compute risk
    k = 1.2;
    max_step_saturated = 5;
    max_value = 1;
    tnOffset = 0.5;
    [ normRiskVec ] = compute_risk( minD_norm, t_norm, k, max_step_saturated, max_value, tnOffset);
    risk_struct.values = normRiskVec;
    
    % Compute risk colors per point
    riskColorVec = uint8(zeros(length(normRiskVec),3));
    for idx = 1:length(normRiskVec)
        colormapIdx = round(normRiskVec(idx) * size(riskColormap,1));
        if colormapIdx == 0
            colormapIdx = 1;
        end;
        if colormapIdx > size(riskColormap,1)
            colormapIdx = size(riskColormap,1);
        end;
        riskColorVec(idx,:) = uint8(riskColormap(colormapIdx,:)*255);
    end;
    
    risk_struct.colors = riskColorVec;
    figure('units','normalized','outerposition',[0 0 1 1]);
    
    % Visualize risk indicators with risk colors
    subplot(2,2,1); hold on;
    % plot(minD_norm,t_norm,'.');
    for idx = 1:length(normRiskVec)
        plot(minD_norm(idx),t_norm(idx),'.','MarkerEdgeColor',double(riskColorVec(idx,:))/255);
    end;
    axis equal;
    xlabel('min dist normalized (sec)');
    ylabel('time to min dist normalized (sec)');
    xlim([0 30]);
    ylim([-10 50]);
    colormap(riskColormap);
    colorbar;
    title('Collision Indicators (with risk colors)')
    
    % Visualize the camera and 3D
    axisHndl = subplot(2,2,2);
    distLimitForDisplay = 100;
    
    ptCloudRisk = pointCloud(wPoints3D, 'Color', riskColorVec); % Create the point cloud with risk colors
    [indices,dists] = findNeighborsInRadius(ptCloudRisk,[0 0 0],distLimitForDisplay);
    % ptCloudRiskNear = ptCloudRisk;
    ptCloudRiskNear = pointCloud(ptCloudRisk.Location(indices,:));
    ptCloudRiskNear.Color = ptCloudRisk.Color(indices,:);
    show3dpointscamera(ptCloudRiskNear,v,b2_T_b1(1:3,1:3),axisHndl); % Note : the translation and rotation are not verified to be the correct ones (Nuno)
    
    % xlim([-30 30]);
    % ylim([-10 10]);
    % zlim([0 distLimitForDisplay]);
    title('3D (with risk colors)');
    
    
    
    %% Code from Klemen for the equirect intepolation
    % Generate equidistant map with smoothed risks
    equi_sample_deg = 0.5;
    n_neighbors_used = 20;
    max_neighbor_smoothing = 0.2;
    
    rand_sparse_pts = wPoints3D./repmat(sqrt(sum(wPoints3D.^2,2)),1,3);    % Make it on unit sphere
    risk_rand_sparse_pts = normRiskVec;
    
    if sim_flag
        rand_sparse_pts(:,3) = -rand_sparse_pts(:,3);   % ONLY FOR SIMULATION
        rand_sparse_pts(:,2) = -rand_sparse_pts(:,2);   % ONLY FOR SIMULATION
    end
    
    % Smoothed Image
    [ equi_risk_smoothed_flipped ] = generate_equi_image_interpolate( equi_sample_deg, rand_sparse_pts, risk_rand_sparse_pts, n_neighbors_used, max_neighbor_smoothing );
    equi_risk_smoothed = flipdim(equi_risk_smoothed_flipped,2); % Added by Nuno
    
    % % % % Show image with anchor points
    % % % figure;
    % % % imshow(equi_risk_smoothed)
    % % % hold on;
    
    % Convert anchor points to equi image
    azimuth_area_w_rad = equi_sample_deg*2*pi/360;
    elevation_area_w_rad = (equi_sample_deg*2)*pi/360;
    [ px_x, px_y ] = XYZToEquiPx( rand_sparse_pts(:,1), rand_sparse_pts(:,2), rand_sparse_pts(:,3), azimuth_area_w_rad, elevation_area_w_rad );
    
    % This is where the equivalent of an omnidirectional camera projection
    % would come. I've tweaked to make it work by flipping left and right
    aux = [px_x px_y] / size(equi_risk_smoothed,2);
    aux(:,1) = 1 - aux(:,1);
    inlierPoints2 = aux * size(I2_uint8,2);
    
    % Plotting each point using a polygon representation on the
    % equirectangular image
    polygon_coordinates = CreatePolygon(32,1); % polygon of 8 sides and radius 1
    polygons_3d_points = [];
    
    % Show 3D point projections with risk colors
    h = subplot(2,2,3);
    for j = 1:size(inlierPoints2,1)
        polygonxy = DrawEquiCircle(inlierPoints2(j,1),inlierPoints2(j,2),size(I2_uint8,1),0.5,polygon_coordinates);
        polygons_3d_points = [polygons_3d_points;polygonxy];
    end
    I3 = insertShape(I2_uint8,'polygon',polygons_3d_points,'LineWidth',3,'Color',riskColorVec);
    imshow(I3);
    
%     imshow(I2_uint8); 
%     hold on;    
%     for idx = 1:length(normRiskVec)
%         plot(inlierPoints2(idx,1),inlierPoints2(idx,2),'+','MarkerEdgeColor',double(riskColorVec(idx,:))/255);
%     end;
%     
%     if saveImages
%         if exist('Plotted3DPoints_EquirectangularImages','dir')
%             warning('The directory has been already created before.');
%         else
%             mkdir('Plotted3DPoints_EquirectangularImages');
%         end
%         
%         current_path = pwd;
%         equi_path = '/home/eduochoa/Matlab/Plotted3DPoints_EquirectangularImages';
%         
%         cd(equi_path);
%         equi_name = sprintf('Risk_map%d.png',counter);
%         saveas(gcf,equi_name);
%         cd(current_path);
%     end
    
    % Create the color coded heatmap image
    Irisk_RGB_small = ind2rgb(gray2ind(equi_risk_smoothed),riskColormap);
    Irisk_RGB = imresize(Irisk_RGB_small,[size(I2_uint8,1) size(I2_uint8,2)]);
    
    I2_hsv = rgb2hsv(I2_uint8);
    Irisk_hsv = rgb2hsv(Irisk_RGB);
    
    I2modif_hsv(:,:,1) = Irisk_hsv(:,:,1);
    I2modif_hsv(:,:,2) = Irisk_hsv(:,:,2) * 0.75;
    I2modif_hsv(:,:,3) = I2_hsv(:,:,3);
    
    I2modif = hsv2rgb(I2modif_hsv);
    
    subplot(2,2,4);
    imshow(I2modif);
    
    drawnow;
    
    if tests_flag
        figure('Name','Z figure');
        % Visualize risk indicators with risk colors
        hold on;
        for idx = 1:length(normRiskVec)
            plot(minD_norm(idx),t_norm(idx),'.','MarkerEdgeColor',double(riskColorVec(idx,:))/255);
        end;
        axis equal;
        xlabel('min dist normalized (sec)');
        ylabel('time to min dist normalized (sec)');
        xlim([0 30]);
        ylim([-10 50]);
        colormap(riskColormap);
        colorbar;
        title('Collision Indicators (with risk colors)')
        
        figure('Name','Equiractangular map');
        imshow(I2modif);
        title('Equirectangular risk map')
    end
    
    drawnow;
    
    if saveImages
        if exist('Plotted3DPoints_EquirectangularImages2','dir')
            warning('The directory has been already created before.');
        else
            mkdir('Plotted3DPoints_EquirectangularImages2');
        end
        
        if exist('Smoothed_risk_maps','dir')
            warning('The directory has been already created before.');
        else
            mkdir('Smoothed_risk_maps');
        end
        
        current_path = pwd;
        equi_path = '/home/eduochoa/Matlab/Plotted3DPoints_EquirectangularImages2';
%         smoothed_path = '/home/eduochoa/Matlab/Smoothed_risk_maps';
% 
        cd(equi_path);
        equi_name = string(sprintf('pano_frame%05d.jpg',counter));
        imwrite(I3,equi_name,'Quality',100);
% 
%         cd(smoothed_path);
%         smoothed_name = string(sprintf('pano_frame%05d.jpg',counter));
%         imwrite(I2modif,smoothed_name,'Quality',100);
        cd(current_path);
    end
