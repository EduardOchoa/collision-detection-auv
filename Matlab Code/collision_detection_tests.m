cam_filepath = '/home/eduochoa/PointClouds/Boreas/CSV_CamPoses';
pts_filepath =  '/home/eduochoa/PointClouds/Boreas/CSV_PointClouds';
panoramic_imgs_path = '/home/eduochoa/panos';
panoramic_img_dir = dir(strcat(panoramic_imgs_path,'/*.jpg'));
panoramic_img_filenames = {panoramic_img_dir.name};


figure(1);

for pcd_i = 3:18
   cam_data = readtable(sprintf('%s%s%s_%d.csv', cam_filepath,filesep,'CAM',pcd_i),'Delimiter',' ');
   pts_data = readtable(sprintf('%s%s%s_%d.csv', pts_filepath,filesep,'PointCloud',pcd_i));
   
   % Getting the current image sequence name
   current_frame_name = cam_data{2,19};
   [idx,~] = strnearest(current_frame_name,panoramic_img_filenames);
   
   % Get timespamps
   timestamp_1 = cam_data{1,18};
   timestamp_2 = cam_data{2,18};
   
   % Compute relative pose
   w_T_b1 = reshape(cam_data{1,2:17},4,4)';
   w_T_b2 = reshape(cam_data{2,2:17},4,4)';
   b2_T_w = eye(4,4);
   b2_T_w(1:3,1:3) = w_T_b2(1:3,1:3)';
   b2_T_w(1:3,4) = -w_T_b2(1:3,1:3)' * w_T_b2(1:3,4);
   b2_T_b1 = b2_T_w*w_T_b1;
   
   % Get data
   C1 = b2_T_b1(1:3,4);
   C2 = zeros(3,1);
   v = C2 - C1;
   P = pts_data{:,1:3};
   delta_time = timestamp_2-timestamp_1;
   norm_factor = (norm(v)/delta_time);
   
   v_n = v/norm_factor;
   C1_n = C1/norm_factor;
   P_n = P/norm_factor;
   x1 = repmat(C1_n',size(P_n,1),1);
   x = x1 - P_n;
   
   % Compute risk
   % Script to compute risk
   v_n_mat = repmat(v_n',size(P_n,1),1);
   t_min_num = dot(x,v_n_mat,2);
   t_min = -t_min_num/(norm(v_n)^2);
   t_min = t_min-1;
   
   d_min_num = cross(v_n_mat,x,2);
   d_min = vecnorm(d_min_num,2,2)/norm(v_n);

   % Parameters
   k = 0.2;
   max_step_saturated = 1;
   max_value = 10;
   sigma = k.*t_min;
   
   F = [];

   for i = 1:size(d_min,1)
      if t_min(i) <= 0
          F(i) = 0;
          continue;
      else
          F(i) = k/sigma(i) * (exp(-d_min(i)^2/(2*sigma(i)^2)));
      end
   end
   
   if max_value~=0
       F = (max_step_saturated*max_value).*F;
       F(F>max_value) = max_value;
   else
       F = max_step_saturated.*F;
   end

   
   % Figure;
   figure(1);
   % Points
   scatter3(P(:,1),P(:,2),P(:,3),10,'b','filled');
   hold on;
   % Cameras
   scatter3(C1(1),C1(2),C1(3),20,'k','filled');
   scatter3(C2(1),C2(2),C2(3),20,'r','filled');
   % Vector v
   plot3([C1(1), C1(1)+v(1)], [C1(2), C1(2)+v(2)], [C1(3), C1(3)+v(3)], 'b');
   axis equal;
   hold off;
   pause(1);
end