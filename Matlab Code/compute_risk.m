function [ f ] = compute_risk( dn, tn, k, step_saturation, max_value, tnOffset )
%COMPUTE_WEIGHT Summary of this function goes here
% max_step_saturated - until which the values will be saturated
% max_value - maximum value of the weights
% k - Sigma declining parameter

if nargin < 6
    tnOffset = 0;
end;

% Ensure shape
Dn = reshape(dn,[],1);
Tn = reshape(tn,[],1);

Tn = Tn + tnOffset;                             % Added by Nuno
step_saturation = step_saturation + tnOffset;   % Added by Nuno

% Tn = abs(Tn);                                   % Added by Nuno
Tn(Tn<0.01) = 100;                              % Added by Nuno 27/5/2019

% Compute sigma
sigma = k.*Tn(:);

F = k./sigma .* (exp(-Dn(:).^2./(2*sigma.^2)));

if max_value~=0
    F = (step_saturation*max_value).*reshape(F,size(Dn));
    F(F>max_value) = max_value;
else
    F = step_saturation.*reshape(F,size(Dn));
end

f = reshape(F,size(dn));


end

