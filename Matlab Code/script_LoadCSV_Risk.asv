clear all;
close all;
clc;

cam_filepath = '/media/klemen/0FA236AB6A4A40DB/work/workspace/EduardoVibot/Boreas_gaussian_risk_function_v2/CSV_CamPoses';
pts_filepath =  '/media/klemen/0FA236AB6A4A40DB/work/workspace/EduardoVibot/Boreas_gaussian_risk_function_v2/CSV_PointClouds';

cam_filepath = 'Boreas_risk_computed_with_gaussian_function/CSV_CamPoses';
pts_filepath =  'Boreas_risk_computed_with_gaussian_function/CSV_PointClouds';


I2_uint8 = imread('pano_smooth_frame23730_gradblend.jpg');

figure(1);

for pcd_i = 3:19
   cam_data = readtable(sprintf('%s%s%s_%d.csv', cam_filepath,filesep,'CAM',pcd_i),'Delimiter',' ');
   pts_data = readtable(sprintf('%s%s%s_%d.csv', pts_filepath,filesep,'PointCloud',pcd_i));
   
   % Get timespamps
   timestamp_1 = cam_data{1,18};
   timestamp_2 = cam_data{2,18};
   
   % Compute relative pose
   w_T_b1 = reshape(cam_data{1,2:17},4,4)';
   w_T_b2 = reshape(cam_data{2,2:17},4,4)';
   b2_T_w = eye(4,4);
   b2_T_w(1:3,1:3) = w_T_b2(1:3,1:3)';
   b2_T_w(1:3,4) = -w_T_b2(1:3,1:3)' * w_T_b2(1:3,4);
   b2_T_b1 = b2_T_w*w_T_b1;
   
   % Get data
   C1 = b2_T_b1(1:3,4);
   C2 = zeros(3,1);
   v = C2 - C1;
   P = pts_data{:,1:3};
   delta_time = timestamp_2-timestamp_1;
   
   % Compute risk
   
   wt1 = C1;
   wt2 = C2;
   dt = delta_time;
   wPoints3D = P;
   riskColormap = jet(64);
   
   % Compute colision risk indicators
   
% dt = 2; % Assumed time interval between the images in seconds
[minD_norm,t_norm] = edu_computemindt(wt1,wt2,dt,wPoints3D);

% plot colision risk indicators
figure; plot(minD_norm,t_norm,'.'); axis equal;
xlabel('min dist normalized (sec)');
ylabel('time to min dist normalized (sec)');
xlim([0 30]);
ylim([-20 50]);

% Compute risk
k = 0.4;
max_step_saturated = 2;
max_value = 1;
tnOffset = 0.5;
[ normRiskVec ] = compute_risk( minD_norm, t_norm, k, max_step_saturated, max_value, tnOffset);

% Compute risk colors per point
riskColorVec = uint8(zeros(length(normRiskVec),3));
for idx = 1:length(normRiskVec)
    colormapIdx = round(normRiskVec(idx) * size(riskColormap,1));
    if colormapIdx == 0
        colormapIdx = 1;
    end;
    if colormapIdx > size(riskColormap,1)
        colormapIdx = size(riskColormap,1);
    end;
    riskColorVec(idx,:) = uint8(riskColormap(colormapIdx,:)*255);
end;

% Visualize risk indicators with risk colors
figure; hold on;
% plot(minD_norm,t_norm,'.'); 
for idx = 1:length(normRiskVec)
    plot(minD_norm(idx),t_norm(idx),'.','MarkerEdgeColor',double(riskColorVec(idx,:))/255);
end;
axis equal;
xlabel('min dist normalized (sec)');
ylabel('time to min dist normalized (sec)');
xlim([0 30]);
ylim([-10 50]);
colormap(riskColormap);
colorbar;

% Visualize the camera and 3D
ptCloudRisk = pointCloud(wPoints3D, 'Color', riskColorVec); % Create the point cloud with risk colors
show3dpointscamera(ptCloudRisk,v,b2_T_b1(1:3,1:3)); % Note : the translation and rotation are not verified to be the correct ones (Nuno)
title('Collision Risk');

%% Code from Klemen for the equirect intepolation 
% Generate equidistant map with smoothed risks
equi_sample_deg = 0.5;
n_neighbors_used = 50;
max_neighbor_smoothing = 0.1;

rand_sparse_pts = wPoints3D./repmat(sqrt(sum(wPoints3D.^2,2)),1,3);    % Make it on unit sphere
risk_rand_sparse_pts = normRiskVec;

% Smoothed Image
[ equi_risk_smoothed ] = generate_equi_image_interpolate( equi_sample_deg, rand_sparse_pts, risk_rand_sparse_pts, n_neighbors_used, max_neighbor_smoothing );

% Show image with anchor points
figure;
imshow(equi_risk_smoothed)
hold on;

% Convert anchor points to equi image
azimuth_area_w_rad = equi_sample_deg*2*pi/360;
elevation_area_w_rad = (equi_sample_deg*2)*pi/360;
[ px_x, px_y ] = XYZToEquiPx( rand_sparse_pts(:,1), rand_sparse_pts(:,2), rand_sparse_pts(:,3), azimuth_area_w_rad, elevation_area_w_rad );
scatter(px_x, px_y, 30 ,'b');
scatter(px_x, px_y, 30 ,repmat(risk_rand_sparse_pts,1,3),'filled');

% This is where the equivalent of an omnidirectional camera projection is
inlierPoints2 = [px_x px_y]

% Show 3D point projections with risk colors
figure; imshow(I2_uint8); hold on;
for idx = 1:length(normRiskVec)
    plot(inlierPoints2(idx,1),inlierPoints2(idx,2),'+','MarkerEdgeColor',double(riskColorVec(idx,:))/255);
end;



return;
   
   
   % Figure;
   figure(1);
   % Points
   scatter3(P(:,1),P(:,2),P(:,3),10,'b','filled');
   hold on;
   % Cameras
   scatter3(C1(1),C1(2),C1(3),20,'b','filled');
   scatter3(C2(1),C2(2),C2(3),20,'g','filled');
   % Vector v
   plot3([C1(1), C1(1)+v(1)], [C1(2), C1(2)+v(2)], [C1(3), C1(3)+v(3)], 'b');
   axis equal;
   hold off;
   pause(1);
end