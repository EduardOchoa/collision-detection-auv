function show3dpointscamera(ptCloud,t,R,axisHndl)
% show3dpointscamera(ptCloud,t,R)
%
% Draws a simple 3D scene with two cameras (taken from Matlab example)
%

if nargin < 4
    figHndl = figure;
    axisHndl = figHndl.CurrentAxes;
end;

% Visualize the camera locations and orientations
cameraSize = 0.3;

plotCamera('Size', cameraSize, 'Color', 'r', 'Label', '1', 'Opacity', 0);
hold on
grid on
plotCamera('Location', t, 'Orientation', R, 'Size', cameraSize, ...
    'Color', 'b', 'Label', '2', 'Opacity', 0);

% Visualize the point cloud
% pcshow(ptCloud, 'VerticalAxis', 'y', 'VerticalAxisDir', 'down', ...
%     'MarkerSize', 45);
pcshow(ptCloud, 'VerticalAxis', 'z', 'VerticalAxisDir', 'up', ...
    'MarkerSize', 45);


% Rotate and zoom the plot
% camorbit(0, -30);
camorbit(-80, 0);
camzoom(1.2);
% campos([-17.3734 -224.3460 -344.9331]);

% Label the axes
xlabel('x-axis');
ylabel('y-axis');
zlabel('z-axis')
