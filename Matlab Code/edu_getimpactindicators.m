function [currDistCam2,minDistCam2,timeToMinDistCam2] = edu_getimpactindicators(wt1,wt2,wPoints3D)
% [currDistCam2,minDistCam2,timeToMinDistCam2] = edu_getimpactindicators(wt1,wt2,wPoints3D)
%
% Get Impact Indicators :
%
% Computes impact indicators for a pair of cameras with positions in
% wt1,wt2 in the reference frame of the world for a given list of 3D points
% wPoints3D also in the world coodinate frame.
%

currDistCam2 = sqrt((wPoints3D(:,1) - wt2(1))^2 + (wPoints3D(:,2) - wt2(2))^2 + (wPoints3D(:,3) - wt2(3))^2);




