function [minD_norm,t_norm] = edu_computemindt(wt1,wt2,dt,wPoints3D)
% [minD_norm,t_norm] = edu_computemindt(wt1,wt2,dt,wPoints3D)
%
% 
% dt is the duration of the camera motion step in seconds

wt1 = wt1(:)';  % Just to be sure these are row vectors
wt2 = wt2(:)';  % Just to be sure these are row vectors

% using the formulas found in http://mathworld.wolfram.com/Point-LineDistance3-Dimensional.html
for point3dIdx = 1:size(wPoints3D,1)
    wt0 = wPoints3D(point3dIdx,:);
    timeToMinDistCam2Vec(point3dIdx) = -((wt1-wt0)*(wt2-wt1)')/((wt2-wt1)*(wt2-wt1)');
    xprod = cross(wt2-wt1,wt1-wt0);
    minDistCam2Vec(point3dIdx) = sqrt((xprod*xprod')/((wt2-wt1)*(wt2-wt1)'));
end;

t_norm = timeToMinDistCam2Vec' * dt; % This is now the time to colision normalised, expressed in seconds

normalized_dist_step = (sqrt((wt2-wt1)*(wt2-wt1)') + 1) / dt; % This is the normalized distance step in mm/sec. I'm adding 1 because the the original formula assume that the time flows from camera 1 to camera 2, where t=0 corresponds to camera 1 and t = 1 corresponds to camera 2. By adding 1 we offset this time so that camera 2 becomes time-aligned with t = 0.  
minD_norm = minDistCam2Vec' / normalized_dist_step;  % This is the minimal distance, expressed in seconds