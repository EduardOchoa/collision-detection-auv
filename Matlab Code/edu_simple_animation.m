% This script is used to estimate the extrinsics from point matches

clear all;
close all;
clc;

baseDir = fileparts(fileparts(fileparts(which(mfilename))));

% if isempty(baseDir)
%     baseDir = 'C:\Users\ngracias';
% end;

% addpath C:\Users\serdp\svn\onlinemosaics\hashim_work -end;
% addpath C:\users_local\serdp\svn\onlinemosaics\hashim_work -end;
% addpath Z:\Users\nuno\serdp\svn\onlinemosaics\hashim_work -end;
% addpath Z:\Users\nuno\serdp\toolbox_calib -end;

addpath([fileparts(baseDir) filesep 'serdp\toolbox_calib'],'-end');

% imgLeftFilename = 'C:\Users\ngracias\deaimgdata\cirs20140507_short\sync_stereo_examples\imgleft_offset2050_00007.jpg';
% imgRightFilename = 'C:\Users\ngracias\deaimgdata\cirs20140507_short\sync_stereo_examples\imgright_offset2050_00007.jpg';

% imgLeftFilename = 'C:\Users\ngracias\deaimgdata\cirs20140519\widebaseline_flashsync_motionsync_distances_rolling\imageLeft07504.jpg';
% imgRightFilename = 'C:\Users\ngracias\deaimgdata\cirs20140519\widebaseline_flashsync_motionsync_distances_rolling\imageRight07532.jpg';

% % This is a set of radially corrected images. It works ok with Nuno's sifts
% imgLeftFilename = 'C:\Users\ngracias\deaimgdata\cirs20140528\widebaseline_flash_motionsync_3D_rolling\corrLeftResync07450.jpg';
% imgRightFilename = 'C:\Users\ngracias\deaimgdata\cirs20140528\widebaseline_flash_motionsync_3D_rolling\corrRightResync07450.jpg';

% % This is a set of original, not radially corrected images of the pool where the external calibration exists, done with Bouguets
% imgLeftFilename = 'C:\Users\ngracias\deaimgdata\cirs20140528\widebaseline_flash_motionsync_3D_rolling\imageLeftResync07450.jpg';
% imgRightFilename = 'C:\Users\ngracias\deaimgdata\cirs20140528\widebaseline_flash_motionsync_3D_rolling\imageRightResync07450.jpg';
% measuredTlength = 350;
% SIFTOPTIONS.resizeFactor = 0.5; SIFTOPTIONS.imgFilterNumber = 3; SIFTOPTIONS.ignoreSIFTfile = 0; SIFTOPTIONS.maxSifts = 2000;

% This is a set of original, not radially corrected images of the pool where the right camera was deliberatly pitched down to simulate a sudden change in the cameras arrangement
% imgLeftFilename = 'C:\users_local\serdp\test\sequences\boreas_sample\camera4_frame24141.jpg';
% imgRightFilename = 'C:\users_local\serdp\test\sequences\boreas_sample\camera4_frame24156.jpg';
% imgLeftFilename = 'Z:\Users\nuno\serdp\test\sequences\boreas_sample\camera4_frame24141.jpg';
% imgRightFilename = 'Z:\Users\nuno\serdp\test\sequences\boreas_sample\camera4_frame24156.jpg';
% imgLeftFilename = 'C:\users\serdp\test\sequences\boreas_sample\camera4_frame24141.jpg';
% imgRightFilename = 'C:\users\serdp\test\sequences\boreas_sample\camera4_frame24156.jpg';
%
% imgLeftFilename = 'C:\users\serdp\test\sequences\boreas_sample\camera4_frame24156.jpg';
% imgRightFilename = 'C:\users\serdp\test\sequences\boreas_sample\camera4_frame24171.jpg';

% SEQNAMEstruct = sd_findsubfolderfiles('C:\users\serdp\test\sequences\boreas_sample','jpg');
imgDir = [fileparts(baseDir) filesep '\serdp\test\sequences\boreas_sample'];

SEQNAMEstruct = sd_findsubfolderfiles(imgDir,'jpg');

v = VideoWriter(['risk_animation_' datestr(now,30) '.avi']);
v.FrameRate = 2;
open(v);

% for pairIdx = 1:(length(SEQNAMEstruct)-2)
for pairIdx = 1:10    
    
    imgLeftFilename = SEQNAMEstruct(pairIdx).filename;
    imgRightFilename = SEQNAMEstruct(pairIdx+1).filename;
    
    [~,shortFilename1] = fileparts(imgLeftFilename);
    [~,shortFilename2] = fileparts(imgRightFilename);
    
    matFilename = [imgDir filesep 'save_' shortFilename1 '_' shortFilename2 '.mat'];
    
    if exist(matFilename,'file')
        load(matFilename);
    else
        [I1_uint8,I2_uint8,points3D,ptCloud,t,R,inlierPoints1,inlierPoints2] = edu_processimgpair(imgLeftFilename,imgRightFilename);
        
        save(matFilename,'I1_uint8','I2_uint8','points3D','ptCloud','t','R','inlierPoints1','inlierPoints2','-mat');
    end;
    
      

wPoints3D = points3D;
wt1 = [0 0 0];
wt2 = t;

riskColormap = jet(64);



%% New method

% Compute colision risk indicators
dt = 0.1; % Assumed time interval between the images in seconds
[minD_norm,t_norm] = edu_computemindt(wt1,wt2,dt,wPoints3D);

% % plot colision risk indicators
% figure; plot(minD_norm,t_norm,'.'); axis equal;
% xlabel('min dist normalized (sec)');
% ylabel('time to min dist normalized (sec)');
% xlim([0 30]);
% ylim([0 50]);

% Compute risk
k = 0.4;
max_step_saturated = 2;
max_value = 1;
tnOffset = 0.5;
[ normRiskVec ] = compute_risk( minD_norm, t_norm, k, max_step_saturated, max_value, tnOffset);

% Compute risk colors per point
riskColorVec = uint8(zeros(length(normRiskVec),3));
for idx = 1:length(normRiskVec)
    colormapIdx = round(normRiskVec(idx) * size(riskColormap,1));
    if colormapIdx == 0
        colormapIdx = 1;
    end;
    if colormapIdx > size(riskColormap,1)
        colormapIdx = size(riskColormap,1);
    end;
    riskColorVec(idx,:) = uint8(riskColormap(colormapIdx,:)*255);
end;


% % Visualize the camera locations and orientations
% ptCloudRisk1 = ptCloud;
% ptCloudRisk1.Color = riskColorVec;
% show3dpointscamera(ptCloudRisk1,t,R);
% title('Collision Risk 1');

% % Show 3D point projections with risk colors
% figure; imshow(I2_uint8); hold on;
% for idx = 1:length(normRiskVec)
%     plot(inlierPoints2(idx,1),inlierPoints2(idx,2),'+','MarkerEdgeColor',double(riskColorVec(idx,:))/255);
% end;

% Compute collision risk heat image
[I2modif,Irisk_RGB] = edu_riskimage(inlierPoints2,normRiskVec,I2_uint8,riskColormap);
% figure; imshow(Irisk_RGB);
% newfig(I2modif);


figure;
figure('units','normalized','outerposition',[0 0 1 1]);

subplot(2,2,1); hold on;
% plot(minD_norm,t_norm,'.'); 
for idx = 1:length(normRiskVec)
    plot(minD_norm(idx),t_norm(idx),'.','MarkerEdgeColor',double(riskColorVec(idx,:))/255);
end;
axis equal;
xlabel('min dist normalized (sec)');
ylabel('time to min dist normalized (sec)');
xlim([0 30]);
ylim([-5 50]);
colormap(riskColormap);
colorbar;

axisHndl = subplot(2,2,2); 
ptCloudRisk = ptCloud;
ptCloudRisk.Color = riskColorVec;

distLimitForDisplay = 100;

[indices,dists] = findNeighborsInRadius(ptCloudRisk,[0 0 0],distLimitForDisplay);
% ptCloudRiskNear = ptCloudRisk;
ptCloudRiskNear = pointCloud(ptCloudRisk.Location(indices,:));
ptCloudRiskNear.Color = ptCloudRisk.Color(indices,:);
show3dpointscamera(ptCloudRiskNear,t,R,axisHndl);
% xlim([-30 30]);
ylim([-10 10]);
zlim([0 distLimitForDisplay]);




subplot(2,2,3); 
imshow(I2_uint8); hold on;
for idx = 1:length(normRiskVec)
    plot(inlierPoints2(idx,1),inlierPoints2(idx,2),'+','MarkerEdgeColor',double(riskColorVec(idx,:))/255);
end;

subplot(2,2,4);
imshow(Irisk_RGB);
imshow(I2modif);


frame = getframe(gcf);
newheight = floor(size(frame.cdata,1)/16)*16; 
newwidth = floor(size(frame.cdata,2)/16)*16;
frame.cdata = frame.cdata(1:newheight,1:newwidth,:);
writeVideo(v,frame);



close all;

end;

close(v);

