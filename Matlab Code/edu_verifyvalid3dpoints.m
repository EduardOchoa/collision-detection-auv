function [wPoints3Dvalid,validMask] = edu_verifyvalid3dpoints(wPoints3D,panoValidMask)
% [minD_norm,t_norm] = edu_processrisk(wt1,wt2,dt,wPoints3D,riskColormap,ptCloud,inlierPoints2,I2_uint8)
%
% 
% dt is the duration of the camera motion step in seconds
% R is the rotation of camera 2 with repect to 1 as definer by matlabs function relativeCameraPose

    
    rand_sparse_pts = wPoints3D./repmat(sqrt(sum(wPoints3D.^2,2)),1,3);    % Make it on unit sphere

    equi_sample_deg = 360/size(panoValidMask,2);
    azimuth_area_w_rad = equi_sample_deg*2*pi/360;
    elevation_area_w_rad = (equi_sample_deg*2)*pi/360;
    
    [ px_x, px_y ] = XYZToEquiPx( rand_sparse_pts(:,1), rand_sparse_pts(:,2), rand_sparse_pts(:,3), azimuth_area_w_rad, elevation_area_w_rad );
    
    % This is where the equivalent of an omnidirectional camera projection
    % would come. I've tweaked to make it work by flipping left and right
    aux = [px_x px_y] / size(panoValidMask,2);
    aux(:,1) = 1 - aux(:,1);
    inlierPoints2 = aux * size(panoValidMask,2);
    
    validMask = false(size(wPoints3D,1),1);
    for pointIdx = 1:size(wPoints3D,1)
        if round(inlierPoints2(pointIdx,1)) == 0
            inlierPoints2(pointIdx,1) = 1;
        end;
        if round(inlierPoints2(pointIdx,2)) == 0
            inlierPoints2(pointIdx,2) = 1;
        end;
        validMask(pointIdx) = panoValidMask(round(inlierPoints2(pointIdx,2)),round(inlierPoints2(pointIdx,1)));
    end;
    
    wPoints3Dvalid = wPoints3D(validMask,:); 
    
     