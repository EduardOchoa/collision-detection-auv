img= imread('/home/eduochoa/panos/pano_smooth_frame24273_gradblend.jpg');

figure(2)
imshow(img)

theta = [];
azimuth = [];
for i = 1:size(P,1)
    theta(i) = acos(P(i,3)/norm(P(i,:)));
    azimuth(i) = atan2(P(i,2),P(i,1));
end

u = ((theta(:)+pi)./(2*pi)).*size(img,2);
v = (azimuth(:)./pi).*size(img,1);

hold on;
for i = 1:size(u,1)
    plot(-v(i)+size(img,1),u(i)-size(img,1),'r+','MarkerSize',10,'LineWidth',1);
end

