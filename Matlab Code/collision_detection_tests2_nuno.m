clear all;
close all;
clc;
addpath('InterpolationEquiCode');
addpath('InterpolationEquiCode/functions');
addpath('InterpolationEquiCode/functions/SphereMath');

dataset = 2;

if dataset == 1
    cam_filepath = '/home/eduochoa/PointClouds/BoreasLong_PC/CSV_CamPoses';
    pts_filepath =  '/home/eduochoa/PointClouds/BoreasLong_PC/CSV_PointClouds';
    panoramic_imgs_path = '/home/eduochoa/Datasets/BoreasLongFiles/panos_1.5m';
    sim_flag = 0;
    data_size = 482;
elseif dataset == 2
    cam_filepath = '/home/eduochoa/PointClouds/Sim_all_cams_fixed/CSV_CamPoses';
    pts_filepath =  '/home/eduochoa/PointClouds/Sim_all_cams_fixed/CSV_PointClouds';
    panoramic_imgs_path = '/home/eduochoa/Datasets/ladybug_simulated_panos';
    sim_flag = 1;
    data_size = 265;
end

panoramic_img_dir = dir(strcat(panoramic_imgs_path,'/*.jpg'));
% panoramic_img_filenames = {panoramic_img_dir.name};

% vidObj = VideoWriter(['risk_animation_' datestr(now,30) '.avi']);
% vidObj.FrameRate = 2;
% open(vidObj);


for pcd_i = 100:data_size
    cam_data = readtable(sprintf('%s%s%s_%d.csv', cam_filepath,filesep,'CAM',pcd_i),'Delimiter',' ');
    pts_data = readtable(sprintf('%s%s%s_%d.csv', pts_filepath,filesep,'PointCloud',pcd_i));
    
    % Getting the current image sequence name
    current_frame_name = cell2mat(cam_data{2,19});
    idx_frameID_in_str = strfind(current_frame_name,'frame');
    frameID = current_frame_name(idx_frameID_in_str:idx_frameID_in_str+9);
    
    % Checking which is the panoramic img correspondence
    for img_n = 1:size(panoramic_img_dir,1)
        if strfind(panoramic_img_dir(img_n).name,frameID)
            panoram_img = imread(strcat(panoramic_imgs_path,'/',panoramic_img_dir(img_n).name));
        end
    end
    
    % For the simulated environment. Switching channels as the images comes
    % as BGR
    if sim_flag
        pani(:,:,1) = panoram_img(:,:,3);
        panoram_img(:,:,3) = panoram_img(:,:,1);
        panoram_img(:,:,1) = pani(:,:,1);
    end
    
    % Get timespamps
    timestamp_1 = cam_data{1,18};
    timestamp_2 = cam_data{2,18};
    
    % Compute relative pose
    w_T_b1 = reshape(cam_data{1,2:17},4,4)';
    w_T_b2 = reshape(cam_data{2,2:17},4,4)';
    b2_T_w = eye(4,4);
    b2_T_w(1:3,1:3) = w_T_b2(1:3,1:3)';
    b2_T_w(1:3,4) = -w_T_b2(1:3,1:3)' * w_T_b2(1:3,4);
    b2_T_b1 = b2_T_w*w_T_b1;
    
    % Get data
    C1 = b2_T_b1(1:3,4);
    C2 = zeros(3,1);
    v = C2 - C1;
    P = pts_data{:,1:3};
    delta_time = timestamp_2-timestamp_1;
    norm_factor = (norm(v)/delta_time);
    
    %% Nuno's block here
    
    
    % just adapt the inputs to my naming of the variables
    wt1 = C1;
    wt2 = C2;
    dt = delta_time;
    wPoints3D = P;
    I2_uint8 = panoram_img;
    riskColormap = jet(64);
    
    
    % Compute colision risk indicators
    
    % dt = 2; % Assumed time interval between the images in seconds
    [minD_norm,t_norm] = edu_computemindt(wt1,wt2,dt,wPoints3D);
    
    % % % plot colision risk indicators
    % % figure; plot(minD_norm,t_norm,'.'); axis equal;
    % % xlabel('min dist normalized (sec)');
    % % ylabel('time to min dist normalized (sec)');
    % % xlim([0 30]);
    % % ylim([-20 50]);
    
    % Compute risk
    k = 0.4;
    max_step_saturated = 2;
    max_value = 1;
    tnOffset = 0.5;
    [ normRiskVec ] = compute_risk( minD_norm, t_norm, k, max_step_saturated, max_value, tnOffset);
    
    % Compute risk colors per point
    riskColorVec = uint8(zeros(length(normRiskVec),3));
    for idx = 1:length(normRiskVec)
        colormapIdx = round(normRiskVec(idx) * size(riskColormap,1));
        if colormapIdx == 0
            colormapIdx = 1;
        end;
        if colormapIdx > size(riskColormap,1)
            colormapIdx = size(riskColormap,1);
        end;
        riskColorVec(idx,:) = uint8(riskColormap(colormapIdx,:)*255);
    end;
    
    figure('units','normalized','outerposition',[0 0 1 1]);
    
    % Visualize risk indicators with risk colors
    subplot(2,2,1); hold on;
    % plot(minD_norm,t_norm,'.');
    for idx = 1:length(normRiskVec)
        plot(minD_norm(idx),t_norm(idx),'.','MarkerEdgeColor',double(riskColorVec(idx,:))/255);
    end;
    axis equal;
    xlabel('min dist normalized (sec)');
    ylabel('time to min dist normalized (sec)');
    xlim([0 30]);
    ylim([-10 50]);
    colormap(riskColormap);
    colorbar;
    title('Collision Indicators (with risk colors)')
    
    
    
    % Visualize the camera and 3D
    axisHndl = subplot(2,2,2);
    distLimitForDisplay = 100;
    
    ptCloudRisk = pointCloud(wPoints3D, 'Color', riskColorVec); % Create the point cloud with risk colors
    [indices,dists] = findNeighborsInRadius(ptCloudRisk,[0 0 0],distLimitForDisplay);
    % ptCloudRiskNear = ptCloudRisk;
    ptCloudRiskNear = pointCloud(ptCloudRisk.Location(indices,:));
    ptCloudRiskNear.Color = ptCloudRisk.Color(indices,:);
    show3dpointscamera(ptCloudRiskNear,v,b2_T_b1(1:3,1:3),axisHndl); % Note : the translation and rotation are not verified to be the correct ones (Nuno)
    
    % xlim([-30 30]);
    % ylim([-10 10]);
    % zlim([0 distLimitForDisplay]);
    title('3D (with risk colors)');
    
    
    
    %% Code from Klemen for the equirect intepolation
    % Generate equidistant map with smoothed risks
    equi_sample_deg = 0.5;
    n_neighbors_used = 20;
    max_neighbor_smoothing = 0.2;
    
    rand_sparse_pts = wPoints3D./repmat(sqrt(sum(wPoints3D.^2,2)),1,3);    % Make it on unit sphere
    
    if sim_flag
        rand_sparse_pts(:,3) = -rand_sparse_pts(:,3);   % ONLY FOR SIMULATION
        rand_sparse_pts(:,2) = -rand_sparse_pts(:,2);   % ONLY FOR SIMULATION
    end
    
    risk_rand_sparse_pts = normRiskVec;
    
    % Smoothed Image
    [ equi_risk_smoothed_flipped ] = generate_equi_image_interpolate( equi_sample_deg, rand_sparse_pts, risk_rand_sparse_pts, n_neighbors_used, max_neighbor_smoothing );
    equi_risk_smoothed = flipdim(equi_risk_smoothed_flipped,2); % Added by Nuno

    % % % % Show image with anchor points
    % % % figure;
    % % % imshow(equi_risk_smoothed)
    % % % hold on;
    
    % Convert anchor points to equi image
    azimuth_area_w_rad = equi_sample_deg*2*pi/360;
    elevation_area_w_rad = (equi_sample_deg*2)*pi/360;
    [ px_x, px_y ] = XYZToEquiPx( rand_sparse_pts(:,1), rand_sparse_pts(:,2), rand_sparse_pts(:,3), azimuth_area_w_rad, elevation_area_w_rad );
    % % % scatter(px_x, px_y, 30 ,'b');
    % % % scatter(px_x, px_y, 30 ,repmat(risk_rand_sparse_pts,1,3),'filled');
    
    % This is where the equivalent of an omnidirectional camera projection
    % would come. I've tweaked to make it work by flipping left and right
    aux = [px_x px_y] / size(equi_risk_smoothed,2);
    aux(:,1) = 1 - aux(:,1);
    inlierPoints2 = aux * size(I2_uint8,2);
    
    % Show 3D point projections with risk colors
    subplot(2,2,3);
    imshow(I2_uint8); hold on;
    for idx = 1:length(normRiskVec)
        plot(inlierPoints2(idx,1),inlierPoints2(idx,2),'+','MarkerEdgeColor',double(riskColorVec(idx,:))/255);
    end;
    
    % Create the color coded heatmap image
    Irisk_RGB_small = ind2rgb(gray2ind(equi_risk_smoothed),riskColormap);
    Irisk_RGB = imresize(Irisk_RGB_small,[size(I2_uint8,1) size(I2_uint8,2)]);
    
    % figure; imshow(normal(vq));
    % figure; imshow(Irisk_RGB);
    
    I2_hsv = rgb2hsv(I2_uint8);
    Irisk_hsv = rgb2hsv(Irisk_RGB);
    
    I2modif_hsv(:,:,1) = Irisk_hsv(:,:,1);
    I2modif_hsv(:,:,2) = Irisk_hsv(:,:,2) * 0.75;
    I2modif_hsv(:,:,3) = I2_hsv(:,:,3);
    
    I2modif = hsv2rgb(I2modif_hsv);
    
    subplot(2,2,4);
    % imshow(Irisk_RGB);
    imshow(I2modif);
    
    drawnow;
    
%     frame = getframe(gcf);
%     close all;
%     newheight = floor(size(frame.cdata,1)/16)*16;
%     newwidth = floor(size(frame.cdata,2)/16)*16;
%     frame.cdata = frame.cdata(1:newheight,1:newwidth,:);
%     writeVideo(vidObj,frame);

    figure('Name','Results','units','normalized','outerposition',[0 0 1 1]);
    % Visualize risk indicators with risk colors
    subplot(1,2,1); hold on;
    for idx = 1:length(normRiskVec)
        plot(minD_norm(idx),t_norm(idx),'.','MarkerEdgeColor',double(riskColorVec(idx,:))/255);
    end;
    axis equal;
    xlabel('min dist normalized (sec)');
    ylabel('time to min dist normalized (sec)');
    xlim([0 20]);
    ylim([-10 50]);
    colormap(riskColormap);
    colorbar;
    title('Collision Indicators (with risk colors)')
    
    subplot(1,2,2);
    imshow(I2modif);
    
    drawnow;

    pause;
    close all;
    
    
end

close(vidObj);