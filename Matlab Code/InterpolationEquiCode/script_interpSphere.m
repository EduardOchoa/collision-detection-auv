
addpath(genpath('functions'));

% Generate random points with random risk
n_seeds = 800;
rand_sparse_pts = rand(n_seeds,3)-rand(n_seeds,3);
rand_sparse_pts = rand_sparse_pts./repmat(sqrt(sum(rand_sparse_pts.^2,2)),1,3);    % Make it on unit sphere
risk_rand_sparse_pts = rand(size(rand_sparse_pts,1),1);

% Generate equidistant map with smoothed risks
equi_sample_deg = 0.5;
n_neighbors_used = 50;
max_neighbor_smoothing = 0.1;

% Smoothed Image
[ equi_risk_smoothed ] = generate_equi_image_interpolate( equi_sample_deg, rand_sparse_pts, risk_rand_sparse_pts, n_neighbors_used, max_neighbor_smoothing );

% Show image with anchor points
figure;
imshow(equi_risk_smoothed)
hold on;

% Convert anchor points to equi image
azimuth_area_w_rad = equi_sample_deg*2*pi/360;
elevation_area_w_rad = (equi_sample_deg*2)*pi/360;
[ px_x, px_y ] = XYZToEquiPx( rand_sparse_pts(:,1), rand_sparse_pts(:,2), rand_sparse_pts(:,3), azimuth_area_w_rad, elevation_area_w_rad );
scatter(px_x, px_y, 30 ,'b');
scatter(px_x, px_y, 30 ,repmat(risk_rand_sparse_pts,1,3),'filled');
