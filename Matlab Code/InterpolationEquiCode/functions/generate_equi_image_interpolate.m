function [ equi_img ] = generate_equi_image_interpolate( equi_sample_deg, sparse_xyz, sparse_data, n_interpl_pts, max_interpl_d )
%GENERATE_EQUI_IMAGE Summary of this function goes here
%   Detailed explanation goes here

    px_value_undefined = 0;

    if isempty(sparse_data)
        n_ch = 1;
    else
        n_ch = size(sparse_data,2);
    end
    
    % Check if data is normalized to unit sphere
    pts_norms = sqrt(sum(sparse_xyz.^2,2));
    if std(pts_norms)==0
        % Compute sphere radius from data
        radius_sphere = norm(sparse_xyz(1,:));
    else
        sparse_xyz = sparse_xyz./repmat(pts_norms,1,3);
        radius_sphere = 1;
    end
    
    % Determine size of equirectangular image
    azimuth_area_w_rad = equi_sample_deg*2*pi/360;
    elevation_area_w_rad = (equi_sample_deg*2)*pi/360;
    equi_img_size = [2*pi/azimuth_area_w_rad, pi/elevation_area_w_rad];

    % Compute az/el for each px of equirectangular
    [px_x, px_y] = meshgrid((1:equi_img_size(1))',(1:equi_img_size(2))');
    [px_az, px_el] = EquiPxToAzEl(azimuth_area_w_rad, elevation_area_w_rad, px_x(:)-1, px_y(:)-1 );

    % Compute XYZ for each px of equirectangular
    [px_X, px_Y, px_Z] = AzElToXYZ(px_az, px_el, repmat(radius_sphere, size(px_az,1),1));
    
    % Create ZERO anchors
    all_sphere_pts = [px_X, px_Y, px_Z];
    all_sphere_pts = all_sphere_pts(1:10:end,:);
    % Create Kd-tree for closest pt search
    NS = createns(sparse_xyz);
    [~, closest_sparse_d] = (knnsearch(NS, all_sphere_pts,'K',1));
    b_additional_zero_anchors = closest_sparse_d > max_interpl_d;
    
    sparse_xyz_augmented = [sparse_xyz;all_sphere_pts(b_additional_zero_anchors,:)];
    sparse_data_augmented = [sparse_data;repmat(px_value_undefined,sum(b_additional_zero_anchors),1)];
    

    % Compute index for each px
    px_idx = zeros(equi_img_size(2) * equi_img_size(1), n_ch);
    for ch_i = 1:n_ch
        px_idx(:,ch_i) = sub2ind([equi_img_size(2), equi_img_size(1), 3], px_y(:), px_x(:), repmat(ch_i,size(px_x(:))));
    end

    % Find closest point for each px on sphere
    NS = createns(sparse_xyz_augmented);
    [closest_sparse_pt_idx, closest_sparse_d] = (knnsearch(NS, [px_X, px_Y, px_Z],'K',n_interpl_pts));

    b_perfect_fit_sphere_pts = closest_sparse_d(:,1) == 0;
    b_interpolate_sphere_pts = ~b_perfect_fit_sphere_pts;

    
    % Generate images
    equi_img = zeros(equi_img_size(2), equi_img_size(1), n_ch);
        
    % Inverse depth square
    valid_w = 1./(closest_sparse_d(b_interpolate_sphere_pts,:).^(3));
    valid_w = valid_w ./ repmat(sum(valid_w,2),1,size(valid_w,2));
    
    for ch_i = 1:n_ch
        sparse_data_ch = sparse_data_augmented(:,ch_i);
        % Compute interpolated values
        w_equi_pts = repmat(px_value_undefined,size(equi_img,1)*size(equi_img,2),1);       
        w_equi_pts(b_perfect_fit_sphere_pts) = sparse_data_ch(closest_sparse_pt_idx(b_perfect_fit_sphere_pts));
        w_equi_pts(b_interpolate_sphere_pts) = sum(sparse_data_ch(closest_sparse_pt_idx(b_interpolate_sphere_pts,:)) .* valid_w,2);
 
        equi_img(px_idx(:,ch_i)) = w_equi_pts;
    end
end

