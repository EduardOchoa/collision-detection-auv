function [ pt_x, pt_y ] = XYZToEquiPx( pt_X, pt_Y, pt_Z, azimuth_area_w_rad, elevation_area_w_rad )
%XYZTOEQUIPX Summary of this function goes here
%   Detailed explanation goes here

%     [pt_az, pt_el, pt_r] = cart2sph(pt_X, pt_Y, pt_Z);
%     px_x = (-pt_el +  pi/2) ./ elevation_area_w_rad;
%     px_y = (pt_az +  pi) ./ azimuth_area_w_rad;

[ sphere_Az, sphere_El, sphere_R ] = XYZToAzEl( pt_X, pt_Y, pt_Z );
[ pt_x, pt_y ] = AzElToEquiPx( azimuth_area_w_rad,elevation_area_w_rad, sphere_Az, sphere_El );
end

