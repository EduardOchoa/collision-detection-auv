function [pt_x, pt_y, pt_z ] = EquiPxToXYZ( azimuth_area_w_rad, elevation_area_w_rad, px_x, px_y, px_z )
%EQUIPXTOAZEL Summary of this function goes here
%   Detailed explanation goes here
    [ px_az, px_el ] = EquiPxToAzEl( azimuth_area_w_rad, elevation_area_w_rad, px_x, px_y );
    [ pt_x, pt_y, pt_z ] = AzElToXYZ( px_az, px_el, px_z );
end
