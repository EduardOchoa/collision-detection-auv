function [ pt_x, pt_y ] = AzElToEquiPx( azimuth_area_w_rad,elevation_area_w_rad, pt_az, pt_el )
%AZELTOEQUIPX Summary of this function goes here
%   Detailed explanation goes here

pt_x = round((pt_az + pi) ./ azimuth_area_w_rad);
pt_x(pt_x==0) = 1;
pt_y = round((-pt_el + pi/2) ./ elevation_area_w_rad);
pt_y(pt_y==0) = 1;

end

