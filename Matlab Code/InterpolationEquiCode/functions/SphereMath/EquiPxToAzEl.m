function [ px_az_v, px_el_v ] = EquiPxToAzEl( azimuth_area_w_rad, elevation_area_w_rad, px_x, px_y )
%EQUIPXTOAZEL Summary of this function goes here
%   Detailed explanation goes here
    px_az_v = ((px_x).*azimuth_area_w_rad)- pi;
    px_el_v = -(((px_y).*elevation_area_w_rad) - pi/2);
end
