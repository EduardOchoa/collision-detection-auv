function [ I ] = read_binary_image( img_path )
%READ_BINARY_IMAGE Read single-channel float image created by MDM

% Open file
fid = fopen(img_path,'r');

% Read image dimensions
I_dim = fread(fid,2,'*float');  % width, height

% Read image data - row major
I_row = fread(fid,I_dim(1)*I_dim(2),'*float');

% Close file
fclose(fid);

% Reshape and correct for column-major in matlab
I = reshape(I_row, I_dim(1), I_dim(2))';

end

