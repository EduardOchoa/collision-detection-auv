% Load AUX functions
addpath(genpath('functions'));

depth_image_path = 'test_data/camera5_frame23700.jpg_equi.bin';

% Load image
equi_depth_image = read_binary_image(depth_image_path);

% Subsample depth image - IMPORTANT TO USE NEAREST PT to not add smoothing
% witn 0 depth values (Induces gap in glow!)
equi_depth_image = imresize(equi_depth_image,0.25,'nearest');

% Get pixel coordinates
[px_x,px_y] = meshgrid(1:size(equi_depth_image,2),1:size(equi_depth_image,1));
px_z = equi_depth_image;

% Remove zero points (UNKNOWN DEPTH)
px_x = px_x(equi_depth_image~=0);
px_y = px_y(equi_depth_image~=0);
px_z = px_z(equi_depth_image~=0);

% Get equirectangulat pixels in 3D
azimuth_area_w_rad = (2*pi) / size(equi_depth_image,2);
elevation_area_w_rad = pi/size(equi_depth_image,1);
[pt_x, pt_y, pt_z ] = EquiPxToXYZ( azimuth_area_w_rad, elevation_area_w_rad, px_x, px_y, px_z );

% Create smoothed version
equi_sample_deg = 0.5;
n_neighbors_used = 50;
max_neighbor_smoothing = 0.2;
[ equi_risk_smoothed ] = generate_equi_image_interpolate( equi_sample_deg, [pt_x, pt_y, pt_z], px_z, n_neighbors_used, max_neighbor_smoothing );


% Show
figure;imshow(equi_depth_image,[]); title('Original EQUI');
figure;imshow(equi_risk_smoothed,[]); title('Smoothed EQUI');

