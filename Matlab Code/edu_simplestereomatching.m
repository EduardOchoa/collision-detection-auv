% This script is used to estimate the extrinsics from point matches

clear all;
close all;
clc;

baseDir = fileparts(fileparts(fileparts(which(mfilename))));

% if isempty(baseDir)
%     baseDir = 'C:\Users\ngracias';
% end;

% addpath C:\Users\serdp\svn\onlinemosaics\hashim_work -end;
% addpath C:\users_local\serdp\svn\onlinemosaics\hashim_work -end;
% addpath Z:\Users\nuno\serdp\svn\onlinemosaics\hashim_work -end;
% addpath Z:\Users\nuno\serdp\toolbox_calib -end;

addpath([fileparts(baseDir) filesep 'serdp\toolbox_calib'],'-end');

%
% imgLeftFilename = 'C:\users\serdp\test\sequences\boreas_sample\camera4_frame24156.jpg';
% imgRightFilename = 'C:\users\serdp\test\sequences\boreas_sample\camera4_frame24171.jpg';

% SEQNAMEstruct = sd_findsubfolderfiles('C:\users\serdp\test\sequences\boreas_sample','jpg');
SEQNAMEstruct = sd_findsubfolderfiles([fileparts(baseDir) filesep '\serdp\test\sequences\boreas_sample'],'jpg');

% for pairIdx = 19:(length(SEQNAMEstruct)-2)

imgIdx1 = 185;
imgIdx2 = 190;
    
    imgLeftFilename = SEQNAMEstruct(imgIdx1).filename;
    imgRightFilename = SEQNAMEstruct(imgIdx2).filename;
    
    [I1_uint8,I2_uint8,points3D,ptCloud,t,R,inlierPoints1,inlierPoints2] = edu_processimgpair(imgLeftFilename,imgRightFilename);
    


wPoints3D = points3D;

riskColormap = jet(64);

dt = 2; % Assumed time interval between the images in seconds
[minD_norm,t_norm] = edu_processrisk(t,R,dt,wPoints3D,riskColormap,ptCloud,inlierPoints2,I2_uint8);

return;


%% New method

% Compute colision risk indicators
dt = 2; % Assumed time interval between the images in seconds
[minD_norm,t_norm] = edu_computemindt(wt1,wt2,dt,wPoints3D);

% plot colision risk indicators
figure; plot(minD_norm,t_norm,'.'); axis equal;
xlabel('min dist normalized (sec)');
ylabel('time to min dist normalized (sec)');
xlim([0 30]);
ylim([0 50]);

% Compute risk
k = 0.4;
max_step_saturated = 2;
max_value = 1;
tnOffset = 0.5;
[ normRiskVec ] = compute_risk( minD_norm, t_norm, k, max_step_saturated, max_value, tnOffset);

% Compute risk colors per point
riskColorVec = uint8(zeros(length(normRiskVec),3));
for idx = 1:length(normRiskVec)
    colormapIdx = round(normRiskVec(idx) * size(riskColormap,1));
    if colormapIdx == 0
        colormapIdx = 1;
    end;
    if colormapIdx > size(riskColormap,1)
        colormapIdx = size(riskColormap,1);
    end;
    riskColorVec(idx,:) = uint8(riskColormap(colormapIdx,:)*255);
end;


% Visualize the camera locations and orientations
ptCloudRisk1 = ptCloud;
ptCloudRisk1.Color = riskColorVec;
show3dpointscamera(ptCloudRisk1,t,R);
title('Collision Risk 1');

% Show 3D point projections with risk colors
figure; imshow(I2_uint8); hold on;
for idx = 1:length(normRiskVec)
    plot(inlierPoints2(idx,1),inlierPoints2(idx,2),'+','MarkerEdgeColor',double(riskColorVec(idx,:))/255);
end;

% Compute collision risk heat image
[I2modif,Irisk_RGB] = edu_riskimage(inlierPoints2,normRiskVec,I2_uint8,riskColormap);
figure; imshow(Irisk_RGB);
newfig(I2modif);



figure('units','normalized','outerposition',[0 0 1 1])

subplot(2,2,1); hold on;
% plot(minD_norm,t_norm,'.'); 
for idx = 1:length(normRiskVec)
    plot(minD_norm(idx),t_norm(idx),'.','MarkerEdgeColor',double(riskColorVec(idx,:))/255);
end;
axis equal;
xlabel('min dist normalized (sec)');
ylabel('time to min dist normalized (sec)');
xlim([0 30]);
ylim([0 50]);
colormap(riskColormap);
colorbar;

axisHndl = subplot(2,2,2); 
show3dpointscamera(ptCloud,t,R,axisHndl);
xlim([-30 30]);
ylim([-10 10]);
zlim([0 50]);


subplot(2,2,3); 
imshow(I2_uint8); hold on;
for idx = 1:length(normRiskVec)
    plot(inlierPoints2(idx,1),inlierPoints2(idx,2),'+','MarkerEdgeColor',double(riskColorVec(idx,:))/255);
end;

subplot(2,2,4);
imshow(Irisk_RGB);
imshow(I2modif);


