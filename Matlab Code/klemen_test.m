clear all;
close all;
clc;

addpath(genpath('InterpolationEquiCode'));

doGroundtruth = false;
doValidmask = true;
maxAcceptableInc3Dpoints = 7000;

% Variables to select testing setups
dataset = 2;
forces_flag = true;

if dataset == 1
    cam_filepath = '/home/eduochoa/PointClouds/BoreasLong_PC/CSV_CamPoses';
    pts_filepath =  '/home/eduochoa/PointClouds/BoreasLong_PC/CSV_PointClouds';
    panoramic_imgs_path = '/home/eduochoa/Datasets/BoreasLongFiles/panos_1.5m';
    sim_flag = 0;
    data_size = 482;
elseif dataset == 2
    cam_filepath = '/home/eduochoa/PointClouds/Sim_all_cams_fixed/CSV_CamPoses';
    pts_filepath =  '/home/eduochoa/PointClouds/Sim_all_cams_fixed/CSV_PointClouds';
    panoramic_imgs_path = '/home/eduochoa/Datasets/ladybug_simulated_panos';
    sim_flag = 1;
    data_size = 100;
end

panoramic_valid_mask_img_filename = ['pano_valid_mask.png'];


riskColormap = jet(64);

if doValidmask
    panoValidMask = (rgb2gray(imread(panoramic_valid_mask_img_filename)) > 128);
    % newfig(normal(double(panoValidMask)));
end;

if forces_flag
    % Point cloud of the total trajectory in world coordinates
    world_pc = [];
    % Camera positions in world coordinates
    cam_trajectory = [];
    % Repulsion forces
    repulsion_vectors = [];
end

% data_size = 120;

for pcd_i = 20:5:data_size
    cam_data = readtable(sprintf('%s%s%s_%d.csv', cam_filepath,filesep,'CAM',pcd_i),'Delimiter',' ');
    pts_data = readtable(sprintf('%s%s%s_%d.csv', pts_filepath,filesep,'PointCloud',pcd_i));
    
    % Getting the current image sequence name
    current_frame_name = cell2mat(cam_data{2,19});
    idx_frameID_in_str = strfind(current_frame_name,'frame');
    frameID = current_frame_name(idx_frameID_in_str:idx_frameID_in_str+9);
    
    % Get timestamps
    timestamp_1 = cam_data{1,18};
    timestamp_2 = cam_data{2,18};
    
    % Compute relative pose
    w_T_b1 = reshape(cam_data{1,2:17},4,4)';
    w_T_b2 = reshape(cam_data{2,2:17},4,4)';
    b2_T_w = eye(4,4);
    b2_T_w(1:3,1:3) = w_T_b2(1:3,1:3)';
    b2_T_w(1:3,4) = -w_T_b2(1:3,1:3)' * w_T_b2(1:3,4);
    b2_T_b1 = b2_T_w*w_T_b1;
    
    % Get data
    C1 = b2_T_b1(1:3,4);
    C2 = zeros(3,1);
    %     v = C2 - C1;
    incPoints3D_raw = pts_data{:,1:3};
    delta_time = timestamp_2-timestamp_1;
    
    % For testing the speed effect
    factor_of_time = 1;
    delta_time = factor_of_time * delta_time;
    
    %% Nuno's block here
    
    if doValidmask
        incPoints3D = edu_verifyvalid3dpoints(incPoints3D_raw,panoValidMask);
    else
        incPoints3D = incPoints3D_raw;
    end;
    
    incPoints3D = incPoints3D(sqrt(sum(incPoints3D.^2,2))<30,:);
    incPoints3D = incPoints3D(1:10:end,:);
    
%     incPoints3D = (rotz(180)* incPoints3D')';
    
    if size(incPoints3D,1) > maxAcceptableInc3Dpoints
        randIdx = randperm(size(incPoints3D,1),maxAcceptableInc3Dpoints);      
        incPoints3D = incPoints3D(randIdx,:);
    end;
    
    [minD_norm,t_norm,risks] = klemen_processriskomni(C1,C2,b2_T_b1,delta_time,incPoints3D,riskColormap);
    
    
    if forces_flag
        [total_force,world_pc,cam_trajectory,repulsion_vectors] = compute_forces(C1,C2,incPoints3D,risks.values,w_T_b1,w_T_b2,risks.colors,world_pc,cam_trajectory,repulsion_vectors);
    end

%     if pcd_i ~= data_size 
%         close all;
%     end
% pause(0.5);
campos([66.0099874039327,-29.2327330155307,94.6118457883268]);
camva([10.83481946582971]);
camtarget([3.447809004450001,12.176269544036,-12.5347779031225]);
camup([0.683079275849926,-0.452120309837077,-0.573576436351046]);
% 
% campos([67.53602179490615,-5.351878198211589,-29.29231748246671]);
% camva([10.83481946582971]);
% camtarget([-3.607491497515836,-1.000554518536229,3.944440847931406]);
% camup([0.421829993491952,-0.02580022766003,0.90630778703665]);

end