function [I2modif,Irisk_RGB] = edu_riskimage(inlierPoints2,normRisk1Vec,I2_uint8,riskColormap)
% [I2modif,Irisk_RGB] = edu_riskimage(inlierPoints2,normRisk1Vec,I2_uint8,riskColormap)
%
%


sizeDivider = 10;
[xq,yq] = meshgrid(1:1:size(I2_uint8,2)/sizeDivider,1:1:size(I2_uint8,1)/sizeDivider);
x = inlierPoints2(:,1)/sizeDivider;
y = inlierPoints2(:,2)/sizeDivider;
v = normRisk1Vec;

normalizedJump = floor(100/sizeDivider);
[xback,yback] = meshgrid(1:normalizedJump:size(I2_uint8,2)/sizeDivider,1:normalizedJump:size(I2_uint8,1)/sizeDivider);

xbackVec = xback(:);
ybackVec = yback(:);

[knnIdx,knnDist] = knnsearch([x y],[xbackVec ybackVec]);

validPointToAddIdx = find(knnDist > (normalizedJump * 2)); 

xbackValidVec = xbackVec(validPointToAddIdx);
ybackValidVec = ybackVec(validPointToAddIdx);

% % % figure; 
% % % plot(x,y,'+'); hold on;
% % % plot(xbackValidVec,ybackValidVec,'.');
% % % axis image

xadded = [x; xbackValidVec];
yadded = [y; ybackValidVec];
vadded = [v; zeros(size(xbackValidVec))];


vq = griddata(xadded,yadded,vadded,xq,yq,'linear');
vq(isnan(vq)) = 0;

Irisk_RGB_small = ind2rgb(gray2ind(vq),riskColormap);

Irisk_RGB = imresize(Irisk_RGB_small,[size(I2_uint8,1) size(I2_uint8,2)]);


% figure; imshow(normal(vq));
% figure; imshow(Irisk_RGB);

I2_hsv = rgb2hsv(I2_uint8);
Irisk_hsv = rgb2hsv(Irisk_RGB);

I2modif_hsv(:,:,1) = Irisk_hsv(:,:,1);
I2modif_hsv(:,:,2) = Irisk_hsv(:,:,2) * 0.75;
I2modif_hsv(:,:,3) = I2_hsv(:,:,3);

I2modif = hsv2rgb(I2modif_hsv);

% newfig(I2modif);