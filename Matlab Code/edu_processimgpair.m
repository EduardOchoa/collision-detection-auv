function [I1_uint8,I2_uint8,points3D,ptCloud,t,R,inlierPoints1,inlierPoints2] = edu_processimgpair(imgLeftFilename,imgRightFilename)
% [I1_uint8,I2_uint8,ptCloud,t,R] = edu_processimgpair(imgLeftFilename,imgRightFilename)
%
%


% measuredTlength = 350;
SIFTOPTIONS.resizeFactor = 0.5; SIFTOPTIONS.imgFilterNumber = 3; SIFTOPTIONS.ignoreSIFTfile = 0; SIFTOPTIONS.maxSifts = 8000;


% % This is a set of original, not radially corrected images of the sea
% imgLeftFilename = 'C:\Users\ngracias\deaimgdata\sea20140504\imageLeftResync02486.jpg';
% imgRightFilename = 'C:\Users\ngracias\deaimgdata\sea20140504\imageRightResync02486.jpg';
% measuredTlength = 95.5;
% SIFTOPTIONS.resizeFactor = 0.5; SIFTOPTIONS.imgFilterNumber = 0; SIFTOPTIONS.ignoreSIFTfile = 0; SIFTOPTIONS.maxSifts = 4000;


I1_uint8 = imread(imgLeftFilename);
I2_uint8 = imread(imgRightFilename);

useMatlabExampleCode = false;

if useMatlabExampleCode
    displayResults = 1;
    [inlierPoints1,inlierPoints2,fMatrix, matchedPoints1, matchedPoints2] = matchimagesfundamental_matlabexample(I1_uint8,I2_uint8,displayResults);
    
    figure; showMatchedFeatures(I1_uint8, I2_uint8, matchedPoints1, matchedPoints2);
    legend('Putatively matched points in I1', 'Putatively matched points in I2');
    
    figure; showMatchedFeatures(I1_uint8, I2_uint8, inlierPoints1, inlierPoints2);
    legend('Inlier points in I1', 'Inlier points in I2');
    
    [fMatrix, epipolarInliers, status] = estimateFundamentalMatrix(matchedPoints1, matchedPoints2);
    
    inlierPoints1 = matchedPoints1(epipolarInliers, :);
    inlierPoints2 = matchedPoints2(epipolarInliers, :);
    
    figure; showMatchedFeatures(I1_uint8, I2_uint8, inlierPoints1, inlierPoints2);
    legend('Inlier points in I1', 'Inlier points in I2');
end;


%% match using Nuno's functions

% SIFTOPTIONS.resizeFactor = 0.5;
% SIFTOPTIONS.imgFilterNumber = 3;
% SIFTOPTIONS.ignoreSIFTfile = 0;
% SIFTOPTIONS.maxSifts = 2000;


% SIFTOPTIONS.resizeFactor = sd_getparameter(SIFTOPTIONS,'resizeFactor',[]);
% SIFTOPTIONS.imgFilterNumber = sd_getparameter(SIFTOPTIONS,'imgFilterNumber',0);
% SIFTOPTIONS.ignoreSIFTfile = sd_getparameter(SIFTOPTIONS,'ignoreSIFTfile',0);
% SIFTOPTIONS.maxSifts = sd_getparameter(SIFTOPTIONS,'maxSifts',2000);

% MATCHOPTIONS.method = 'lsplanar';
MATCHOPTIONS.method = 'lssemrig';
MATCHOPTIONS.samplesize = 4;
MATCHOPTIONS.numiter1 = 500;
MATCHOPTIONS.numiter2 = 100;
MATCHOPTIONS.medtrhes = 0;
MATCHOPTIONS.disttrhes = 300;
MATCHOPTIONS.useJointMatch = 0;
MATCHOPTIONS.minIniSiftMatches = 30;
MATCHOPTIONS.maxMatchesPerSift = 2;


[im, des, lastloc, lastdesU8] = siftnuno(imgLeftFilename,SIFTOPTIONS);
[im, des, currloc, currdesU8] = siftnuno(imgRightFilename,SIFTOPTIONS);

% Attempt matching
[Mlastcurr,FINALCL1,FINALCL2,CL1uv,CL2uv] = imgmatchsift(lastloc,lastdesU8,currloc,currdesU8,MATCHOPTIONS);

figure;
subplot(1,2,1); showMatchedFeatures(I1_uint8, I2_uint8, FINALCL1,FINALCL2);
legend('Inlier points in I1', 'Inlier points in I2');
title('Accepted with planar homography');


[fMatrix, epipolarInliers, status] = estimateFundamentalMatrix(FINALCL1,FINALCL2,'NumTrials',1000);

inlierPoints1 = FINALCL1(epipolarInliers, :);
inlierPoints2 = FINALCL2(epipolarInliers, :);

subplot(1,2,2); showMatchedFeatures(I1_uint8, I2_uint8, inlierPoints1, inlierPoints2);
legend('Inlier points in I1', 'Inlier points in I2');
title('Accepted with F matrix from Nuno''s sift');


% % Test of the optimization from triangulation done by Hashim
% baseDir = fileparts(fileparts(fileparts(which(mfilename))));
% if isempty(baseDir)
%     baseDir = 'C:\Users\ngracias';
% end;

% These values were taken from
% C:\users_local\serdp\test\sequences\boreas_sample\camera_data\cam_intrinsics\gsfm_f_06_m_08_eq001_fix_new_intr_hard_ext_prunned_guided_BA_intrinsics_info.csv
fc_left = [1184.03 1184.03]';
cc_left = [1329.98	1052.05]';
kc_left = 0 * [0.119245	-0.0876406	0.0636973	-0.0168751 0]';
alpha_c_left = 0;

fc_right = [1184.03 1184.03]';
cc_right = [1329.98	1052.05]';
kc_right = 0 * [0.119245	-0.0876406	0.0636973	-0.0168751 0]';
alpha_c_right = 0;

ny = 2028;
nx = 2704;

% % warning('Correcting for the T that was wrongly set to 100mm instead of 56mm');
% om_ori = [0 0 0];   % These are the initial values for the rotation between the cameras, not sure which convention, but the same as used in Bouguet's toolbox
% T_ori = [0 0 measuredTlength];  % These are the initial values for the translation between the cameras, not sure which convention, but the same as used in Bouguet's toolbox 
% clear T;


COORDLISTleft = [];
COORDLISTright = [];

%   Matched points
xl = [inlierPoints1; COORDLISTleft]';
xr = [inlierPoints2; COORDLISTright]';
%   Left and right images
imgl = imgLeftFilename;
imgr = imgRightFilename;


matchedpt = [xl; xr];




%% Create matlab structures for display
% % % matchedPoints1 = imagePoints1(validIdx, :);
% % % matchedPoints2 = imagePoints2(validIdx, :);
matchedPoints1 = inlierPoints1;
matchedPoints2 = inlierPoints2;

K = [fc_left(1) 0 cc_left(1); 0 fc_left(2) cc_left(2); 0 0 1];
cameraParams = cameraParameters('IntrinsicMatrix',K');

[R, t] = cameraPose(fMatrix, cameraParams, inlierPoints1, inlierPoints2);

% Compute the camera matrices for each position of the camera
% The first camera is at the origin looking along the X-axis. Thus, its
% rotation matrix is identity, and its translation vector is 0.
camMatrix1 = cameraMatrix(cameraParams, eye(3), [0 0 0]);
camMatrix2 = cameraMatrix(cameraParams, R', -t*R');

% Compute the 3-D points
points3D = triangulate(matchedPoints1, matchedPoints2, camMatrix1, camMatrix2);

% Get the color of each reconstructed point
numPixels = size(I1_uint8, 1) * size(I1_uint8, 2);
allColors = reshape(I1_uint8, [numPixels, 3]);
colorIdx = sub2ind([size(I1_uint8, 1), size(I1_uint8, 2)], round(matchedPoints1(:,2)), ...
    round(matchedPoints1(:, 1)));
color = allColors(colorIdx, :);

% Create the point cloud
ptCloud = pointCloud(points3D, 'Color', color);


% Visualize the camera locations and orientations
show3dpointscamera(ptCloud,t,R);
title('Up to Scale Reconstruction of the Scene');
