function [minD_norm,t_norm,risk_struct] = klemen_processriskomni(C1,C2,b2_T_b1,delta_time,wPoints3D,riskColormap)
% [minD_norm,t_norm] = edu_processrisk(wt1,wt2,dt,wPoints3D,riskColormap,ptCloud,inlierPoints2,I2_uint8)
%
% 
% dt is the duration of the camera motion step in seconds
% R is the rotation of camera 2 with repect to 1 as definer by matlabs function relativeCameraPose
    
    sim_flag = 0;
    tests_flag = false;
    
    % just adapt the inputs to my naming of the variables
    wt1 = C1;
    wt2 = C2;
    v = C2 - C1;
    dt = delta_time;
%     wPoints3D = P;
%     riskColormap = jet(64);
    
    norm_factor = (norm(v)/delta_time);
    
    
    % Compute colision risk indicators
    
    % dt = 2; % Assumed time interval between the images in seconds
    [minD_norm,t_norm] = edu_computemindt(wt1,wt2,dt,wPoints3D);
    
    % % % plot colision risk indicators
    % % figure; plot(minD_norm,t_norm,'.'); axis equal;
    % % xlabel('min dist normalized (sec)');
    % % ylabel('time to min dist normalized (sec)');
    % % xlim([0 30]);
    % % ylim([-20 50]);
    
    % Compute risk
    k = 1.2;
    max_step_saturated = 4;
    max_value = 1;
    tnOffset = 0.5;
    [ normRiskVec ] = compute_risk( minD_norm, t_norm, k, max_step_saturated, max_value, tnOffset);
    risk_struct.values = normRiskVec;
    
    % Compute risk colors per point
    riskColorVec = uint8(zeros(length(normRiskVec),3));
    for idx = 1:length(normRiskVec)
        colormapIdx = round(normRiskVec(idx) * size(riskColormap,1));
        if colormapIdx == 0
            colormapIdx = 1;
        end;
        if colormapIdx > size(riskColormap,1)
            colormapIdx = size(riskColormap,1);
        end;
        riskColorVec(idx,:) = uint8(riskColormap(colormapIdx,:)*255);
    end;
    
    risk_struct.colors = riskColorVec;
    
    
