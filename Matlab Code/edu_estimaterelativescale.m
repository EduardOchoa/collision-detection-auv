function [incMultiplier,sfmMultiplier] = edu_estimaterelativescale(incPoints3D,sfmPoints3D)
% [incMultiplier,sfmMultiplier] = edu_estimaterelativescale(incPoints3D,sfmPoints3D)
%
%

% Find some points that are right below the reference frame
ptCloudIncrementalOnlyXY = pointCloud(incPoints3D * diag([1 1 0]));
distSelection = 100;
[indicesIncrementalOnlyXY,distsIncrementalOnlyXY] = findNeighborsInRadius(ptCloudIncrementalOnlyXY,[0 0 0],distSelection);
%     ptCloudIncremental.Location(indicesIncrementalOnlyXY,:)
incDistanceInZtoClosestOnlyXY = incPoints3D(indicesIncrementalOnlyXY(1:10),3)
incMultiplier = abs(mean(incDistanceInZtoClosestOnlyXY))



% Find some points that are right below the reference frame
ptCloudSfmOnlyXY = pointCloud(sfmPoints3D * diag([1 1 0]));
distSelection = 100;
[indicesSfmOnlyXY,distsIncrementalOnlyXY] = findNeighborsInRadius(ptCloudSfmOnlyXY,[0 0 0],distSelection);
%     ptCloudIncremental.Location(indicesIncrementalOnlyXY,:)
sfmDistanceInZtoClosestOnlyXY = sfmPoints3D(indicesSfmOnlyXY(1:10),3)
sfmMultiplier = abs(mean(sfmDistanceInZtoClosestOnlyXY))


% Visualize the point cloud

ptCloudInc = pointCloud(incPoints3D, 'Color', repmat([1 0 0],size(incPoints3D,1),1));

ptCloudSfm = pointCloud(sfmPoints3D, 'Color', repmat([0 1 0],size(sfmPoints3D,1),1));

figure;

% pcshow(ptCloud, 'VerticalAxis', 'y', 'VerticalAxisDir', 'down', ...
%     'MarkerSize', 45);
pcshow(ptCloudInc, 'VerticalAxis', 'z', 'VerticalAxisDir', 'up', ...
    'MarkerSize', 45);
hold on;
pcshow(ptCloudSfm, 'VerticalAxis', 'z', 'VerticalAxisDir', 'up', ...
    'MarkerSize', 45);


% Rotate and zoom the plot
% camorbit(0, -30);
camorbit(-80, 0);
camzoom(1.2);
% campos([-17.3734 -224.3460 -344.9331]);

% Label the axes
xlabel('x-axis');
ylabel('y-axis');
zlabel('z-axis');



ptCloudIncScaled = pointCloud(incPoints3D*incMultiplier, 'Color', repmat([1 0 0],size(incPoints3D,1),1));
ptCloudSfmScaled = pointCloud(sfmPoints3D*sfmMultiplier, 'Color', repmat([0 1 0],size(sfmPoints3D,1),1));

figure;

% pcshow(ptCloud, 'VerticalAxis', 'y', 'VerticalAxisDir', 'down', ...
%     'MarkerSize', 45);
pcshow(ptCloudIncScaled, 'VerticalAxis', 'z', 'VerticalAxisDir', 'up', ...
    'MarkerSize', 45);
hold on;
pcshow(ptCloudSfmScaled, 'VerticalAxis', 'z', 'VerticalAxisDir', 'up', ...
    'MarkerSize', 45);


% Rotate and zoom the plot
% camorbit(0, -30);
camorbit(-80, 0);
camzoom(1.2);
% campos([-17.3734 -224.3460 -344.9331]);

% Label the axes
xlabel('x-axis');
ylabel('y-axis');
zlabel('z-axis');
