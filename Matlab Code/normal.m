function N = normal(M)
% N = normal(M)
%
% Normalize:
% Normaliza uma imagem, escalando linearmente a intensidade de cada ponto
% para valores entre 0 e 1.0 .
% 

MINM = min(min(M));
MAXM = max(max(M));

M = M - ones(size(M)) * MINM;
N = M / (MAXM-MINM); 